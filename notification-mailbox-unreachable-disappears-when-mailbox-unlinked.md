After the Briar user sees the notification that their mailbox is unreachable, tapping the notification will take them to the Briar's mailbox status screen.  On that screen, they can tap the Unlink link at the bottom of the screen, which will unlink their mailbox. 

After that, the notification that says that mailbox is not reachable should disappear. 

Tested OK build c855967d56c53d2c5e0ba5d28b6357d521c90e61

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)