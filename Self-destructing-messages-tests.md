 MR 1351, ticket #1893

**MR1351 Testing instructions**

* send messages with a contact and enable/disable disappearing messages in between messages.
* check that there's little notices highlighting the change that say 'tap to learn more'
* check that tapping a notice opens a screen explaining disappearing messages

Ticket #1893
**Title: Provide explanation when self-destruct timer gets changed**

When self-destruct timer gets changed initially and later, we should provide a way to get more information about this feature for users that don't understand it, yet.

This could be as simple as making the timer changed message bubble tapable "Tap here to change" and bring the user to the settings screen for self-destructing messages that has further explanation #1837 (closed)


**Test Specification **
(derived form the above two gitlab entries and from the actual use of devices)
- [ ] Default is: disappearing messages setting is off
- [ ] Precondition: User on device1 has some contacts, but no messages yet. User on device2 - ditto.
- [ ] Send a few messages from one contact to another, while the 'disappearing messages' setting is off. 
- [ ] User on device1 goes to the menu in the upper right corner and selects Disappearing Messages
- [ ] A screen on device1 shows with the message: 
"Make future messages in this conversation automatically disappear after 7 days", 
a little bomb icon, and a 
switch to turn this setting on or off.  
Tappable Link that says: learn more
- [ ] User on device1 sees the above described screen, but before they send messages to device2, the device2 user doesn't know that this setting has changed - there is no infromation for them yet at this point.
- [ ] on tapping Learn more link, User on device1 is shown the information screen exaplaining the functioning of this setting.
- [ ] Bottom right corner of the information screen, there is a Got It tappable link, and tapping on that, returns the user on device1 back to the previous screen.  There is also 'Back' navigation arrow in the navigtion bar, which returns the user from the information screen back to the previous screen. (POrtrait and landscape orientation). 
- [ ] Information screen is scrollable in landscape orientation and all the links still work (back button in navigation bar and Got it in the right hand side bottom corner)
- [ ] <When the user on device 1 starts typing the next message to the user on device2, there is a little bomb icon in the text field itself, to remind the user that they are typing a message that will disappear
- [ ] After device1 sends the message to the device2, both users are informed that the messages will disappear (automatically generated information messages on their sceens that inform them that messages will disappear after a period of time - not configurable by user). 
- [ ] After the device1 sends a disappearing message to the device2, this act will change the device2 disappearing setting: if it is 'off' it will change it to 'on'.
- [ ] Device 1 user sees a little bomb icon on their sent message, and the device2 user sees the little bomb icon on the message they received. 
- [ ] The messages disappear after the stated time interval  
- [ ] Tested with both devices having this setting 'on', one 'on' and one 'off', changing the setting from either device during conversation, portrait and landscape, and both devices having this setting 'off'. 

**#1864 Show warning dialog when the expected timer differs from the current timer **

We mirror the timer duration from our contact's messages. It is possible that we are writing a message and short before we hit send, the timer changes or gets turned off. To prevent this scenario, we should show a warning dialog that pops up if the timer when sending is different from the timer when we started typing the message.


**MR 1328 Show warning dialog when the expected timer differs from the current timer **

This MR refactors message sending to return a LiveData that the TextSendController can observe to react to the sending state. The main reason is to be able to reliably catch automatic changes to the auto-delete timer which might surprise the user.

Test instructions:

* Add a contact, open private conversation screen and send a message
* Start typing a second message and activate disappearing messages, finish typing the message and send
  * check that a warning message appears warning about the changed timer
* Start typing a third message, but don't send
* Let your contact disable disappearing messages and send a message
* Wait for the contact's message to arrive and then send your message
  * check that a warning message appears warning about the changed timer

**Test Specification**

Preconditions: 
- [ ] Delete all messages between the device1 and Device2, if there are any. 
- [ ] Send a few non-disappearing messages between the two devices
- [ ] Device1 start typing a message to device 2, device 2 change the disappearing messages setting to 'on' and start typing a message to device1
- [ ] device2 sends its message to device1, device 1 sends it message to device 2
- [ ] Device 2 sees an automatically generates message on their screen: Disappearing messages changed.  Since you started composing your message, disappearing messages have changed'.  ON this warning message, options for the user are: 'send anyway' and 'cancel'.  
- [ ] Tapping on 'cancel' cancels the warning message and returns the user back to the mesage composing screen.
- [ ] Tapping on 'Send anyway' sends the message to the device1
- [ ] Device 1 receives the disappearing message, and there is the 'device2's message, containing little bomb icon, there is an automatically generated message to say that device2's message will disappear after a period of time, and there is a little icon on the device1 text input field.  
- [ ] Device1 setting 'disappearing messages' has been turned 'on' when they received the device2 disappearing message. 
- [ ] When device1 tries to send the message they were composing, they receive the same warning as did device2 when they changed this setting in the middle of composing the message: Disappearing messages changed.  Since you started composing your message, disappearing messages have changed'.  ON this warning message, options for the user are: 'send anyway' and 'cancel'.  

**#1833 Delete messages when their self-destruct timers expire**

#1836 Automatically decline incoming private group invitations when they self-destruct

**MR 1389 Automatically decline incoming private group invitations when they self-destruct**
 
Test instructions:

* use two or more contacts
* create various private groups
* invite contacts to private groups
* accept/decline invitations
* enable/disable disappearing messages throughout the process
* ensure that messages sent with enabled timers self-destruct (also while private conversation is open)
* ensure that open invitations get automatically declined and are recognizable as automatic for the *sender* of the decline

**#1833 Delete messages when their self-destruct timers expire**

Create a component that tracks pending self-destruct timers and deletes messages when their self-destruct timers expire.

Conversation clients will register messages for deletion during delivery. The new component will be responsible for calling back into the client when a message is due to be deleted. This will allow the client to take any necessary steps before deletion, such as declining an open introduction.

**MR 1312 Mirror the contact's changes to the self-destruct timer**
 
This branch records the self-destruct timer from incoming messages and updates the local timer to match the remote timer when appropriate:

* If the incoming message has an earlier or equal timestamp to the latest message sent or received so far, we don't update our timer
* If we haven't changed the local timer since sending our last message, we mirror the remote timer
* If we have changed the local timer since sending our last message, we only mirror the remote timer if it has changed; messages that continue to use the old timer don't overwrite our local change

**Test specification**

For the incoming message to have an earlier timestamp than the latest message we sent or received, there would have to be a delay in the message delivery - how to simulate that?


* If we haven't changed the local timer since sending our last message, we mirror the remote timer

To test this scenario: 
- [ ] device 1 has their Disappearing messages setting set to On.  The remote device2 has their Disappearing setting set to Off.  
- [ ] device2 sends device1 a message.  
- [ ] device1 receives message, together with the automatically generated messages informing them that the message they just received is not going to be seld-destucted.

**MR1382 Delete messages when their self-destruct timers expire (no UI) **

This branch implements automatic deletion of private messages and attachments. The sender starts the timer for a private message when it's acked; the recipient starts the timer when the private message is read.

When an attachment is received, we check whether it's listed as an attachment by any private message received in the conversation so far. If not, an orphan cleanup timer with a duration of 28 days is started. The timer is stopped if we receive a private message that lists the attachment; otherwise the orphaned attachment is deleted.

Manual deletion of private messages with missing attachments is now allowed; the orphan cleanup timer will ensure any attachments that arrive after their private messages have been deleted will eventually be deleted too. Making this change was easier than updating the attachment format to indicate whether each attachment belongs to a private message with a self-destruct timer, and I think it improves the manual deletion feature.

This branch also contains an unrelated change: moving ConversationManagerImpl to the conversation package (like its interface). I can move this change to a separate MR if preferred.

The default timer duration is changed to 1 minute for easier testing; we should revert this before merging the feature branch.

Part of #1833.

Was WIP because the UI isn't updated when messages are deleted. @grote let's discuss whether to merge this to the feature branch and add the UI changes separately, or add them before merging this MR. We also need to update the onboarding text to explain that the recipient's timer starts when the message is read.

Ticket #1834 Automatically decline incoming introduction requests when they self-destruct (MR 

Testing instructions: 
* Use three devices, users A, B and C
* Enable self-destructing messages in the conversations A-B and A-C
* Let A introduce contacts B and C
* Expect invitation messages to arrive at B and C about the invitation
* Expect the invitation messages to have a auto-delete timers
* Let those timers expire. Expect that to trigger an automatic decline of the invitation, i.e. on all three devices it is visible that the introduction failed (due to the expired response)
* Expect all messages from that interaction to destroy after each message's timer expires
* Let A introduce B and C again. Expect this *not* to fail due to an introduction that is already going on (because none should be going on any longer)
* Let B and C accept the introduction
* Expect the introduction to work
* Confirm that B and C have each other in the contact list
* Expect all messages involved in the transaction to have auto-delete timers
* Let those timers expire and expect all those messages to disappear

Preconditions: 
Use three devices, users A, B and C
A has a contact with B and A has a contact with C. 
B and C are not connected.  


Test Scenario: 
- [ ] A wants to introduce B and C, and A all three of them have the disappearing messages setting set to On. Both B and C accept the introduction.  
- [ ] Expected results: introductions are successful, B and C appear in each other's contact lists, 
- [ ] Sender A receives an automatically generated message saying "Your messages will disappear after x minutes.  Tap to learn more. "
- [ ] Tap on words 'tap to learn more' A screen appears with a message saying "Make future messages in this conversation automatically disappear after 7 days.  The user is able to siwtch the Disappearing messages setting on or off on this screen. User can tap on Learn More link.
- [ ] Tapping on Learn More - An informational screen appears explaining to the user how this setting works.
- [ ] Sender A receives another automatically generated message which contains: The text message they typed into their intro before senidng it+ and underneath that "you have asked to introduce B to C" + bomb icon.
- [ ] Recipient B = the one the sender selected first in their contact list... and from there they selected Make Introduction.
- [ ] Recipient B receives: autogenerated message saying : "A's messages will disappear in 1 min", the text message that A typed to accompany their intro msg, and an atugenerated message aying "A asked to introduced you to C".  Which the user can accept or decline.









B receives a message saying: A has asked to introduce you to C... doesn't have a little bomb icon on. 





- [ ] Timer expired before either B or C have seen the introduction message.  The original automatically generated message that the A received, saying "Your messages will disappear after x minutes.  Tap to learn more. " has disappeared as did the message saying you have sent an introduction message to B and C... 

**Ticket # Automatically decline incoming blog/forum invitations when they self-destruct** 

MR1392 Testing Instructions
Test instructions:

Testing that an invitation to a "shareable" -- a forum, a contact's personal blog, or an imported RSS feed -- is automatically declined when the invitation message is self-destructed.

* use two or more contacts
* create various forums or RSS blogs
  * personal blogs are automatically shared between mutual contacts, but can be shared to a 3rd party
* share items to other contacts
* accept/decline invitations
* enable/disable disappearing messages throughout the process
* ensure that the invitation message to a shareable gets automatically declined and are recognizable as automatic for the contact that declines it

Test Scenario: 
Device A - writes a blog
Device B is a direct contact of device A - they receive the blog directly.  The blog entries cannot be 'self-destructing', only direct messages to direct contacts can. 
Device B is a direct contact of Device C. Disappearing messages = ON on both devices. 
Device B shares a blog wtih device C. 








 













