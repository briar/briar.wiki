### Goals

* Mailbox has a single owner
* Owner manages mailbox's contact list
* Owner and contacts connect to mailbox via Tor hidden service
* Mailbox receives encrypted messages from contacts, stores them for collection by owner
* Mailbox receives encrypted messages from owner, stores them for collection by contacts
* Messages that have not been collected are eventually deleted
* Mailbox runs on Android

### Future goals (outside scope of Sponsor 6)

* Owner connects to mailbox via internet, without Tor
* Owner and contacts connect to mailbox via LAN (or hotspot provided by mailbox)
* Mailbox runs on Linux, Windows and Mac
* Mailbox sends push notifications to wake owner's main device when messages arrive
* Mailbox-to-mailbox forwarding:
   * Alice uploads message to Alice's mailbox
   * Alice's mailbox forwards message to Bob's mailbox
   * Bob downloads message from Bob's mailbox

### Non-goals

* Owner's contacts can send messages to each other via the mailbox
* Strangers can send or receive messages or files via the mailbox

### Architecture

* Mailbox doesn't implement any Bramble-specific protocols
* Owner and contacts communicate with mailbox via HTTP
* Owner and contacts authenticate themselves to mailbox using bearer tokens
* Mailbox relies on Tor hidden service to authenticate itself to owner and contacts
* Mailbox stores encrypted messages in filesystem
* Mailbox stores contact list and message metadata in database
* Database is not encrypted, allowing mailbox to start automatically after booting

### Future architecture (outside scope of Sponsor 6)

* HTTPS with self-signed cert for communicating over internet/LAN
* Owner and contacts need to know mailbox's cert fingerprint
* Owner and contacts need to be able to check fingerprint of received cert

### RSS

* On Android, mailbox can receive RSS feeds shared by other apps in the form of XML files
* Mailbox stores these files for collection by owner
