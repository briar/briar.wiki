# Development 101

A page for things everyone hacking on briar should know. If you don't find something in here,
also check the [Development](https://code.briarproject.org/briar/briar/-/wikis/home#development) section on the main Wiki page.

## A few hints for the development/debug workflow

### Entering your password automatically

During development you may find yourself having to enter your briar password very often which can be tedious. It can be helpful to enter the password from your development machine's command line instead:

    adb shell input text your-password && adb shell input keyevent 66

### Running multiple versions of Briar on a single device

Running two versions of Briar side by side on one device does not work out
of the box. For example it is not possible to run the released version and
the development version at the same time; only one of both will be online.
The reason is that both versions are trying to use the same hard-coded
Tor ports to create their hidden service, which doesn't work. The solution
is to use different ports on the development versions. To do so, change the
following:

In `bramble-api/src/main/java/org/briarproject/bramble/api/plugin/TorConstants.java`:

    - int SOCKS_PORT = 59050;
    - int CONTROL_PORT = 59051;
    + int SOCKS_PORT = 59060;
    + int CONTROL_PORT = 59061;

In `bramble-core/src/main/resources/torrc`:

    - ControlPort 59051
    + ControlPort 59061
      ...
    - SocksPort 59050
    + SocksPort 59060

## Fixing common problems

### Working with the Gradle Witness

The project uses the Gradle Witness to make sure third party dependencies have not been tampered with. If you encounter problems with this feature, for example after upgrading dependencies, it may be required to recalculate the checksums:

    ./update-dependency-pinning.sh

## Coding style

There's an article on the
[code-style](https://code.briarproject.org/briar/briar/-/wikis/code-style)
used in the project.

The coding style configuration is checked into version control at
`.idea/codeStyleSettings.xml` so that it will be picked up automatically by
Android Studio. AS does not automatically format your files when changing
them, so you need to remember to use `Ctrl+Alt+L` to format and also
`Ctrl+Alt+O` for organizing your imports.

If you find that tedious, there are some alternatives:
* Install the Save Actions plugin and configure it to perform those actions
  while saving your file.
* Use AS to commit changes to git. If you do so, there's a small preferences
  button on next to the commit button which let's you configure actions such
  as 'Reformat code' and 'Optimize imports' that happen at commit time.

### Save Actions Plugin

The Save Actions Plugin needs some configuration to work as expected:

* Go to `settings` → `Other Settings` → `Save Actions`
  * In the `General` section, enable 'Activate save actions on save'
  * In the `Formatting Actions` section, enable
    * 'Optimize imports'
    * 'Reformat file'
    * 'Rearrange filds and methods' (important for xml attribute order etc.)
  * In the `Java IInspection and Quick Fix` section, enable
    * 'Add missing @Override annotations'

Enabling the 'Rearrange fields and methods' option is nice for automatically
rearranging attributes in XML files, however it also enables rearranging of
Java source files, which can cause undesired changes in existing files.
To prevent that, navigate to `Editor` → `Code Style` → `Java`, select the
`Arrangement` tab and remove all the Matching rules defined in the
bottommost section.

## Coding patterns

### Patterns for visibility and interfaces

As an addition to the points noted in the
[pre review checklist](https://code.briarproject.org/briar/briar/-/wikis/pre-review-checklist):

the convention we usually follow is that classes should be package-private. If
they're used outside their package, it should be via a public interface, with
the impl bound to the interface via Dagger.

There are a few exceptions to this convention:

* Activities, fragments, etc generally need to be public so the framework can
  instantiate them
* Dagger modules need to be public
* JUnit tests need to be public
* bramble-api and briar-api contain some public "data classes" (eg Contact) -
  these are immutable and shouldn't contain any significant logic
* There are a handful of public classes in bramble-api and briar-api that do
  contain significant logic (eg BdfDictionary) - these are so widely used that
  injecting them would add a lot of boilerplate
* We have a few public utility classes (eg StringUtils) with static utility
  methods to reduce code duplication

Additionally we sometimes also use interfaces within packages.
It can help establishing a clearer separation of concerns and easier testing
when there's complex interaction between classes.
