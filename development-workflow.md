## Tickets

### Ticket lifecycle

* When we decide to work on a ticket, it should be assigned to a milestone
* Some tickets in the current milestone are given high priority
* High-priority tickets are eligible to be worked on
* When you start working on a ticket, assign it to yourself and label it `In progress`
* When you create a merge request, label the ticket `In code review`
* When your branch is merged, close the ticket if necessary, and label it `Fixed`

### Parent and child tickets

* If a ticket involves more than one person, or more than one week of work, divide it into child tickets
* Mention the parent ticket in the child tickets
* Close the parent ticket and label it `Fixed` when all the child tickets have been fixed

### Categories

* Every ticket should be labelled `Bug`, `Feature`, `Feature request`, `Task`, `Document` or `UX design`
  * `Bug`: a defect that affects the user, e.g. a potential crash
  * `Feature`: a user-visible improvement
  * `Feature request`: a feature someone has asked for -- we might or might not implement it, but either way the request should be recorded
  * `Task`: work that isn't visible to the user, e.g. refactoring or research
  * `Document`: work that produces a document
  * `UX design`: work that produces a design mockup
* Before assigning the `UX design` label, ask yourself whether the ticket can be closed when a mockup is produced. If the answer is yes, assign the label. If the answer is no, create a child ticket for the mockup and assign the label to the child.
* Tickets like `Usability` and `Security` can be assigned to any ticket regardless of whether it's a bug, feature, etc.

### Resolutions

* Every closed ticket should be labelled `Fixed`, `Duplicate` or `Rejected`
  * `Fixed`: the issue described by the ticket was addressed
  * `Duplicate`: another ticket describes the same issue -- add a comment mentioning which ticket
  * `Rejected`: the issue described by the ticket is obsolete, or we're not planning to address it -- add a comment explaining why not

## Branches

* Always work on a branch
* Name your branch after the ticket you're working on, e.g. `55-key-manager-refactoring`
* Push your branch at least once a day so everyone knows what you're working on -- that will help us to avoid conflicts

#### When your branch is ready to merge

* Make sure **all** tests are passing
* Check the [pre-review checklist](pre-review-checklist)
* Rebase your branch if necessary to fix any merge conflicts: `git rebase -i master`
* After rebasing, use `git push -f` to overwrite the remote branch
* [Open a merge request](https://code.briarproject.org/briar/briar/merge_requests/new)

## Code review

* At least one other person must review your code before it's merged to master
* You should respond to all the reviewer's comments before merging
  * If an issue needs to be fixed before merging, push the fix to your branch and notify the reviewer
  * If an issue is better dealt with after merging, create a ticket and reply with the ticket number
* Once your branch has been merged you can delete the remote branch

#### When reviewing a branch

* Check out and run the code as well as reading it
* Make sure you understand what the code is doing and why
* Ask plenty of questions
* The goal is to ensure that you and the developer have a shared understanding of the code

## Tests

* To run the tests in Android Studio:
  * Select `All tests` from the run configuration dropdown
* To run the tests from the command line:
  * `./gradlew test --continue`

## Meetings

* At the start of each week, let the team know what days you'll be working via the standup channel
* Post a daily update to the standup channel saying:
  * What you worked on yesterday/today
  * What you'll be working on today/tomorrow
  * Any blockers
* If you're awake and online at 10am UTC, please post your update at that time so we can have a standup meeting
* We have a planning meeting in the town square channel each Monday at 2pm UTC

## Dependencies

* If the ticket you're working on depends on an earlier ticket that's in code review, base your new branch on the branch that's awaiting review
* When the first branch is merged, rebase the second branch onto master
* You can put the second branch up for review by selecting the first branch as the target, instead of master
* The second merge request should be marked WIP until the first one is merged
* Mention the dependency in the second merge request
* Remember to update the target branch of the second merge request when the first branch is merged
