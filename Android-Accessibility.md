When making new Android UI, please keep accessibility in mind.

## Upstream Documentation

* [Principles for improving app accessibility](https://developer.android.com/guide/topics/ui/accessibility/principles)
* [Make apps more accessible](https://developer.android.com/guide/topics/ui/accessibility/apps)

## Simple tips

Make sure images have a `contentDescription`.

If the image is only decorative, use `android:importantForAccessibility="no"` instead of `tools:ignore="contentDescription"`.

Setting `android:screenReaderFocusable` attribute to `true` in the root view of list item makes the screenreader read all non-focusable items inside. Note this is only available on API level 27. Using `android:focusable` might have the same effect (needs checking!).

Otherwise, items that are not focusable (or are not screenReaderFocusable) are in most cases invisible to screenreaders and thus visibly impaired users as well.