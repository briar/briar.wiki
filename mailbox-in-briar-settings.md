### Scenario 1


 ![device-2022-02-08-150654](uploads/24bf60aa39ff5cc26964c38b5afbadc6/device-2022-02-08-150654.mp4)
- In Briar app, go to Settings and verify that the option Mailbox exists
- tap on Mailbox
- Verify that the the mailbox onboarding screen opens
- Navigate back to Briar settings
- double tap the Mailbox settings, 
- Rotate the device to landscape whilst the page is opening
- Verify that the page opens correctly and displayed information is readable, accessible etc
- tap the back button
- verify that user is returned to Briar settings
- tap the Mailbox screen again and then put Briar into the background
- go to another app or device settings, bring Briar into the foreground
- tap Continue
- Sign out of Briar
- Sign back in


[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)