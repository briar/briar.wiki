When Briar app tries to unlink its mailbox AND the mailbox is reachable, this process will trigger the mailbox wiping

**Steps to run**
- link the briar app with a mailbox.  Then on Mailbox status screen, tap Unlink.
- when unlink is tapped in the Briar app, user will get a dialog box informing them that their mailbox has been unlinked.  To close the dialogue box they need to tap onto Got it. 
- Then user can go back to briar > settings > mailbox and they can scan the QR code again to create a new link with the same or different mailbox

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)