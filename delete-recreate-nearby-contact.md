**Steps to execute**

* Install Briar afresh on twodevices
* Add a neaby contact on each device
* On one device - delete the contact
* Then add it again via nearby method

**Expected results**

* Contact should be added on the device that has deleted it previously + success notification sayys: _ContactName_ added.
* On the othe device that has not deleted the contact, it should not be added again.  This device receives notification that the contact in question already exists.
* Contact should be able to see each other online - the online indicator should be green

However, currently, thsi is not possible in Briar.  If a contact is deleted on one device it cannot be re-added.  Only if both contacts delete each other from their phones, they can be added again.  

For the user this is currently confusing, as they are not given any messages to let them know that re-adding of one contact will not be successful, so they may be wondering what is wrong when they finish re-adding their contact, but still cannot message them.  This is a known issue reported in the ticket number #2.

This issue also means that the contacts in question cannot be connected via bluetooth, but this is not clear to the user - reported in ticket #2068

The process of reconnection runs OK, and the user gets the success notification in this case, but the contacts still do not see each other online and the online indicator is not green.  Sending messages to each other is still not possible. 

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)






