THESE SCENARIOS CAN BE REPEATED FOR DIFFERENT TYPES OF MESSAGES/

* TEXT
* ATTACHMENT
* PROFILE PICTURES
* PRIVATE GROUPS
* BLOGS
* FORUMS
* IMPORT RSS
* INTRODUCTIONS However, this is not strictly speaking necessary as all messages are treated the same - so if one type gets uploaded it means that all of them will. (Most recent mention of that in the testing channel in MM, on 19/09/22)

The expected results are defined here 
[Mailbox scenarios](https://code.briarproject.org/briar/briar/-/wikis/Mailbox-Scenarios)

Scenarios:

When X has a mailbox, and Y knows about X's mailbox:

* X will upload messages and acks for Y to X's mailbox
* Y will upload messages and acks for X to X's mailbox
* X will check X's mailbox for messages and acks uploaded by Y
* Y will check X's mailbox for messages and acks uploaded by X
* Communication via X's mailbox will work in both directions

### Scenario 3: X has a mailbox that Y knows about

In this scenario, X and Y become contacts and then X links a mailbox (M) while Y is online. Y learns about X's mailbox, because there has been a connection between X and Y since X linked the mailbox. Then Y goes offline.

While Y is offline, X writes a message to Y. X's message is uploaded to X's mailbox (one tick). Then X goes offline and Y comes back online. Y knows about X's mailbox, so Y downloads X's message from the mailbox.

While X is offline, Y writes a message to X. Y knows about X's mailbox so Y's message is uploaded to X's mailbox (one tick), along with an ack for X's message. Then Y goes offline and X comes back online. X downloads Y's message and the ack for X's message from X's mailbox. X's message is shown as sent and acked (two ticks). X uploads an ack for Y's message to X's mailbox. Then X goes offline and Y comes back online.

While X is offline, Y downloads the ack for Y's message from X's mailbox. Y's message is shown as sent and acked (two ticks).

* Install Briar on X and Y
* Add X and Y as contacts
* In Briar's connection settings, turn off wifi and Bluetooth connections on X and Y
* Install Mailbox on M
* Link Briar running on X with Mailbox running on M
* Wait a few seconds for X's update informing Y about X's mailbox to be delivered to Y
* Take Y offline
* X writes a message to Y
* Expectation: the message is shown as sent (one tick) in X's conversation screen (after a delay of up to 2 minutes for X to connect to M and upload the message)
* Take X offline
* Bring Y back online
* Expectation: X is shown as offline in Y's contact list
* Expectation: X's message appears in Y's conversation screen (after a delay of up to 2 minutes for Y to connect to M and download the message)
* While X is still offline, Y writes a message to X
* Expectation: Y's message is shown as sent (one tick) in Y's conversation screen (after a delay of up to 2 minutes for Y to connect to M and upload the message)
* Take Y offline
* Bring X back online
* Expecation: Y is shown as offline in X's contact list
* Expectation: Y's message appears in X's conversation screen (after a delay of up to 2 minutes for X to connect to M and download the message)
* Expectation: X's message is shown as sent and acked (two ticks) in X's conversation screen (after a delay of up to 2 minutes for X to connect to M and download the ack)
* Take X offline
* Bring Y back online
* Expectation: X is shown as offline in Y's contact list
* Expectation: Y's message is shown as sent and acked (two ticks) in Y's conversation screen (after a delay of up to 2 minutes for Y to connect to M and download the ack)

Executed with the following devices and versions of software

* Briar build dd3a9aa71ba44293be2c05e853c5cb652ce6bf67
* Mailbox build fe5453902347ef7ee91e24b373d340e6a9921f12

Devices:

1. Samsung Mini 9195 Android 4.4.2 Briar (X) Samsung A01S Core Android 10 Briar (Y) HTC One M9 Android 7 (M)

= OK


2. Motorola E2 Android 6 (X), XIAOMI Mi 11 Lite 5G Android 11 (Y) HTC E9 Android 5.0.2 (M) , 
= OK

3. Samsung 6810s Android 4.1.2 (X) Huawei CDY NX9A Android 10 (Y), PIxel2 Android 11 (M) 
= OK


[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/Testing)