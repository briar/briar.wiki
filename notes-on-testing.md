- test Mailbox screens when user selects large font
- test Mailbox screens on different screen types/sizes/ratios, portrait and landscape, all devices.  This is a 'nice to have', as the cosmetics for different screens can be dealt with at a later stage and not necessarily in the MVP.
- according to the mattermost discussion of 13/14 dec, there may be variations of illustration size used on mailbox screens - normal, short and none... if that decision is made, then that will need to be tested too

- discussion on the MM 20/12 - error handling.  Find out how to trigger each error response mentioned, to verify that they are handled correctly.
[Maibox_coverage_matrix.ods](uploads/6eb0d54b9d22dc1cd5eb870964ee106d/Maibox_coverage_matrix.ods)
- https://code.briarproject.org/briar/briar/-/issues/2191 will give rise to a few scenarios
- https://code.briarproject.org/briar/briar/-/issues/2226 also some scenarios when uploads do not complete successfully
- verify that the messages are not sent to mailbox (own or contact's when contact is online - done
- various scenarios of who is online and offline and what happens to messages in each case - done

- find out what needs to be tested re:Tor.  

- to test what happens when different api versions are present in own and ocntact's mailbox or briar app... see discussion on MM,channel, today 4/4/22 

"one possibility here, which i haven't fully thought through, is to use the existing client versioning mechanism to signal our support for mailbox versions. so api version 0.0 would be tied to mailbox properties client version 0.0, etc. api version 0.x would be backward compatible with 0.0, and 1.x would break compatibility with 0.x

so by looking at the client versions received from each contact we could work out whether the contact would be able to communicate with our mailbox, and if the contact sent us properties (in one or more of the major versions we support, since each major client version has its own independent group for exchanging properties) we'd know that we could communicate with that mailbox using the major api version corresponding to the major client version and the minor api version corresponding to min(our minor client version, contact's minor client version)

but all of this could also be done by including our own client and server api versions in our mailbox properties, or in a second type of update message exchanged by the mailbox properties client, and in that case it wouldn't be tied to the client version. which is maybe more sensible?

12:31 PM






(by client and server api versions, i mean the versions our briar app can speak as a client, and the versions our mailbox app can speak as a server. we need to send both if we want a contact to know whether we'll be able to talk to their mailbox)"


- see discussiojn on MM on Friday 01/04/22 - that will likely need to be tested too.
I wonder how we can make the user aware of a problem with their mailbox that requires their attention, like if it has been unreachable for too long.

Show a nagging snackbar in the main activity?

Show a button or icon in the nav drawer?

Use a system notification?

5:56 PM

A dialog that pops into the user's face?

- uninstall briar without unlinking with mailbox, and uninstall mailbox without first unlinking to briar.  The reinstall briar and try to link with the same mailbox instance s before, or reinstall mailbox and try to link with the same instance of briar as before.  
- try to link briar to one instance of mailbox, then unlink, and link with another. same for mailbox side - unlink from one instance of briar and link with another?
- does it matter in which language briar is? 

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)