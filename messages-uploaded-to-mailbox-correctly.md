THESE SCENARIOS CAN BE REPEATED FOR DIFFERENT TYPES OF MESSAGES/ 

- TEXT
- ATTACHMENT
- PROFILE PICTURES
- PRIVATE GROUPS
- BLOGS
- FORUMS
- IMPORT RSS
- INTRODUCTIONS
However, this is not strictly speaking necessary as all messages are treated the same - so if one type gets uploaded it means that all of them will.  (Most recent mention of that in the testing channel in MM, on 19/09/22)

Mailbox screens do not show any messages, therefore the user cannot know if their messages were correctly uploaded or not, either to their own or to the contact's mailbox.  For the user, that is all black box, and they only see the clock, or one or two ticks below each message to tell them what the status of that message is. 

A user will not know who of their contacts does or does not have a mailbox linked - they always only see the status of their messages via a clock icon (unsent message), one tick (sent but not acknowledged) or two ticks (send and acknowledged), and status of their contacts being reachable or not (green circle for reachable ones). 


The expected results are defined here [Mailbox scenarios](https://code.briarproject.org/briar/briar/-/wikis/Mailbox-Scenarios)
 
Scenarios: 

When X has a mailbox, but Y doesn't yet know about X's mailbox:

* X will upload messages and acks for Y to X's mailbox
* X will check X's mailbox for messages and acks uploaded by Y
* Y won't upload anything to X's mailbox, or download anything from it, as Y doesn't yet know about X's mailbox
* Communication via X's mailbox won't work in either direction
* This problem will resolve itself when Y receives X's update informing Y about X's mailbox, which will happen the next time X and Y are online at the same time

### Scenario 2: X has a mailbox that Y doesn't know about

In this scenario, X and Y become contacts and then Y goes offline. While Y is offline, X links a mailbox (M). Y doesn't know about X's mailbox, because no connection has happened between X and Y since X linked the mailbox.

While Y is offline, X writes a message to Y. X's message is uploaded to X's mailbox (one tick). Then X goes offline and Y comes back online. Y doesn't download X's message from X's mailbox because Y doesn't know about the mailbox yet.

While X is offline, Y writes a message to X. Y's message is not uploaded to X's mailbox (clock icon), because Y doesn't know about the mailbox yet. Then X comes back online.

When X and Y are online at the same time, both messages are sent and acked (two ticks).

* Install Briar on X and Y
* Add X and Y as contacts
* In Briar's connection settings, turn off wifi and Bluetooth connections on X and Y
* Take Y offline
* Install Mailbox on M
* Link Briar running on X with Mailbox running on M
* X writes a message to Y
* Expectation: the message is shown as sent (one tick) in X's conversation screen (after a delay of up to 2 minutes for X to connect to M and upload the message)
* Take X offline
* Bring Y back online
* Expectation: X is shown as offline in Y's contact list
* Expectation: X's message does not appear in Y's conversation screen
* Y writes a message to X
* Expectation: Y's message is shown as not sent (clock icon) in Y's conversation screen
* While keeping Y online, bring X back online
* Expectation: X is shown as online in Y's contact list and vice versa (after a delay of up to 2 minutes for X and Y to connect to each other)
* Expectation: X's message appears in Y's conversation screen
* Expectation: X's message is shown as sent and acked (two ticks) in X's conversation screen
* Expectation: Y's message appears in X's conversation screen
* Expectation: Y's message is shown as sent and acked in Y's conversation screen


Executed with the following devices and versions of software

- Briar build 34815eb1a51d3aac2f0978d0066c694c963afc3b
- Mailbox build fe5453902347ef7ee91e24b373d340e6a9921f12

Devices: 
1) Samsung Mini 9195 Android 4.4.2 Briar (X)
Pixel 2 Android 11 Briar (Y)
Nokia 3.2 Android 10 Mailbox

= OK

Executed with the following devices and versions of software

- Briar build 04011e50bc92f4c220187c93dc7330fae2cb96c9
- Mailbox build fe5453902347ef7ee91e24b373d340e6a9921f12

2) 
HTC One m9 Android 7 (x)
Samsung Core A01s Android 10 (y)
Motorola E2 Android 6 (M)

= OK

3) Huawei CDY NX9A Android 10 (X)
XIAOMI Mi 11 Lite 5G Android 11 (Y)
Samsung 6810 S Android 4.1.2 (M)

= OK






[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/Testing)


