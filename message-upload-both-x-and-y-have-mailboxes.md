**When X and Y both have mailboxes, and both know about each other's mailboxes:**
reference: https://code.briarproject.org/briar/briar/-/wikis/Mailbox-Scenarios

* X will upload messages and acks for Y to Y's mailbox
* Y will upload messages and acks for X to X's mailbox
* X will check X's mailbox for messages and acks uploaded by Y
* Y will check Y's mailbox for messages and acks uploaded by X
* Communication will work in both directions. Messages and acks from X to Y will travel via Y's mailbox, while messages and acks from Y to X will travel via X's mailbox

### Scenario 4: X and Y both have mailboxes that the other knows about

In this scenario, X and Y become contacts and then X links a mailbox (M) and Y links a mailbox (N) while X and Y are online. X and Y learn about each other's mailboxes, because there has been a connection between X and Y since they linked their mailboxes. Then Y goes offline.

From this point onwards, this scenario is identical to scenario 3 from the user's point of view. Some of the messages and acks will travel through X's mailbox and others through Y's mailbox, but the difference is not visible to the user provided both mailboxes are reachable.

While Y is offline, X writes a message to Y. X's message is uploaded to the Y's mailbox (one tick), because X knows about Y's mailbox. Then X goes offline and Y comes back online. Y downloads X's message from Y's mailbox.

While X is offline, Y writes a message to X. Y's message is uploaded to X's mailbox (one tick), along with an ack for X's message, because Y knows about X's mailbox. Then Y goes offline and X comes back online. X downloads Y's message and the ack for X's message from X's mailbox. X's message is shown as sent and acked (two ticks). X uploads an ack for Y's message to Y's mailbox. Then X goes offline and Y comes back online.

While X is offline, Y downloads the ack for Y's message from Y's mailbox. Y's message is shown as sent and acked (two ticks).

* Install Briar on X and Y
* Add X and Y as contacts
* In Briar's connection settings, turn off wifi and Bluetooth connections on X and Y
* Install Mailbox on M and N
* Link Briar running on X with Mailbox running on M
* Link Briar running on Y with Mailbox running on N
* Wait a few seconds for the updates informing X and Y about each other's mailboxes to be delivered
* Take Y offline
* X writes a message to Y
* Expectation: the message is shown as sent (one tick) in X's conversation screen (after a delay of up to 2 minutes for X to connect to N and upload the message)
* Take X offline
* Bring Y back online
* Expectation: X is shown as offline in Y's contact list
* Expectation: X's message appears in Y's conversation screen (after a delay of up to 2 minutes for Y to connect to N and download the message)
* While X is still offline, Y writes a message to X
* Expectation: Y's message is shown as sent (one tick) in Y's conversation screen (after a delay of up to 2 minutes for Y to connect to M and upload the message)
* Take Y offline
* Bring X back online
* Expecation: Y is shown as offline in X's contact list
* Expectation: Y's message appears in X's conversation screen (after a delay of up to 2 minutes for X to connect to M and download the message)
* Expectation: X's message is shown as sent and acked (two ticks) in X's conversation screen (after a delay of up to 2 minutes for X to connect to M and download the ack)
* Take X offline
* Bring Y back online
* Expectation: X is shown as offline in Y's contact list
* Expectation: Y's message is shown as sent and acked (two ticks) in Y's conversation screen (after a delay of up to 2 minutes for Y to connect to N and download the ack)

29/09/2022 build 84d336a47ff545be05f374bf7a5fae855f2f3c3d unable to start mailbox, reported in MM... 


[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)
