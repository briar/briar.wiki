test adding contacts nearby with focus on the permission prompts before the camera appears.

* android 12+ should ask for nearby devices permission, but not location permission
* old android should ask for location permission
* when location permission is denied, a dialog should offer the user to go to app system settings

### Scenario 1 - Positive 

- Verify that Briar debug doesn't have any permissions granted in devices > settings > apps > pBriar debug > permissions
- in Briar app go to add new nearby contact
- Dialog box asks the user to give Briar permission to access: 
   - camera - tap Allow
   - nearby devices - tap Allow
   - A message to the user that Birar will make the device visible to other devices during 120s - tap OK
   - Camera opens up which can scan the nearby contact's QR code, and the QR code to be scanned by the nearby contact
   
Result = OK
Build 25f99bd7decfb1494462e54c7f92d2a11e025df0

### Scenario 2 - Negative 

Verify that Briar debug doesn't have any permissions granted in devices > settings > apps > pBriar debug > permissions
- in Briar app go to add new nearby contact
- Dialog box asks the user to give Briar permission to access: 
   - camera - tap Allow
   - nearby devices - tap Don't allow
   - A message to the user that Birar they have denied Briar access to nearby devices, but Briar needs this permission.  Please consider granting access.  Tap OK.
   - Device app settings open, and the user can grant the Briar app access to nearby devices.  
   - User decides not to grant it. 
   - User goes back to Briar, logs in and retart the process of creating nearby contact. 
   - Dialog is displayed, asking th euser to Allow Briar debug to find, conntect and determine relative position... User taps Allow
   - message: Briar debug is askign to turn onteh bluetooth This will allow your phone to be visible to others during 120 secs. User taps allow.  
   - Camera + QR screen is displayed.
   - Nearby contact can be created successfully

Result = OK

The above scenario on Android 7 is also OK, on the same build.Android 7 is asking for permission to access location, and not nearby devices, which is as expected.


   - Camera opens up which can scan the nearby contact's QR code, and the QR code to be scanned by the nearby contact

### Scenario 3 - Negative 

New Installation of Briar debug
Verify that Briar debug doesn't have any permissions granted in devices > settings > apps > pBriar debug > permissions
- in Briar app go to add new nearby contact
- Dialog box asks the user to give Briar permission to access: 
   - camera - tap Allow
   - nearby devices - tap Don't allow
   - A message comes up, Nearby Devices Permission' informing the user that To use the bluetooth communication, Briar needs permission to find and connect to nearby devices.  User taps continue (as that is the only option here).  (This message appears only after the user performs the 'create nearby contact' for the first time on a new installation.  )
- Dialog box asks the user again to allow Briar access to nearby devices. 
- A message to the user that Birar they have denied Briar access to nearby devices, but Briar needs this permission.  Please consider granting access.  Tap OK.
   - Device app settings open, and the user can grant the Briar app access to nearby devices.  
   - User decides to grant it. 
   - user goes back to Briar using the app switcher
   - message: Briar debug is asking to turn on the bluetooth This will allow your phone to be visible to others during 120 secs. User taps allow.  
   - Camera + QR screen is displayed.
   - Nearby contact can be created successfully

Result = OK

The above scenario on Android 7 is also OK, on the same build.Android 7 is asking for permission to access location, and not nearby devices, which is as expected.


   - Camera opens up which can scan the nearby contact's QR code, and the QR code to be scanned by the nearby contact

[Back to testing](https://code.briarproject.org/briar/briar/-/wikis/testing)