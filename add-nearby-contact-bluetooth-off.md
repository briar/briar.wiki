### Add nearby contacts when Bluetooth settings is off both on Briar itself and on the device

**Device settings:**
- Device 1 Briar location permission = On
- Device 2 Briar location permission = On
- Bluetooth setting on Briar app = Off
- Bluetooth setting on device itself (Connection preferences) = Off

**Briar settings:**
- Device 1 - connect via Bluetooth = On 
- Device 2 - connect via Bluetooth = Off


* Start the process by tapping on + on the contacts screen. 
* Select the 'nearby contacts' and tap 'continue' when asked.
* message "Briar is asking to allow phone to be visible to other devices for 120 seconds" - tap "allow"
* Both bluetooth settings are turned on by Briar (both within the app and on the device)
* QR code is shown in half of the creen and the other half is a camera's screen, ready to take a photo of the QR code of the neaby contact's device.
* Allow the 120 seconds to expire.  Then attempt to continue the process.

**Expected results:** 
* After 120 seconds - Process can be completed
* Users appear in each other's contact lists
* Bluetooth settings are On, in the Briar app as well as in the phone connection preferences


[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)

