User attempts to import the message file that as generated from essages written to another user. 

**Expected results:** 

User is given a message 'Error importing data' and an option to tap on the Try again button

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)