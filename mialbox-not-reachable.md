In different circumstances mailbox can become unreachable by Briar: 
- when mailbox does not have internet access
- when mailbox reboot takes too long (threshold??? **TBD**)

"yes, i think if you reboot the mailbox device and the mailbox app automatically restarts as intended, then briar is unlikely to reach the threshold where it shows a notification to warn about the mailbox being unreachable

1:48 PM
we haven't decided exactly what that threshold should be - that's to be decided during testing - but ideally it should be high enough that we don't unnecessarily bother the user when there's a temporary issue like the mailbox rebooting"

Is the message to the Briar user going to be the same in all the different cases?

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)