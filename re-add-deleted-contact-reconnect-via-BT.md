This test is linked to the one that deletes and recreates the deleted contact, then tried to connect via BT.. Link below.

[Re-add deleted contact and reconnect via BT](https://code.briarproject.org/briar/briar/-/wikis/delete-recreate-nearby-contact)

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)