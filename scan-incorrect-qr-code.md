- Attempt to scan an incorrect QR code - maybe the one that briar generates for sharing the app offline, or another unrelated QR code. 

Expected results: 

A screen should be shown to the user informing them that the QR code is incorrect. 
There should be a button or link allowing the user to try again.

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)

