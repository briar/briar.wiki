After the Brar user has received the notification that mailbox is unreachable, this notification should disappear when its linked mailbox become reachable again. 

To obtain the notification, install the maibox and link it to a Briar device.  Once linked, take the mailbox device offline - which means that Briar will not be able to reach it. 

Briar will periodically try to reach its mailbox again, and after awhile (usually a few hours), it will disaply this notification. 

When the notification is displayed bring the mailbox device back online, without any intervention on the Briar side. 

Briar will still periodically try to reach the mailbox and when it does so successfully, the notification that tells the user that mailbox is unreachable, will disappear.  

Tested OK, build c855967d56c53d2c5e0ba5d28b6357d521c90e61

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)