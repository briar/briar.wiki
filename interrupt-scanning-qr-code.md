After installing the Mailbox app, and after the Onboarding screens, plus do-no-kill-me fragment, a QR code is displayed on the mailbox app screen

**App in background** 
- whilst the QR code is displayed, go to device settings app (to push the mailbox into the background), then come back to the same mailbox screen
- navigate back from QR code
- go offline (device or wi-fi) whilst the QR code is disaplyed
- stop the app during the scanning or immediately afterwards
- turn the screen to landscape during or immediately after the scanning
- tap the home button on device during the scanning, or immediately afterwards

The handling of all these situations needs to be graceful - either the use can continue the process, or restart it.  
There should be no crashes.

New: Another way to interrupt the QR code scanning is from the Briar side - navigate away from the 'progress' screen, while the message Connecting is displayed... Then try to scan the QR code again... 



[Back to testing](https://code.briarproject.org/briar/briar/-/wikis/testing)