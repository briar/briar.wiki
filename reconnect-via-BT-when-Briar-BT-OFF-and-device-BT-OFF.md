### Verify that bluetooth connection can be made when bluetooth setting in the device itself as well as in the Briar app is OFF
**Steps to execute:**
* Add contact at a distance
* Switch the internet as well as wifi off in Briar app, as well as in devices used
* Switch the Bluetooth setting off in Briar app as well as in devices used
* Contact should be showing as offline
* Select the previsouly added contact and go to menu (upper right hand corner) and select Connect via Bluetooth option
* Follow onscreen instructions

**Expected results:**
* BT on the device is restarted and connection made OK
* BT setting on Briar app is turned ON during this process
* Contact are showing as online
* Contact can message each other

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)