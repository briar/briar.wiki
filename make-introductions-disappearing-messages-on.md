Test objective: verify that behaviour of all messages on all devices is as expected.

Set up: 

Contacts: 
Sender <-> Recipient1 (disappearing messages = ON) and 
Sender <-> Recipient2 (disappearing messages = ON), 
no contact between Recipient1 and Recipient2, but disappearing messages on their devices is still ON.

Steps to execute: 
Sender selects a contact (Recipient1) and then selects the Make introductions option for that contact. 
Sender selects the other contact that they want to introduce Recipient1 to. 
Sender types a text message to both contacts and sends the introduction. 

Expected results: 

Sender's device
- [ ] Sender can add an option text message to the introduction.  The text input field here does not contain the bomb icon and the placerholder text (new disappearing message) like it does when a private disappearing message is being created - this decision was made in order to avoid ambiguity in cases where introductions are sent to multiple contacts, some of which may be disappearing and some not.
- [ ] Sender gets an automatically generated notice that their messages will disappear after 7 days and there is a 'Tap to learn more'.  Tapping leads them to the Diesappearing messages screen.
- [ ] Sender's text message is accompanied by the automatically generated message "you have asked to introduce Recipient1 to Recipient2" and this auto msg contains the bomb icon
- [ ] Sender's messages disappear from their screen after the timer expires.
- [ ] When Recipient1 accepts the introduction, sender's device has these messages in the conversation screen with Recipient1:
- [ ] Automatically generated message "Recipient1's messages will disappear after 7 days.  Tape to learn more."
- [ ] Automatically generated message "Recipient1 accepted the introduction to Recipient2" with a little bomb icon.
- [ ] After Recipient 2 accepts the introduction, in the conversation screen with Recipient2, Sender sees these messages: 
- [ ] Automatically generated message "Recipient2's messages will disappear after 7 days.  Tape to learn more."
- [ ] Automatically generated message "Recipient2 accepted the introduction to Recipient2" with a little bomb icon.
- [ ] All messages disappear after the timer expires.


Recipient1's device
- [ ] Recipient1 > Contact list >  there is a new message for them from Sender 
- [ ] Recipient1 opens that message and they see: 
- [ ] Automatically generated message "Sender's messages will disappear after 7 days; Tap to learn more." 
- [ ] Tapping on Learn more taked them to the Disappearing Messages setting screen. 
- [ ] Navigating back from there brings them to the messages screen.
- [ ] Text message from Sender accompanied by: "Sender has asked to introduce you to Recipient2.  Do you want to add Recipient2 to your contact list?" Accept and Decline are tappable. There is a little bomb icon on this message.
- [ ] Recipient1 accepts the invtroduction
- [ ] They see an automatically generated message: 3Your messages will disappear after 7 days. Tap to learn more."
- [ ] Another automatically generated message: "You accepted the introduction to Recipient2. Before Recipient2 gets added to your contacts, they need to accept the intro as well.  This may take some time."  The small  bomb icon is present at the bottom of this message.
- [ ] Recipient2 is not added yet to the Recipient1's contact list.
- [ ] The above messages disappear after the timer expires.
- [ ] Recipient2 gets added to the Recipient1'scontact list after they accept the introduction.
- [ ] Default Disappearing messages setting for all new contacts is OFF. 


Recipient2's device
- [ ] Recipient2 > Contact list >  there is a new message for them from Sender 
- [ ] Recipient2 opens that message and they see:
- [ ] Automatically generated message "Sender's messages will disappear after 7 days; Tap to learn more." 
- [ ] Tapping on Learn more taked them to the Disappearing Messages setting screen. 
- [ ] Navigating back from there brings them to the messages screen.
- [ ] Text message from Sender accompanied by: "Sender has asked to introduce you to Recipient1.  Do you want to add Recipient1 to your contact list?" Accept and Decline are tappable. There is a little bomb icon on this message.
- [ ] Recipient2 accepts the introduction
- [ ] They see an automatically generated message: "Your messages will disappear after 7 days. Tap to learn more."
- [ ] Another automatically generated message: "You accepted the introduction to Recipient1. Before Recipient1 gets added to your contacts, they need to accept the intro as well.  This may take some time."  The small  bomb icon is present at the bottom of this message.
- [ ] Recipient1 is added to the Recipient2's contact list (because Recipient1  has already accepted the introduction).
- [ ] The above messages disappear after the timer expires.
- [ ] Default Disappearing messages setting for all new contacts is OFF.  




 



