This test will verify that the Briar app and mailbox behaviour is as expected when a sender sends invitation to join their private group to multiple contacts, some of whom are online, and some offline, and some of whom have MB and some do not, and yet others install MB after the invitations have already been sent. Some contact reply via MB, some via exporting replies, some via internet and some via BT.

Also, it can be repeated with the sender exporting the invitations to removable media to some contacts, and those contacts replying the same way, or replying via their MB, or direct messages, or BT. 

Both Invitations and replies can be sent via: 
- direct messages/internet  (with or without the paired mailbox app) 
- direct messages BT (with or without the paired mailbox app)  
- own mailbox (which can be done before or after exporting to removable media)
- exporting to and importing from removable media (which can be done before or after upload to Mailbox)

Who is online to start with?

- Sender = online (briar device 1)
- Rec1 = online (briar device 2)
- Rec2 = offline (briar device 3)
- Rec3 = offline (briar device 4)
- Rec4 = offline (briar device 5)

Has Mailbox?
- Sender = yes (mailbox device 1)
- Rec1 = yes (mailbox device 2)
- Rec2 = no (can be used for installing also a mailbox which will be paired with briar on one of the briar devices)
- Rec3 = yes (mailbox device 3)
- Rec4 = no (can be used for installing also a mailbox which will be paired with briar on one of the briar devices)




| Invite sent via | Rec1 | Rec2 | Rec3 | Rec4 | 
| ------ | ------ | ------ | ------ | ------ |
| ------ | internet | BT | MB | Export|

| Reply rec'd via | Rec1 | Rec2 | Rec3 | Rec4 | 
| ------ | ------ | ------ | ------ | ------ |
| ------ | MB| Exportl | BT | Internet|

Sender sends messages via different methods, and recipients send replies via method that is different from the one via which they received the invitations. 

- briar device 1 is online and it invites 4 recipients to the private group.  
- briar on device 1 is paired with a mailbox on another device.
- only recipient 1 is online and they get a direct invitation message.  
- recipient 1 has a mailbox
- sender 1 goes offline after they send the invitations.  
- recipient 1 replies via own mailbox. 
- recipient 2 is offline when invitations sent, but they are near the sender and the invitation is sent to them via BT.
- recipient 2 switches the BT off and exports their reply to removable media. 
- sender imports the recipient 2's reply from removable media 
- Recipient 3 is offline, and their invitation is uploaded to the sender's mailbox.
- They cannot download it as they are offline.  
- Recipient 3 comes near the sender's device and is resent the invitation via BT
- they reply via BT
- Sender exports the invitation to recipient 4 to removable media
- recipient 4 is initially offline. 
- recipient 4 comes online and the invitation is sent to them via internet
- sender goes offline
- recipient 4 replies via internet, but the sender is now offline
- sender comes online and they receive the reply message from recipient 4 via internet


As all messages should behave the same, these scenarios can be used to also test other multi-user messages such as blogs or reblogs, forum entries nad importing the rss.


[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/Testing)