Circumstances that could cause messages to be lost:
- IO error
- app shutdown
- app crash
- device crash

## Errors while the messages and acks are being written to a file

If this happen it means that we cannot be sure that the messages/acks are written to the file correctly.  

Q: How to know the precise moment when the messages are being written to a file? Is there anything visible on screen to indicate that? 
A: no

Q: is there another way that the tester can know exactly when the messages are bieng written to a file? 
A: yes, a log file - **but TBD how and where it is and what to look for**



This can include scenarios: 
- Sender sending a message file to own mailbox, when mailbox is online or when it is offline, recipient can come online before the mailbox returns online, or they cancoem online after the mailbox has returned online.
- sender sending a message file to recipient's mailbox, when the recipient's mailbox is online or offline, and when recipient comes online when their mailbox is offline, or when recipient's mailbox comes online andthen recipient connects to it.

- if the acks are created when the recipient opens their messages, that would be another point at which to cause crash - while they acks are being uploaded by the recipient's device, onto their own or sender's mailbox, while those mailboxes can be online or offline, and the sender can also be offline or online, and reconnect before or after the mailboxes come back online.
- there is also the scenarios in which both sender and recipient have mailboxes - each of which can be online or offline, and the both sender and recipient can be online or offline, and can reconnect to their respective mailboxes before or after the mailboxes come back online.
- the next scenario could involve sender sendign forum, private group or blog messages to multiple contact of which some can be online and some offline, and some can have mailboxes, some not, and of the mailboxes, can be offline and come back online at various points in time.  Recipients can also come back online after their mailboxes have returned online or before that.


"We also need to handle errors that occur after the file has been created but before it's been uploaded."
- this will again be a quesiton of timing the test correctly, and scenarios are as the ones above.





[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)