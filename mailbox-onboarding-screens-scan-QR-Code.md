## Scenario 1

**Device 1**

* In Briar app, go to settings, tap the Mailbox option
* On the first Mailbox setup screen, tap Continue button
* Verify that the next Mailbox Setup screen opens
* tap Scan Mailbox QR code
* QR code scanner opens
* tap back button
* previous page opens
* Tap Scan Mailbox QR Code again and rotate screen
* Scan the QR code and rotate the screen back to portrait immediately afterwards
* 

**Device 2**

* Needs to have mMilbox app installed
* 

[Back to testing](https://code.briarproject.org/briar/briar/-/wikis/)