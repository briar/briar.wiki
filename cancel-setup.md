ON the QR code screen, there is a button Cancel Setup. 

When this button is tapped, the setup process finishes, the mailbox icon disappears from the top bar, and the mailbox notification disappears too. 

Mailbox can be restarted and the progress circle shows, leading to the QR screen being displayed once again.  

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)

