On Mailbox device, set the sysem clock to a time in the near future - for example 2 hours after current time.

- Try to install and start the mailbox (portrait and landscape) 
- If successful, link it to the briar app 
- Then unlink it Start it again, and cancel setup 
- Start it again and link it again

Then repeat the same thing but with the system clock set further away in the future, for example a year or two.

- Bring the clock back to the current time and install and start the mailbox. 
- Stop the mailbox. (This will ensure that it doesn't autostart after the restart of device) 
- Thne restart the device 
- Change the system clock to a time in the future, as above 
- Start the mailbox again.

Anf finally, also repeat it with tha date set out of bounds date > 1.1.2122

If all goes well after the device restart, ideally, we should see the mailbox status screen showing 'running', or we should see the system clock is out of sync message. Either way this situation should be handled gracefully.


This is a valid test because the clock could potentially get reset if the device battery gets completely out of charge... I have regularly seen the the system clock gets reset if a device battery gets flat - but I have not seen the clock moving to the future, only ever ito the past.  However,this test aims to cover this requirement https://code.briarproject.org/briar/briar-mailbox/-/issues/132 and as such it is a valid test

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)
