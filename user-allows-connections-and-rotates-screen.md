Issue https://code.briarproject.org/briar/briar-mailbox/-/issues/134

From the issue it is not clear exactly at which moment the screen rotation did happen, so this tets will try and rotate the screen at a few different points:

1.  the first time when the do-not-kill me fragment is displayed (ie the button Allow connections), but before the user makes any selections
2.  immediately after the user taps Allow connections
3.  while the dialogue box asking user to confirm is displayed
4. immediately after the user taps on 'Allow'
5. Immediately after tapping 'continue'


[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/Testing)