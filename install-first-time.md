### Steps to execute: 
- If installing a debug version from Android Studio, click on the green button in ASand observe what happens on the device that you're installing the Briar app on. 

- If installing from f-droid or google-play, follow the steps as instructed and observe what happens ont eh device;

### Expected results: 
- Briar should be installed successfully
- It should start automatically (or on the tap of Open button)
- It shoudl disaply the Briar logo over the whole screen
- And the it should display the screen 'create your nickname'

[Return to Testing](https://code.briarproject.org/briar/briar/-/wikis/Testing)