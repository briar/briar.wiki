Every few minutes, Briar tries to connect to your contacts via Tor, wifi and Bluetooth. If multiple connections are made, Briar keeps one connection to each contact via each transport and closes the others (with one exception: if Briar connects to the same contact via Bluetooth and wifi, the Bluetooth connection is closed).

Messages are exchanged with each contact over whatever connections are available. If multiple connections are open (for example via Tor and wifi), they're used in parallel.

When adding a contact in person, all Bluetooth connections to other contacts are temporarily closed to ensure that a connection can be made to the new contact.

If a contact is outside wifi or Bluetooth range, Briar doesn't use other devices that are in range to relay data to the contact.

# WLAN

The last few known IPv4/IPv6 LAN-addresses are shared as [transport properties](Transport-Properties-Client). If two contacts are connected to the same WLAN (Wifi) and at least one of them know the other's current LAN-address from the shared property, then connection should be possible directly over WLAN, even if it has no Internet-connection. But other factors are also involved.

## Regarding IPv6

akwizgran: at the moment most devices have a single ipv6 link-local address that's stable across lans. so once you know this address for a contact you should be able to reach them on any lan. but this is a privacy issue for users (they can be tracked across lans) so i believe android's moving towards using a pseudo-random lan-specific ipv6 address. simultaneously, android is tightening up apps' access to ipv6 addresses by hiding ipv6-only network interfaces from apps. so we may not be able to share a list of lan-specific ipv6 addresses with contacts, in the way we do with ipv4 addresses
