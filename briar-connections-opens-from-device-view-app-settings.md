Scenario 1: when a user is logged in Briar app beforehand

- User goes to device > settings > apps > Briar app > mobile data > view app settings
- Briar connections screen opens and the user is able to switch the Briar connections on or off.  
- Tapping back takes the user to Briar settigns screen, and back again to 'view app settings'

Result OK, build 25f99bd7decfb1494462e54c7f92d2a11e025df0

Scenario 2: when a user is not logged in Briar app beforehand

- User goes to device > settings > apps > Briar app > mobile data > view app settings
- Briar password screen is displayed and when the user types in the password, the briar connections screen is displayed
- user is able to switch teh Briar connections on and off, like before
-

Result OK, build 25f99bd7decfb1494462e54c7f92d2a11e025df0

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)


