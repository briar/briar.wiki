#### Source code repo

https://code.briarproject.org/briar/briar

#### Scope of work

The work will affect the private messaging feature of the Briar Android app, which is implemented in the `briar-api`, `briar-core` and `briar-android` modules. The Briar headless app is out of scope, but will benefit from changes to `briar-core`.

Components expected to be affected by task 1:

* briar-api
    * ConversationManager
    * ConversationClient
    * MessagingManager
* briar-core
    * MessagingManagerImpl
    * New unit and integration tests
* briar-android
    * ConversationActivity
    * ConversationViewModel

Components expected to be affected by task 2:

* briar-api
    * SharingManager
    * BlogSharingManager
    * ForumSharingManager
    * GroupInvitationManager
    * IntroductionManager
* briar-core
    * SharingManagerImpl
    * BlogSharingManagerImpl
    * ForumSharingManagerImpl
    * GroupInvitationManagerImpl
    * IntroductionManagerImpl
    * New unit and integration tests
* briar-android
    * ConversationActivity
    * ConversationViewModel

Components expected to be affected by task 3:

* briar-android
    * ConversationActivity
    * ConversationViewModel
