Regularly tested:

| Device Name | Android Version | API level | Availabiity |
|-------------|-----------------|-----------|-------------|
| Alcatel A3 XL (TCL 9008X) | 7.0 | 24 | Michael |
| Asus Memo Pad 7 (K013/ME176C) | 4.4.2 | 19 | Michael |
| Google Pixel 2 | 11 | 30 | Ivana, Michael |
| Google Pixel 3A | 13 (CalyxOS 4.6.1) | 33 | Michael |
| Google Pixel 5 | 14 beta | 34 | Michael |
| Google Pixel 5 | 13 (CalxyOS 4.7.0) | 33 | Seb |
| Honor 8A (JAT-L29) | 9 (EMUI 9.1.0) | 28 | Michael |
| Honor 70 Lite (RBN-NX1) | 12 (Magic UI 6.1) | 31 | Michael |
| HTC One M9 | 7.0 | 24 | Ivana |
| Huawei Ascend Y330 | 4.2.2 (EmotionUI 2.0) | 17 | Michael |
| Huawei P8 Lite 2015 (ALE-L21) | 5.0.1 (EMUI 3.1) | 21 | Michael |
| Huawei P8 Lite 2017 (PRA-LX1) | 7.0 (EMUI 5.0.1) | 24 | Michael |
| Huawei P40 Lite 5G (CDY-NX9A) | 10 (EMUI 12.0.0) | 29 | Ivana | 
| Huawei Y6P (MED-LX9N) | 10 (EMUI 10.1.0) | 29 | Michael |
| iBall Slide (6351-Q400i) | 4.4.2 | 19 | Michael |
| LG K10 2017 (LG-M250n) | 8.1.0 | 27 | Michael |
| LG Nexus 5 | 6.0 | 23 | Seb |
| LGE Nexus 5X | 8.1.0 | 27 | Michael |
| Motorola Moto E 2nd Gen (XT1524?) | 6.0 | 23 | Ivana |
| Motorola Moto E3 (XT1700) | 6.0 | 23 | Michael |
| Motorola Moto E6 Play (XT2029-2) | 9 | 28 | Michael |
| Motorola Moto G 4G (XT1039) | 5.1 | 22 | Michael |
| Motorola Moto G5S | 8.1.0 | 27 | Seb |
| Motorola Moto G10 | 11 | 30 | Seb |
| Nokia 1.3 | 11 (Android Go) | 30 | Michael |
| Nokia 2.4 | 12 | 31 | Seb |
| Nokia 3.1 | 10 | 29 | Ivana |
| OnePlus 5T (A5010) | 10 | 29 | Michael |
| Oppo A16 (CPH2269) | 11 (ColorOS 11.1) | 30 | Michael |
| Redmi Note 7 | 10 (MIUI 12.5.1) | 29 | Michael |
| Samsung Galaxy Ace 2 (GT-I8160) | 4.1.2 | 16 | Michael |
| Samsung Galaxy A01 Core (SM-A013F/DS) | 10 (Android Go) | 29 | Ivana |
| Samsung Galaxy A10s (SM-A107F/DS) | 11 (One UI Core 3.1) | 30 | Michael |
| Samsung Galaxy A21s (SM-A217F/DSN) | 12 (One UI Core 4.1) | 31 | Michael |
| Samsung Galaxy Fame (GT-S6810P) | 4.1.2 | 16 | Ivana |
| Samsung Galaxy J3 2016 (SM-J320FN) | 5.1.1 | 22 | Michael |
| Samsung Galaxy J5 2016 (SM-J510FN) | 7.1.1 (Samsung Experience 8.5) | 25 | Michael |
| Samsung Galaxy Nexus | 4.3 | 18 | Michael |
| Samsung Galaxy S4 Mini (GT-I9195) | 4.4.2 | 19 | Ivana |
| Samsung Galaxy Tab 4 | 4.4.2 | 19 | Seb |
| Samsung Galaxy XCover 2 (GT-S7710) | 4.1.2 | 16 | Ivana |
| Sony XZ2 (H8216) | 10 | 29 | Michael |
| Ulefone Note 6T | 12 | 31 | Ivana |
| Vivo Y11 | 4.4.4 (Funtouch OS 2.0) | 19 | Michael |
| Xiaomi Mi 11 Lite 5G | 11 (MIUI 12.5.7) | 30 | Ivana |
