After the 'mailbox is unreachable' notification is displayed on Briar device, the user can tap it and they are taken to the Mailbox status page.  

On that page they can tap Check Connection.  And if the connection cannot be estbalished by Briar, they can tap Fix it. 

This takes them to the troubleshooting wizard.

This is all correct, but once tapped, the notification should disappear - which is not the case 

tested NOK, c855967d56c53d2c5e0ba5d28b6357d521c90e61 BUG https://code.briarproject.org/briar/briar/-/issues/2384

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)