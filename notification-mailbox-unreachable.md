When mailbox that is linked to a Briar instance becomes unavailable, Birar shoudl display a notification to the user to say that its mailbox is not reachable. 

![Screenshot_2022-11-18_at_11.56.28](uploads/a8e54a7824796d866e8a288af5cf7286/Screenshot_2022-11-18_at_11.56.28.png)

This can happen if the mailbox device is offline, or its battery is flat, or for any other reason that means that Briar is unable to reach its linked mailbox. 

When this happens, Briar will periodically try to reach the mailbox - and if after a certain number of tried it still cannot reach it, it will disaply this notification to the user.

Tested OK, build c855967d56c53d2c5e0ba5d28b6357d521c90e61

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)