On Mailbox device, set the sysem clock to a time in recent past - for example 2 hours before current time.

Try to install and start the mailbox (portrait and landscape)
If successful, link it to the briar app
Then unlink it
Start it again, and cancel setup
Start it again and link it again

Then repeat the same thing but with the system clock set further back in the past, for example a year or two. 

Bring the clock back to the current time and install and start the mailbox. 
Stop the mailbox.  (This will ensure that it doesn't autostart after the restart of device)
The restart the device
Change the system clock to a time inthe past, as above
Start the mailbox again. 

If all goes well, ideally, we should see the mailbox status screen showing 'running', or we should see the system clock is our of sync message.  Either way this situation should be handled gracefully. 

This is a valid test because the clock could potentially get reset if the device battery gets completely out of charge... which is not such a corner case, as i have seen it happening quite often when I don't use devices for some time and their batteries get flat before I start using them again.

[Backt to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)


