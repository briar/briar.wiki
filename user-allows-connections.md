Steps to execute: 

**Devices that have the battery optimisation setting: **

After installation of the mailbox app, a user has two options: 
- to skip intro 
- to walk through intro

User taps skip Intro

- do-not-kill-me fragment is shown to the user and the user is required to "allow connections". 
- tap the small information button ? and read the info. 
- tap 'Got it'
- go to device settings > apps and notifications > advanced > special access > battery optimisation (this is not in the same place on all devices, so look around if it is not as specified here)
- verify that this setting is On (but... even if it is Off, it could be correct, as how each device handles this setting may depend on many factors which are outside of our control)
- kill the device 'settings' app
- bring back the mailbox up into the foreground 
- tap the 'Allow connections' button
- a pop up pops up (as they do) and asks the user to Allow or Deny
- tap on deny
- the screen asking the user to "allow connections' is still there,and the Continue buttpn is still disabled.  User is unable to progress unless they allow the battery optimisation to be switched off.  
- verify that the battery optimisation settings in the device have not changed
- kill the settings app again
- then tap the "Allow' when the pop up shows
- go back to the device settings app and verify that the battery optimisation settings has now been changed to Off. 
- kill the settings app again
- go back to mailbox app and tap Continue
- Tor services progress screen shows

Repeat the above steps without skipping intro... 

- here the important thing is to verify that the do-not-kill-me fragement shows correctly after the 4th intro screen. 
- details of the screen containing this fragment have been tested above and it may not necessary to repeat them

**Devices that do not have the battery optimisation settings** 

Older devices do not optimise battery life, hence they do not have this setting. 
On these devices, the 'do not kill me' fragment should not be shown
After the intro screens, the app goes straight into the Starting Services progress screen.

**Regression test** 24.11.2022 OK

devices: Samsung 6810 Android 4 - does not show the do-not-kill-me fragment = OK
Motorola E2 Android 6 = shows the do not kill me fragment = OK

NOTE/ One quirky thing here is that even if the user denies the batter optimisations to be turned off twice in the row, the app will still start and change the battery optimisations to off, so that the app can still run in the background.  This was deliberately implemented (although I am not familiar with reasons why), and ocnfirmed by Michael and Torsten.

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/Testing)