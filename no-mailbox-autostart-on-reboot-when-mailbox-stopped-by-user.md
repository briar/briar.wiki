Seb's instructions:

Scenario 2 (restarting device does not start mailbox on boot if mailbox was set up but was stopped before reboot)

* Set up mailbox on a fresh device and pair it with briar or reuse paired setup from scenario 1 and start the mailbox app.
* Stop the mailbox app using the "Stop" button from the status screen.
* Restart device. Confirm that mailbox **does not start** automatically after boot. As it can take quite a while this needs to be observed for a couple of minutes. Good to take into consideration the amount of time it took for the mailbox app to start automatically in scenario 1.

Steps to execute:

As described above.

When the mailbox does not autostart - the status should be reflected in Briar? **TBD** The Briar user would not know that the mailbox app has been stopped until such time as they tap ont eh Check Connection link in  briar > settings > mailbox 

The Briar user will see the troubleshooting screen if Briar is unable to reach mailbox.  However, the mailbox status does not get 'pushed' to the Briar user. 

Repeat with different devices, but also with restarting it manually immediately after reboot, or a few minutes/hours later... (should not make any difference, but just to be sure to be sure)

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)