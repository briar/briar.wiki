Current thoughts on how RPC could be implemented as a [BSP](https://code.briarproject.org/briar/briar-spec/blob/master/protocols/BSP.md) client:

* Each procedure call is encoded as a message
* The arguments may be marshalled into the message itself (pass by value) or its dependencies (pass by reference)
* The response is a message that depends on the call
* The client may wish to garbage collect calls that have been answered, or keep them, e.g. for auditing
* Sync semantics mean that calls are automatically retried idempotently -- the call semantics are "eventually exactly once"

* If the RPC library offers a conventional interface (i.e. one that resembles a local procedure call), the caller will be responsible for holding program state until the call returns
* Alternatively, since a conventional RPC interface may not be well suited to a delay-tolerant implementation, the caller could use the library to save its state and retrieve it when the call was answered or abandoned
* The state could be saved as metadata or unshared data
* We could get elaborate with this -- each stack frame could be a message with a dependency on the previous frame, allowing the live program state to be replicated. Obsolete frames popped off the stack would be garbage collected
* But this is getting a long way from what's normally thought of as RPC
