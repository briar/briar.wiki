Add some contacts at a distance - for contacts that are offline, so the status remains in Pending.
Then add some contacts at a distance - for contacts that are online
* New contacts should be created successfully and the Pending contacts should still be in Pending
* New contacts can send/receive messages

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/Testing)