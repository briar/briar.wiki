https://code.briarproject.org/briar/briar-mailbox/-/issues/95

After the mailbox installation and at various points during the walk through the onboarding screens or during the do-not-kill-me fragment being displayed, or during the generation or display of QR code - change the internet settings on the device, or on router.  

When mailbox device is not connected to the internet, the user should get the 'offline' screen... See [here ](https://www.figma.com/file/bFoueGka5aTwlfL4Ap920v/Mailbox?node-id=0%3A1)

At the time when this screen is displayed the notification should be appropriate too https://code.briarproject.org/briar/briar-mailbox/-/issues/140



Repeat using different devices, in landscape and portrait 

After that screen is displayed, reconnect to the internet - the setup process should continue where it left off, or it should restart - but either way the interruption should behandled gracefully.

[Bck to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)