On the mailbox status page, tap the Unlink button when the linked briar device is offline.

After successful unlinking, connect the same instance of the mailbox app to another instance of the Briar app, on another device. 

Then bring the first briar device back online and go to its own mailbox status page - tap on Check Connection
This should lead to the trobleshooting page... 

The troubleshooting says: 
- do you have access to your mailbox device? yes. 
- what do you see on its screen? I see mailbxo is running (but I don't know that the mailbox is not connectedto my own device)
- then it says: update the software and restart devices, but these actions would not solve this problem.  Maybe troubleshooting should also say: try unlinking and relinking? question put to MM testing channel 29/6/22

- The answer received is: there will be noc changes the current solutions are deemed sufficient. 


This scenario could be repeated when there are messages within the mailbox?

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)