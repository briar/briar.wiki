### Summary

* When X and Y don't have mailboxes:
    * X and Y will only exchange messages and acks when X and Y are online at the same time
* When X has a mailbox, but Y doesn't yet know about X's mailbox:
    * X will upload messages and acks for Y to X's mailbox
    * X will check X's mailbox for messages and acks uploaded by Y
    * Y won't upload anything to X's mailbox, or download anything from it, as Y doesn't yet know about X's mailbox
    * Communication via X's mailbox won't work in either direction
    * This problem will resolve itself when Y receives X's update informing Y about X's mailbox, which will happen the next time X and Y are online at the same time
* When X has a mailbox, and Y knows about X's mailbox:
    * X will upload messages and acks for Y to X's mailbox
    * Y will upload messages and acks for X to X's mailbox
    * X will check X's mailbox for messages and acks uploaded by Y
    * Y will check X's mailbox for messages and acks uploaded by X
    * Communication via X's mailbox will work in both directions
* When X and Y both have mailboxes, and Y knows about X's mailbox, but X doesn't yet know about Y's mailbox:
    * X will upload messages and acks for Y to X's mailbox
    * Y will upload messages and acks for X to X's mailbox
    * X will check X's mailbox for messages and acks uploaded by Y
    * Y will check Y's mailbox for messages and acks uploaded by X
    * Communication from Y to X will work, but communication from X to Y won't, because X is uploading to X's mailbox, but Y is checking Y's mailbox
    * This problem will resolve itself when X downloads Y's update informing X about Y's mailbox
    * This does not require X and Y to be online at the same time (Y's update will travel via X's mailbox)
* When X and Y both have mailboxes, and both know about each other's mailboxes:
    * X will upload messages and acks for Y to Y's mailbox
    * Y will upload messages and acks for X to X's mailbox
    * X will check X's mailbox for messages and acks uploaded by Y
    * Y will check Y's mailbox for messages and acks uploaded by X
    * Communication will work in both directions. Messages and acks from X to Y will travel via Y's mailbox, while messages and acks from Y to X will travel via X's mailbox

### Scenario 1: No mailbox

In this scenario, two contacts (X and Y) communicate without a mailbox. While Y is offline, X writes a message to Y. The message is not sent (clock icon) until X and Y are online at the same time. When X and Y are online at the same time, the message is sent and acked (two ticks).

* Install Briar on X and Y
* Add X and Y as contacts
* In Briar's connection settings, turn off wifi and Bluetooth connections on X and Y
* Take Y offline
* Expectation: Y is shown as offline in X's contact list (after a delay of up to 1 minute for X to notice that Y is offline)
* X writes a private message to Y
* Expectation: X's message is shown as not sent (clock icon) in X's conversation screen
* While keeping X online, bring Y back online
* Expectation: X is shown as online in Y's contact list and vice versa (after a delay of up to 2 minutes for X and Y to connect to each other)
* Expectation: X's message appears in Y's conversation screen
* Expectation: X's message is shown as sent and acked (two ticks) in X's conversation screen

### Scenario 2: X has a mailbox that Y doesn't know about

In this scenario, X and Y become contacts and then Y goes offline. While Y is offline, X links a mailbox (M). Y doesn't know about X's mailbox, because no connection has happened between X and Y since X linked the mailbox.

While Y is offline, X writes a message to Y. X's message is uploaded to X's mailbox (one tick). Then X goes offline and Y comes back online. Y doesn't download X's message from X's mailbox because Y doesn't know about the mailbox yet.

While X is offline, Y writes a message to X. Y's message is not uploaded to X's mailbox (clock icon), because Y doesn't know about the mailbox yet. Then X comes back online.

When X and Y are online at the same time, both messages are sent and acked (two ticks).

* Install Briar on X and Y
* Add X and Y as contacts
* In Briar's connection settings, turn off wifi and Bluetooth connections on X and Y
* Take Y offline
* Install Mailbox on M
* Link Briar running on X with Mailbox running on M
* X writes a message to Y
* Expectation: the message is shown as sent (one tick) in X's conversation screen (after a delay of up to 2 minutes for X to connect to M and upload the message)
* Take X offline
* Bring Y back online
* Expectation: X is shown as offline in Y's contact list
* Expectation: X's message does not appear in Y's conversation screen
* Y writes a message to X
* Expectation: Y's message is shown as not sent (clock icon) in Y's conversation screen
* While keeping Y online, bring X back online
* Expectation: X is shown as online in Y's contact list and vice versa (after a delay of up to 2 minutes for X and Y to connect to each other)
* Expectation: X's message appears in Y's conversation screen
* Expectation: X's message is shown as sent and acked (two ticks) in X's conversation screen
* Expectation: Y's message appears in X's conversation screen
* Expectation: Y's message is shown as sent and acked in Y's conversation screen

### Scenario 3: X has a mailbox that Y knows about

In this scenario, X and Y become contacts and then X links a mailbox (M) while Y is online. Y learns about X's mailbox, because there has been a connection between X and Y since X linked the mailbox. Then Y goes offline.

While Y is offline, X writes a message to Y. X's message is uploaded to X's mailbox (one tick). Then X goes offline and Y comes back online. Y knows about X's mailbox, so Y downloads X's message from the mailbox.

While X is offline, Y writes a message to X. Y knows about X's mailbox so Y's message is uploaded to X's mailbox (one tick), along with an ack for X's message. Then Y goes offline and X comes back online. X downloads Y's message and the ack for X's message from X's mailbox. X's message is shown as sent and acked (two ticks). X uploads an ack for Y's message to X's mailbox. Then X goes offline and Y comes back online.

While X is offline, Y downloads the ack for Y's message from X's mailbox. Y's message is shown as sent and acked (two ticks).

* Install Briar on X and Y
* Add X and Y as contacts
* In Briar's connection settings, turn off wifi and Bluetooth connections on X and Y
* Install Mailbox on M
* Link Briar running on X with Mailbox running on M
* Wait a few seconds for X's update informing Y about X's mailbox to be delivered to Y
* Take Y offline
* X writes a message to Y
* Expectation: the message is shown as sent (one tick) in X's conversation screen (after a delay of up to 2 minutes for X to connect to M and upload the message)
* Take X offline
* Bring Y back online
* Expectation: X is shown as offline in Y's contact list
* Expectation: X's message appears in Y's conversation screen (after a delay of up to 2 minutes for Y to connect to M and download the message)
* While X is still offline, Y writes a message to X
* Expectation: Y's message is shown as sent (one tick) in Y's conversation screen (after a delay of up to 2 minutes for Y to connect to M and upload the message)
* Take Y offline
* Bring X back online
* Expecation: Y is shown as offline in X's contact list
* Expectation: Y's message appears in X's conversation screen (after a delay of up to 2 minutes for X to connect to M and download the message)
* Expectation: X's message is shown as sent and acked (two ticks) in X's conversation screen (after a delay of up to 2 minutes for X to connect to M and download the ack)
* Take X offline
* Bring Y back online
* Expectation: X is shown as offline in Y's contact list
* Expectation: Y's message is shown as sent and acked (two ticks) in Y's conversation screen (after a delay of up to 2 minutes for Y to connect to M and download the ack)

### Scenario 4: X and Y both have mailboxes that the other knows about

In this scenario, X and Y become contacts and then X links a mailbox (M) and Y links a mailbox (N) while X and Y are online. X and Y learn about each other's mailboxes, because there has been a connection between X and Y since they linked their mailboxes. Then Y goes offline.

From this point onwards, this scenario is identical to scenario 3 from the user's point of view. Some of the messages and acks will travel through X's mailbox and others through Y's mailbox, but the difference is not visible to the user provided both mailboxes are reachable.

While Y is offline, X writes a message to Y. X's message is uploaded to the Y's mailbox (one tick), because X knows about Y's mailbox. Then X goes offline and Y comes back online. Y downloads X's message from Y's mailbox.

While X is offline, Y writes a message to X. Y's message is uploaded to X's mailbox (one tick), along with an ack for X's message, because Y knows about X's mailbox. Then Y goes offline and X comes back online. X downloads Y's message and the ack for X's message from X's mailbox. X's message is shown as sent and acked (two ticks). X uploads an ack for Y's message to Y's mailbox. Then X goes offline and Y comes back online.

While X is offline, Y downloads the ack for Y's message from Y's mailbox. Y's message is shown as sent and acked (two ticks).

* Install Briar on X and Y
* Add X and Y as contacts
* In Briar's connection settings, turn off wifi and Bluetooth connections on X and Y
* Install Mailbox on M and N
* Link Briar running on X with Mailbox running on M
* Link Briar running on Y with Mailbox running on N
* Wait a few seconds for the updates informing X and Y about each other's mailboxes to be delivered
* Take Y offline
* X writes a message to Y
* Expectation: the message is shown as sent (one tick) in X's conversation screen (after a delay of up to 2 minutes for X to connect to N and upload the message)
* Take X offline
* Bring Y back online
* Expectation: X is shown as offline in Y's contact list
* Expectation: X's message appears in Y's conversation screen (after a delay of up to 2 minutes for Y to connect to N and download the message)
* While X is still offline, Y writes a message to X
* Expectation: Y's message is shown as sent (one tick) in Y's conversation screen (after a delay of up to 2 minutes for Y to connect to M and upload the message)
* Take Y offline
* Bring X back online
* Expecation: Y is shown as offline in X's contact list
* Expectation: Y's message appears in X's conversation screen (after a delay of up to 2 minutes for X to connect to M and download the message)
* Expectation: X's message is shown as sent and acked (two ticks) in X's conversation screen (after a delay of up to 2 minutes for X to connect to M and download the ack)
* Take X offline
* Bring Y back online
* Expectation: X is shown as offline in Y's contact list
* Expectation: Y's message is shown as sent and acked (two ticks) in Y's conversation screen (after a delay of up to 2 minutes for Y to connect to N and download the ack)
