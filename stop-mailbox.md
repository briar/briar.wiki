When Mailbox is running, there is a button Stop bext to the green tick symbol on its status screen.  
When this button is tapped, the app will stop. 

Check in the notification bar that the app is running 

![Screenshot_2022-05-30_at_12.25.10](uploads/4ab203fd905560a75cdbb615c5c72fe1/Screenshot_2022-05-30_at_12.25.10.png)

Check on the status screen that the app is running

![Screenshot_2022-05-30_at_13.03.26](uploads/ad0a9b0d430b1a5bb9d941b5f577eae0/Screenshot_2022-05-30_at_13.03.26.png)

Then tap the stop button. 

The application is not running any more.  
User can restat the app successfully.  
When restarting, the onboarding screens are not shown, nor is the battery saving screen (ie the do-not-kill-me fragment)

After a successful restart, the status screen shows 'app is running', and so does the notification bar when pulled down.

Check in portrait and landscape on different devices.  Double tap the button as well.  

[Back to testing](https://code.briarproject.org/briar/briar/-/wikis/testing)

