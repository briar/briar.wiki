#### Test Objective 
Verify that the IP address QR code  given by device1 can be successfully scanned and applied by device 2, so that the user of device 2 is not required to type in the IP address manually

#### Settings

**Device 1**
Hotspot Open

**Device 2**
Connected to the device1 hotspot

#### Steps to execute
- Use the camera of the device2 to scan the QR code of device 1
- tap on the small popup appearing on the screen when scanning the QR code

#### Expected results 
- This action opens the browser and disaplays the webpage from which to download the Briar app.
- Tapping on Download button starts dowloading the apk file of the Briar app

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)