When unlinking from the mailbox side, the mailbox does not contact briar, hence the unlinking needs to be performed from the mailbox side as well as from the briar side (MM, seb, 27/6/22, testing channel)

(the Briar app is able to wipe the mailbox remotely, but mailbox is not able to perform unlinkingon the briar side)

Therefore, if the briar is online or offline when the mailbox performs unlinking from its own status page, should make no difference. 

Both actions should lead to their respective apps being available again for linking with the same or different instances of briar or mailbox, on the same or different devices.

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)