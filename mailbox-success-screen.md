At the end of successful linking with Briar, Success screen should be shown both on Briar and on Mailbox device. 

On each device, the only tappable element is the Finish button. 
The screen should look correct both in portrait and in landscape orientation, both in the Briar and Mailbox apps.

Tapping on Finish button on Briar device, closes the success screen and returns to Briar settings.  
Tapping on Finish button on Mailbox device, closes the success screen, and displays the Mailbox status screen "Mailbox is running". 



[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)