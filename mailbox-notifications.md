Check the user notifications in the following scenarios: 

### 1. Mailbox first installed and before scanning of the QR code**

**Starting Mailbox**

- install mailbox and when the intro screens first appear, check the notification drawer.  At this point there is no mailbox notification yet, because the mailbox is not running yet
- Skip the intro screens and when the do-not-kill-me fragment appears, check the notification drawer again.  Still no notification. 
- After tapping Allow connections, there is a notification: Starting Mailbox, and underneath it Mailbox starting.  _This seems a bit redundant?_
- In case of older devices (Android 4, 5) there is no "Allow conenctions", so there is no device setting for optimising the battery usage. Veryf that the notification Starting Mailbox appear there too - this shoudl be after the last intro screen is tapped on and the progress circle appears on the screen.

**Setting up mailbox**


- The next notification is Setting up Mailbox, and underneath it Waiting for Briar to scan QR code - this is when the QR code is displayed
- When Cancel Setup is tapped the mailbox icon and the mailbox notifications disappear.

**Briar Mailbox running**


- After scanning the QR code, the notification says Briar Mailbox running and underneath it Waiting for messages 

- Loss of internet connectivity (by loss of wifi, or switching off the wifi setting ont he mailbox device), Mailbox notification says Briar Mailbox offline, waiting for internet connection.  Once the tinernet connection is back on, the notification shows Mailbox starting and then Mailbxo running - which is correct


- Switching off the notification setting for this app on Pixel 2 - when the setting is switched off the notification disappears, but when it is switched back on, the notification does not reappear.** Question in MM as it whether it should.**



[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)