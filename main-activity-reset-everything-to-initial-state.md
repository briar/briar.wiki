To exit the app, this is what we can do: 

- Cancel Setup
- Tapping the Stop button
- Wiping remotely
- Wiping/unlinking

we can also do it by
- force stopping the app
- rebooting device


After that, we can restart the app in the normal way, and regardless at which point we exited the app, we should see an 'initial state'

But what is that 'initial state'? Intro screens? QR code? 
To clarify at the time of execution if needed - to list the ways in which we exited and what screens we got on re-entring, and ask for confirmation?

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)

