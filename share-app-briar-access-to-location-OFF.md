ON some devices the process of sharing does not continue until the user gives Briar access to location. 

however, on HTC ONe M9, even if the user denied Briar access, the process continues.  

Is this a bug? Or is it as expected?  

(Question in Mattermost... expecting answer - 29062021)

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)