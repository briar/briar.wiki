### Create Blog

- [ ] Go to Main Menu and select Blogs
- [ ] This opens the Blogs main screen 
- [ ] Top right hand corner of that screen contains a pencil icon
- [ ] Tapping on pencil icon opens the Create Blog screen, and brings the keyboard up
- [ ] Type your blog post and when finished, tap Publish

Expected results:
Author's device:
- [ ] Newly created blogpost appears on the top of the main blog screen
- [ ] On the author's device there is a brief message disaplyed at the bottom of the main blog screen - saying: Blog Post Created - for own blogs there should not be the words SCROLL TO as the scrolling should happen automatically and the newly created blogpost should be shown at the top of the main blog post page.

Contacts' devices:
User not actively using Briar (Briar is in background)
- [ ] There is a notification on the device top bar/home screen that new blog post has arrived
- [ ] Tapping (or swiping) that notification takes the user to that post

User actively using Briar - doing things away from the main blog page
- [ ] User can pull down notifications from the devices top notification bar to see what is new in Briar (the same way as when they are not actively using Briar)

User on the main blog feed page

- [ ] Message at the bottom of the main blog post screeen says: New blog post received + SCROLL TO in blue
- [ ] Tapping on Scroll to takes the user to the newly received blog post
- [ ] If not tapped, the message simply disappears and the user can scroll manually to the new post

### Share blogs

User who wants to share
- [ ] User can share blogs by an author by tapping on the author's name, which opens the screen listing all the blog entries by that author
- [ ] Then on that screen tap the share button (upper right hand side corner) 
- [ ] Select contacts with whom to share the blogs
- [ ] Users with whom a particular blog is already shared are greyed out
- [ ] Tap share 
- [ ] This action returns the user to the blog feed screen
- [ ] There is a brief message at the bottom of the screen saying 'blog shared with chosen contacts'

User with whom a blog is shared

- [ ] User receives a private message from the sharer (under contacts -> messages) which is actually a blog invitation 'userX has shared the blog "userY" with you- which the invitee can accept or decline.

#### Blog Invitation accepted
- [ ] If they accept, the blogs from the author whose blog they accepted are downloaded onto their device 
- [ ] If they accept the blog invitation, the tappable words Accept or Decline are replaced with tappable word OPEN
- [ ] Tappingon Open, opens the blog feed of that author
- [ ] If they accept they get an automatically generated message: You accepted the blog invitation from userX' and if they reject this message says; You rejected the blog invitation from userX'



#### Blog invitation declined

- [ ] If the invitation is not accepted the blogs are not downloaded onto the user's device.
- [ ] There is an automatically generated message that says 'you have decline the blog invitation from userX'
- [ ] The word OPEN does not appear on the invitation message (as it does when invitation is accepted
- [ ] The words Accept and Decline disappear from the received invitation message

### RSS Feed Import
- [ ] Go to Settings > Blogs > Import RSS feed
- [ ] Import a valid RSS Feed (for example: http://www.newyorker.com/feed/humor , http://feeds.nature.com/nature/rss/current , https://www.ed.gov/feed , http://www.utah.gov/whatsnew/rss.xml )

- [ ] After successful import, tap the back button (upper left hand side corner of the app screen) to return to the main blog feed page
- [ ] At the bottom of the main blog feed screen, there is a snak bar message: New blog post received' and tappable SCROLL TO in blue
- [ ] Tap on the words SCROLL TO
- [ ] Briar scrolls to the new blogs received from the newly imported feed (ticket #2047)

- [ ] Tapping on newly imported RSS feed link, opens a page containing all the blogs from that feed

#### Sharing RSS feed blog posts

- [ ] each individual blog entry from the newly imported feed can be reblogged - doing so sends them to all the existing contacts (in the same way as reblogging other blog posts does).
- [ ] The way to share all blogposts from one feed is: tap on the title/author of the blog -> which will result in a new screen showing all the blog posts from that feed.
- [ ] In the upper right hand side corner of the screen, there is a share button (in the same way as that butotn exists for sharing other blogposts)
- [ ] Tapping the share button brings up the list of contacts with whom we can share the newly imported blogs. 
- [ ] Selecting the contacts to share the blogs with will send those contacts a private message = invitation to blog
- [ ] Recipient of blog invitation receives a new private message + notification 
- [ ] Recipient of the blog invitation can accept or decline, like for any other blog invitation
- [ ] After accepting or declining, the sender receives an automatically generated confirmation message (in the private messages with this contact)
- [ ] Recipient receives an automatically generated private message too - confirming their choice to either accept or decline the blog invitation
- [ ] If recipient accept the invitation, the blogs are downloaded onto their device
- [ ] If recipient does not accept the invitation, the blogs are not downloaded onto their device








 