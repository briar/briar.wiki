#### Settings 

**Device1**
All connection settings are ON

**Device2**
All connection settings (bluetooth, wifi and internet) are OFF

#### Steps to execute 
- Device1 - Select an image to send to the device2 and add a caption.  Initially, as the contact is offline, the message will not be delivered, but will just have a little clock icon which signifies that it is waiting to be delivered.
- Within the conversation with the device2 contact, go to the menu and select Connections > Transfer data
- Device1 - tap on Send data button, and save the generated file on the USB stick.  Eject the USB stick by going to the status bar, pulling it down and selecting Eject on the USB drive notification
- After this the messages on device1 which were 'sent'to the USB drive, have one 'tick' to denote that they have been sent.
- Device2 - receive the message by going to Contact > Connections > Transfer data, then tapping on Receive data, then selecting the file containing the message sent by device1
- Device2 - reply to that message by one or more messages containing images and captions.  
- Device2 - save the reply onto the USB stick (Transfer data > send data) and give the file a name.
- Device2 - repeat the process and save the same reply to another file - give it a different name
- Device1 - import the device2's message from the USB stick. 
- Device1 - note that the originally sent message to the device2 gains another 'tick' (and now has two of them) when the reply is imported from the USB stick
- Device1 - try to import the other file with the reply from device2 
- This is not possible, and the user gets an error message that the file is not recognised.
- Device1 - verify that all the images are disaplyed correctly and can be manipulated (zoom in, zoom out, save image, etc) 
- Device1 - write a new message to device2 and save it on the USB stick
- A few minutes later - device2 put the settings> connections> Bluetooth + WiFi ON
- This should ensure that the device2 receives the device1's message (even without importing it from the USB stick)
- Device1 go to settings > connections and switch the BT and WiFi OFF.
- Device2 write a message to device1 and save it on the USB stick
- Device1 import the device2's message from the USB stick
- Device1 go to settings > connections and switch the bluetooth and WiFi ON
- This action should add another 'tick' to the already imported message
- The messages that are already imported should not be delivered again when the bluetooth and WIFi are switched back on.  
- Equally, the mesages already received in a normal way, should not be duplicated if they are subsequently imported from the USB


[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)