- [ ] Install Briar 1.2.14
- [ ] Sign up for Briar (create nickname + passwords)
- [ ] change passwords
- [ ] Create new nearby contacts
- [ ] Create new contacts at a distance
- [ ] Send new contacts messages via bluetooth, wifi, internet
- [ ] Change profile picture, 
- [ ] verify that your contacts can see it, 
- [ ] verify that non-contacts cannot see your profile picture (in contact lists, messages, forum entries and private groups)
- [ ] Create new forum
- [ ] Write forum entries, and 
- [ ] Verify that you can see other people's forum entries.
- [ ] Create new private group
- [ ] Send messages to the private group and receive messages from it
- [ ] send some text messages between both contacts, some with text, some with images and text and some with only images
- [ ] open settings
- [ ] create a forum, share it, write a few messages
- [ ] change languages and try and do a few basic things in another language then switch back

## Complicated test case that requires many steps

* [ ] step 1
* [ ] step 2
* [ ] step 3
* [ ] step 4

