### test starting a hotspot, https://code.briarproject.org/briar/briar/-/merge_requests/1710

* it now should ask for the location setting to be enabled on Android 12 and 13
* it should ask for the **precise** location on Android 12 and is not expected to work with approximate location

Verify that hotspot can be correctly opened under these conditions

### Positive test 1: 
- clean installation of Briar debug
- device > settings> apps > Briar debug > location = ON
- device > settings> apps > Briar debug > nearby devices = ON
- Go to Briar settings > share app offline
- Start hotspot screen is displayed -> tap Start Hotspot button
- Hotspot is open showing details of hotspot (or QR code)

Result OK, build 25f99bd7decfb1494462e54c7f92d2a11e025df0 25/10/2022

### Positive test 2
- after the hotspot was already opened once on clean installation.  Follwoing that, the location setting was switched off.
- device > settings> apps > Briar debug > location = OFF
- device > settings> apps > Briar debug > nearby devices = OFF
- device connections wifi = ON
- Go to Briar settings > share app offline
- Sart hotspot screen is displayed -> Start Hotspot button
- A dialog box comes up "Allow Briar debug to access this device's location?"
- User can choose between precise and approximate location - choose precise 
- The other choice the user has to make here is: While using the app, Only this time, Don't allow. 
- choose While using the app.
- Hotspot opens OK (although sometimes it does not... sometimes it seems necessary to turn the wifi (in device > settings > connections off and back on before I will start)

Result OK?

### Positive test 3

New installation
- device > settings> apps > Briar debug > location = OFF
- device > settings> apps > Briar debug > nearby devices = OFF
- device connections wifi = OFF
- Go to Briar settings > share app offline
- Sart hotspot screen is displayed -> Start Hotspot button
- A dialog box comes up "Allow Briar debug to access this device's location?"
- User can choose between precise and approximate location - choose precise 
- The other choice the user has to make here is: While using the app, Only this time, Don't allow. 
- choose **While using the app**.
- Adialog box comes up asking the user to enable the wifi -> Continue
- Wifi switch dialog comes up - switch it to ON -> Done
- Hotspot opens. 

Result OK

### Positive test 4

Not new installation
- device > settings> apps > Briar debug > location = OFF
- device > settings> apps > Briar debug > nearby devices = OFF
- device connections wifi = OFF
- Go to Briar settings > share app offline
- Sart hotspot screen is displayed -> Start Hotspot button
- A dialog box comes up "Allow Briar debug to access this device's location?"
- User can choose between precise and approximate location - choose precise 
- The other choice the user has to make here is: While using the app, Only this time, Don't allow. 
- choose **Ask every time**.
- Adialog box comes up asking the user to enable the wifi -> Continue
- Wifi switch dialog comes up - switch it to ON -> Done
- Hotspot opens. 

### negative test 1
- device > settings> apps > Briar debug > location = OFF
- device > settings> apps > Briar debug > nearby devices = OFF
- device connections wifi = ON
- Go to Briar settings > share app offline
- Sart hotspot screen is displayed -> Start Hotspot button
- A dialog box comes up "Allow Briar debug to access this device's location?"
- Choose Don't allow. 
- A dialog comes up saying that Briar needs to use the device's precise location and when the user taps Continue they are taken to the location settings of the app (in device > settings > apps > Briar debug > permissions.  This is OK.
- In device > settings -> choose Ask every time and Use precise location = OFF
- using the app switcher, go back to Briar
- Start Hotspot screen is displayed, but with the Start Hotspot button disabled.  

[bug](https://code.briarproject.org/briar/briar/-/issues/2375)

Result NOK

### negative test 2
1. device > settings> apps > Briar debug > location = OFF
2. device > settings> apps > Briar debug > nearby devices = OFF
3. device connections wifi = ON
4. Go to Briar settings > share app offline
5. Sart hotspot screen is displayed -> Start Hotspot button
6. A dialog box comes up "Allow Briar debug to access this device's location?"
7. Choose Approximate + Allow this time.
8. Start Hotspot screen appears again **(although - should it?) **
9. A dialog comes up saying that Briar needs to use the device's precise location, and asking the user to consider granting access.  When the user taps OK they are taken to the location settings of the app (in device > settings > apps > Briar debug > permissions.  This is OK.
10. In device > settings -> choose Ask every time and Use precise location = ON
11. using the app switcher, go back to Briar
12. Briar asks the user to log in again - log in.
13. Go t Briar settings > Share this app offline
14. Start Hotspot screen is displayed.  Tap Start Hotspot button
15. Dialog comes up asking the user to choose between the precise and approximate location, and the three options: Allow while in use, Only this time, Don't allow.  However, why this dialog again?  Because I already selected the precise location in step 10. If I check in the device settings, I see that Ask every time and Precise Location are indeed selected.  So... why is the dialog asking me to select it again?



[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)