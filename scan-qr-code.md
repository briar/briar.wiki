- At the end of the onboarding process, a QR code is displayed on Mailbox app screen.  
Related tickets
https://code.briarproject.org/briar/briar-mailbox/-/issues/40 
https://code.briarproject.org/briar/briar-mailbox/-/issues/41
https://code.briarproject.org/briar/briar-mailbox/-/issues/48

- Scan the QR code using Briar app on another device. 
- Verify that this screen shows after successful scanning



![Screenshot_2022-05-30_at_12.06.45](uploads/dcbb1e0074f90beef7e7d92cf8d9d353/Screenshot_2022-05-30_at_12.06.45.png)

Tap on Finish 

Verify that status screen shows after that. 

![Screenshot_2022-05-30_at_12.21.34](uploads/e196ad1b3af1642ebab9a46a22834c85/Screenshot_2022-05-30_at_12.21.34.png)

Pull the top bar down and see that this screen shows 

![Screenshot_2022-05-30_at_12.25.10](uploads/6edc985b18e60f8244a8a840f7b43a00/Screenshot_2022-05-30_at_12.25.10.png)

Tap on it... the status screen should show again (but verify that this tapping action does not create another instance of the status screen - see ticket https://code.briarproject.org/briar/briar-mailbox/-/issues/118

repeat using different device combinations. 

Also, try to double tap the Finish button, or swipe it, in portrait or landscape orientation.  
Try to scan in portait and landscape orientation too. 

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)