1. Create a top-level ticket for the feature
2. If we decide to work on the feature, assign it to a milestone
3. Agree on a minimal first iteration of the feature
4. Divide the first iteration into child tickets -- at minimum, one for UX design and one for implementation
5. Each UX design ticket is closed when we agree on a final mockup, which is copied over to the implementation ticket
6. Each implementation ticket is closed when the code has been reviewed, tested and merged
7. Create tickets for any bugs or tasks identified during code review and testing
8. Repeat steps 4-7 for each subsequent iteration