#### Settings

**Device 1**
- Go to device settings > Location and set the Use Location to OFF
- Go to Privacy > Permissions Manager > Briar > Access to Location > Deny

**Device 2**
- Go to device settings > Location and set the Use Location to OFF
- Go to Privacy > Permissions Manager > Briar > Access to Location > Deny

#### Steps to execute

**Device 1**

- Go to Briar > settings > Share app offline
- Tap the 'Start Sharing' button
- Briar will ask the user to consider giving Briar access to location
- Give Briar access to location by tapping onthe dialog box that appears

#### Expected results: 

- Continue the process of sharing the app - verify that the hotspot is open and visible to other devices
- Verify that other devices are able to connect to the hotspot 

**Variation**
- When Briar asks you to give it access to location, go to device settings > privacy > permissions > location and give Briar access to location 
- ON return to Briar, Briar may require you to login again
- After login, verify that the first screen shown is the Intro screen for sharing app offline, and tap on the Start Sharing button
- The WIFI details are shown on screen, hotspot is open and other devices can connect to it (by sanning the QR code)

Having the 'use location' setting off, doesn't seem to make any difference to the app being able to use the location or not.  The app behaviour is the same as when the permission is denied to Briar to access location  But as soon as that is granted, even though the 'use location' setting on the device is still off, Briar is able to complete the process.





[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)






 