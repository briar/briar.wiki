Briar is a messaging app designed for activists, journalists, and anyone else who needs a safe, easy and robust way to communicate. Unlike traditional messaging tools such as email, Twitter or Telegram, Briar doesn't rely on a central server - messages are synchronized directly between the users' devices.

If the Internet's down, Briar can sync via Bluetooth or Wi-Fi, keeping the information flowing in a crisis. If the Internet's available, Briar can sync via the Tor network, protecting users and their relationships from surveillance.

Feel free to edit this wiki! Your contributions are appreciated.

* [Frequently Asked Questions](FAQ) (FAQ)

# Protocol
* [A Quick Overview of the Protocol Stack](A-Quick-Overview-of-the-Protocol-Stack)
* [How Briar Connects to Contacts](How-Briar-Connects-to-Contacts)
* [BDF](https://code.briarproject.org/briar/briar-spec/blob/master/BDF.md) - a structured data format
* [BQP](https://code.briarproject.org/briar/briar-spec/blob/master/protocols/BQP.md) - a QR code key agreement protocol
* [BHP](https://code.briarproject.org/briar/briar-spec/blob/master/protocols/BHP.md) - a key agreement protocol
* [BRP](https://code.briarproject.org/briar/briar-spec/blob/master/protocols/BRP.md) - a discovery protocol
* [BTP](https://code.briarproject.org/briar/briar-spec/blob/master/protocols/BTP.md) - a transport layer security protocol for delay-tolerant networks
* [BSP](https://code.briarproject.org/briar/briar-spec/blob/master/protocols/BSP.md) - an application layer data synchronisation protocol for delay-tolerant networks

# Architecture
* [Threat Model](threat-model)
* [Application Lifecyle](application-lifecycle)
* [Client API Notes](Client-Api-Notes)
* Some thoughts on [how RPC could be implemented as a BSP client](Rpc-Notes)
* [Mailbox Architecture](https://code.briarproject.org/briar/briar/-/wikis/Mailbox-Architecture)

# Clients
* [Transport Key Agreement Client](https://code.briarproject.org/briar/briar-spec/blob/master/clients/Transport-Key-Agreement-Client.md)
* [Transport Properties Client](https://code.briarproject.org/briar/briar-spec/blob/master/clients/Transport-Properties-Client.md)
* [Messaging Client](https://code.briarproject.org/briar/briar-spec/blob/master/clients/Messaging-Client.md)
* [Forum Client](https://code.briarproject.org/briar/briar-spec/blob/master/clients/Forum-Client.md)
* [Forum Sharing Client](https://code.briarproject.org/briar/briar-spec/blob/master/clients/Forum-Sharing-Client.md)
* [Blog Client](https://code.briarproject.org/briar/briar-spec/blob/master/clients/Blog-Client.md)
* [Blog Sharing Client](https://code.briarproject.org/briar/briar-spec/blob/master/clients/Blog-Sharing-Client.md)
* [Private Group Client](https://code.briarproject.org/briar/briar-spec/blob/master/clients/Private-Group-Client.md)
* [Private Group Sharing Client](https://code.briarproject.org/briar/briar-spec/blob/master/clients/Private-Group-Sharing-Client.md)
* [Introduction Client](https://code.briarproject.org/briar/briar-spec/blob/master/clients/Introduction-Client.md)

# Build Process
* [Building from Source](https://briarproject.org/building.html)
* [Building the Tor Binaries](tor-build-process)

# Development
* [Development 101](development-101)
* [Changelog](changelog)
* ~~[Project Roadmap to 1.0](Roadmap)~~
* [Sponsor 1 Roadmap](Sponsor-1)
* [Product Backlog](product-backlog) (aka possible future features)
* [Process for Designing and Developing New Features](feature-workflow)
* [Development Workflow](development-workflow)
* [Pre-Review Checklist](pre-review-checklist)
* [Coding Style](code-style)
* [Signed Commits](signed-commits)
* [Translation Workflow](translation-workflow)
* [Android Accessibility](Android-Accessibility)
* [Adding support for new architectures in Tor](Adding-support-for-new-architectures-in-Tor)

# Testing
* [Testing](testing)
* [Field Guide](field-guide) (work in progress)

# Family of Wikis

Briar has a whole family of wikis for different parts of the project:
* [Briar Desktop](https://code.briarproject.org/briar/briar-desktop/-/wikis/)
* [Briar Mailbox](https://code.briarproject.org/briar/briar-mailbox/-/wikis/)
* [Social Mesh Research](https://code.briarproject.org/briar/social-mesh-research/-/wikis/)
* [Public Mesh Research](https://code.briarproject.org/briar/public-mesh-research/-/wikis/)
* [Internal Admin Wiki](https://code.briarproject.org/briar/admin/-/wikis/) (only accessible for Briar Contributors)