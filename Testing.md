## Devices Tested
* [Devices Tested](devices-tested)
## Mailbox App Test Coverage
* [Mailbox Test Coverage](mailbox-test-coverage)
## Briar Mailbox Test Coverage
* [Briar Mailbox Test Coverage](briar-mailbox-test-coverage)
## Briar Mailbox Retest [Proguard Minification](https://code.briarproject.org/briar/briar-mailbox/-/issues/173) 
* [Briar Mailbox Tests Executed](briar-mailbox-tests-executed)
## Functionality
### WIP Install Briar
* [Install first time](install-first-time)
* [WIP Upgrade app version](upgrade-version)
### Create Account
* [Create Account](create-account)
### Add contacts
#### Add contact at a distance
* [Add contact at a distance](connect-at-a-distance)
* [Add already added contact at a distance](add-already-added-contact-at-distance)
* [Add contact at a distance when one contact is offline](add-contact-at-distance-one-contact-offline)
* [Delete and recreate the contact at a distance](delete-and-recreate-contact-at-distance)
* [Rotate device whilst adding contact at a distance](rotate-screen-whilst-adding-contact-at-distance)
* [Add contact whilst some contacts are Pending](add-contact-whilst-pending-contacts)
* [Add contact after Pending contact time-out](add-contact-after-time-out)
#### Add nearby contact
* [Add nearby contact](connect-with-nearby-contact)
* [Add nearby contact when bluetooth setting is OFF](add-nearby-contact-bluetooth-off)
* [Deny Briar access to location](deny-briar-access-to-location)
* [WIP Deny Briar access to location multiple times in a row](deny-briar-access-to-location-many-times)
* [Location turned OFF during the adding of nearby contact](location-off-during-contact-creation)
* [Receive messages during the nearby contact creation](receive-messages-during-contact-creation)
* [Delete and recreate a nearby contact on one device](delete-recreate-nearby-contact)
* [Attempt to add the same nearby contact twice](add-nearby-contact-twice)
* [Add nearby contact that was already added at a distance](add-nearby-contact-that-was-already-added-at-distance)
#### Bluetooth setup screen
* [Reconnect via Bluetooth after internet connection lost](bluetooth-setup-screen)
* [Reconnect via Bluetooth when Briar BT setting = OFF](reconnect via BT when BT = OFF)
* [Reconnect via bLuetooth when Briar BT = OFF and device BT setting = OFF](reconnect-via-BT-when-Briar-BT-OFF-and-device-BT-OFF)
* [Reconnect via bLuetooth when Briar BT = OFF and device BT = OFF, device location = OFF](reconnect-via-BT-when-Briar-BT-OFF-and-device-BT-OFF-device-location-OFF)
* [Reconnect via bLuetooth when Briar BT = OFF and device BT = OFF, device location = OFF and Briar has no access to location](reconnect-via-BT-when-Briar-BT-OFF-and-device-BT-OFF-device-location-OFF-Briar-no-access-to-location)
* [Add nearby contact then reconnect via bluetooth](add-nearby-contact-then-reconnect-via-BT)
* [Fresh install - Add nearby contact via BT then reconnect via BT ](fresh-install-add-nearby-contact-BT-reconnect-BT)
* [Fresh install - Add contact at a distance, then reconnect via BT](fresh-install-add-contact-distance-reconnect-via-BT)
* [Re-add deleted contact, then connect via BT](re-add-deleted-contact-reconnect-via-BT)
### Forums
* [Forums](forums)
### Blogs
* [Blogs](blogs)
### Settings
* [Profile pictures](profile-pictures)
### Self destructing messages
* [Self destructing messages](self-destructing-messages)
* [Disappearing messages sent to direct contacts](disappearing-messages-sent-to-direct-contacts)
* [Make introductions, Disappearing Messages ON, intro accepted](make-introductions-disappearing-messages-on-intro-accepted)
* [Make introductions, Disappearing Messages ON/OFF, intro accepted](make-introductions-disappearing-messages-onoff-intro-accepted)
* [Make introductions, Disappearing Messages ON, intro declined by one](make-introductions-disappearing-messages-on-intro-declined-by-one)
* [Make introductions, Disappearing Messages ON, intro declined by both](make-introductions-disappearing-messages-on-intro-declined-by-both)
* [Make introductions, Disappearing Messages ON, intro auto-declined by one/both](make-intro-dis-msgs-on-autodecline)
* [Make introductions, Disappearing Messages ON/OFF, intro declined/autodeclined](make-introductions-disappearing-messages-onoff-intro-decl-autodecl-by-both)
* [WIP Make introductions, Disappearing Messages ON/OFF, intro declined-by-one](make-introductions-disappearing-messages-onoff-intro-declined-by-one)
* [WIP Make reintroductions, Disappearing Messages ON/OFF, after intro declined by one](make-reintroductions-disappearing-messages-onoff-after-intro-declined-by-one)
* [WIP Make reintroductions, Disappearing Messages ON/OFF, after intro declined by both](make-reintroductions-disappearing-messages-onoff-after-intro-declined-by-both)
* [WIP Make introductions to multiple contacts, Disappearing msgs ON and OFF, all accept](introductions-multiple-contacts-disapp-msgs-on-off-all-accept)
* [WIP Make introductions to multiple contacts, Disappearing msgs ON and OFF, all decline](introductions-multiple-contacts-disapp-msgs-on-off-all-decline)
* [WIP Make introductions to multiple contacts, Disappearing msgs ON and OFF, some accept some not](introductions-multiple-contacts-disapp-msgs-on-off-some-accept-some-not)
* [WIP Make another introduction while having one unfinished intro, Disappearing msgs ON and OFF, ](make-intro-while-having-one-unfinished-intro-disapp-msg-on-off)
### Offline App Sharing
* [Open a hotspot on a device - type network name manually](open-hotspot-manually)
* [Open  hotspot by scanning the QR code](open-hotspot-scan-qr-code)
* [Download app - type address manually](download-type-address-manually)
* [Download app - scan QR code](download-app-scan-QR-code)
* [Install shared apk](install-shared-apk)
* [Create account on shared Briar app](create-account-on-shared-briar-app)
* [Add contacts in Briar app received via hotspot](add-contact-on-biar-received-via-hotspot)
* [WIP Share the app received via hotspot](share-app-received-via-hotspot)
* [WIP Receive an upgrade via hotspot](receive-an-upgrade-via-hotspot)
* [Share app while wi-fi on device= OFF](share-app-device-wifi-OFF)
* [Share app while wi-fi in briar app = OFF](share-app-wifi-in-briar-OFF)
* [Share app while device location = OFF](share-app-device-location-OFF)
* [Share app while briar access to location = OFF)](share-app-briar-access-to-location-OFF)
* [Connect multiple devices to hotspot](connect multiple-devices-to-hotspot)
* [Stop sharing after download finished](stop-sharing-after-download)
* [Stop sharig before download finished](stop-sharing-before-download-finished)
* [Briar in the background during sharing](briar-in-background-during-sharing)
* [Navigate away from sharing screen](Navigate-away-from-sharing-screen)
* [WIP receive messages during sharing](receive-messages-during-sharing)
* [Comparative test several devices location and wifi settings](comparative-test- several-devices-location-and-wifi-settings)
### Transfer data securely via SD card or USB stick
* [Transfer direct messages to a contact](transfer-direct-messages)
* [Transfer messages containing images](transfer-messages-with-images) 
* [Re-Import already imported messages](reimport-imported-messages)
* [User attempts to import messages sent to another person](import-somebody-elses-messages)
* [WIP Transfer disappearing messages](transfer-disappearing-messages)
* [WIP Transfer introductions](transfer-introductions)
* [WIP Transfer forum posts](transfer-forum-posts)
* [Transfer blog posts ](transfer-blog-posts)
* [WIP Transfer Private group invitations](transfer-private-group-messages)
* [WIP double click, cancel, navigate back and forth, landscape]
* [WIP Transfer profile pictures](transfer-profile-pictures)
* [WIP Transfer large volume of msgs](transfer-large-volume-of-messages)

# Mailbox

### Mailbox Briar side

* [Notes on testing](notes-on-testing)
* [Mailbox in Briar settings](mailbox-in-briar-settings)
* [Mailbox onboarding share link](mailbox-onboarding-screens-share-link)
* [WIP Mailbox onboarding scan QR Code](mailbox-onboarding-screens-scan-QR-Code)
* [WIP Q #2226 Error handling for mailbox uploads](error-handling-mailbox-uploads)

### Message upload and download

* [Verify that messages behave as expected when no mailbox](messages-as-expected-no-mailbox)
* [Verify that messages are uploaded correctly when Y doesn't know X has mb](messages-uploaded-to-mailbox-correctly)
* [Verify that messages are uploaded correctly when Y knows X has mb](messages-uploaded-Y-knows-x-has-mb)
* [Verify correct uploading of messages when X and Y both have mailboxes that the other knows about](message-upload-both-x-and-y-have-mailboxes)
* [WIP Unlink mb and relink on the same device](unlink-mb-relink-on-same-device)
* [WIP Unlink mb and link to a mb on another device](unlink-mb-link-oon-another-device)
* [WIP Messages exported to removable media versus uploaded to Mailbox](messages-removable-media-mailbox)
* [Private group invitations and messages](private-group-invitations-messages)
* [WIP Invite multiple contacts to private group](invite-multiple-contacts-to-private-group)
* [WIP Forum invitations and messages](forum-invitations-messages)
* [WIP Blogging and reblogging](blogging-reblogging)
* [WIP Disappearing messages](disappearing-messsages)
* [WIP Reconnect via BT](reconnect-via-BT)

### API versions supported

### Drop support for Android 4

### Briar Mailbox pairing
* [The 4 Onboarding screens](onboarding-UI)
* [User leaves setup](user-leaves-setup)
* [Navigation to and from do-not-kill-me fragment](first-install-do-not-kill-me-fragment)
* [App in background while do-not-kill-me fragment displayed](app-in-background-while-do-not-kill-me-fragment-displayed)
* [User "Allow connections"](user-allows-connections)
* [User "Allow connections" + rotate screen](user-allows-connections-and-rotates-screen)
* [Manually change 'Battery optimisation' setting to -> Off](manually-change-battery-optimisation-setting-to-off)
* [Manually change 'Battery optimisation' setting to -> ON](manually-change-battery-optimisation-setting-to-ON)
* [Open Mailbox after device goes to sleep](reopen-mailbox-after-device-goes-to-sleep)
* [WIP Open battery settings](open-battery-settings)
* [Device offline warning](device-offline-warning)
* [Scan QR code](scan-qr-code)
* [Tor port](tor-port)
* [Interrupt Scanning QR code](interrupt-scanning-qr-code)
* [Scan incorrect QR code](scan-incorrect-qr-code)
* [Try to scan valid QR code with two devices at the same time](scan-valid-QR-code-more-than-once)
* [Cancel Setup](cancel-setup)
* [Mailbox Success screen](mailbox-success-screen)
* [Restart mailbox after wiping + do not keep activities](restart-mailbox-after-wiping-do-not-keep-activities)
* [MainActivity should reset everything to an initial state](main-activity-reset-everything-to-initial-state) 


### Mailbox status 

* [Mailbox is running](mailbox-is-running)
* [Time last connected](time-last-connected)
* [Stop mailbox](stop-mailbox)
* [WIP Mailbox not reachable by Briar](mailbox-not-reachable)
* [WIP Mailbox cannot reach briar](Mailbox-cannot-reach-Briar)
* [Mailbox has no access to internet](mailbox-no-internet)

### Mailbox user notifications

* [Mailbox notifications](mailbox-notifications)

## Mailbox Unreachable Notification in Briar 

* [Notification Mailbox unreachable](notification-mailbox-unreachable)
* [Notification Mailbox unreachable disappears after it's tapped](notification-mailbox-unreachable-disappears-after-tapping) 
* [Notification Mailbox unreachable disappears when mailbox reachable](notification-mailbox-unreachable-disappears-when-mailbox-reachable)
* [Notification Mailbox unreachable disappears when mailbox unlinked](notification-mailbox-unreachable-disappears-when-mailbox-unlinked)



### Autostart mailbox on reboot (https://code.briarproject.org/briar/briar-mailbox/-/merge_requests/83)

* [Autostart mailbox on reboot when mailbox set up](autostart-mailbox-on-reboot-when-mailbox-set-up)
* [Autostart mailbox on reboot when mailbox set up not completed yet](autostart-mailbox-on-reboot-when-mailbox-set-up-not-completed-yet)
* [No mailbox Autostart on reboot when mailbox QR code scan canceled](no-mailbox-autostart-on-reboot-when-QRcode-scan-canceled)
* [No mailbox Autostart on reboot when mailbox was stopped by user](no-mailbox-autostart-on-reboot-when-mailbox-stopped-by-user)
* [Reboot briar device immediately after scanning the QR code ](reboot-briar-device-after-QR-code-scan)


### Mailbox unlinking/wiping

* [unlink mailbox](unlink-mailbox)
* [WIP Interrupt the process of unlinking from Briar side](interrupt-unlinking-from-briar-side)
* [WIP Interrupt the process of unlinking from Mailbox side](interrupt-unlinking-from-mailbox-side)
* [WIP Unlink own mailbox containing pending messages](unlink-own-mailbox-containing-messages)
* [WIP contact unlinks their mailbox containing messages sent by the sender](contact-inlinks-mailbox-containing-messages-sent-by-sender)
* [WIP cunlink own mailbox containing messages then export messages](unlink-own-mailbox-containing-messages-then-export-messages)
* [Mailbox wiping complete screen](mailbox-wiping-complete-screen)

### Mailbox device system clock out of sync

* [Mailbox device system clock in the past](mailbox-system-clock-in-the-past)
* [Mailbox device system clock in the future](mailbox-system-clock-in-the-future)

### Contact management

* [Upload new contacts to mailbox](upload-new-contacts-to-mailbox)

### Mailbox Translations

* [Mailbox  strings translations](mailbox-strings-translations)

## Android 12

* [WIP Share this app offline](share-app-offline)
* [WIP Reconnect via BT](reconnect-via-BT)
* [Nearby contact](nearby-contact)
* [Remove nearby devices permission and start app](remove-nearby-devices-permissions-and-start-app)
* [Briar connections screen opens from Mobile data > view app settings](briar-connections-opens-from-device-view-app-settings)












