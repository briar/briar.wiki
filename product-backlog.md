 "platforms" theme
  * desktop
  * ios
  * chrome os
  * extend headless app
  * remain viable on android (wake locks, foreground services, etc)


"feature parity" theme

  * delete private messages
  * self-destructing messages
  * avatars
  * audio messages
  * multi-device support (equal peers or leader-follower)
  * account backups
  * account transfers
  * improve ui for threaded conversations


"offline" theme

  * improve ui for controlling data transports
  * improve reliability of bluetooth and wifi transports
  * reduce nearby metadata leakage by using ble and wifi direct discovery
  * improve ui and protocol for adding contacts in person
  * test under simulated blackout conditions
  * investigate medium-range transports (e.g. lora, afsk over handheld radios)
  * investigate long-range simplex transports (e.g. satellite, afsk over am/fm broadcast)


"mailbox" theme

  * architecture and features to be decided


"performance" theme

  * reduce battery consumption (partner: tor)
  * reduce bandwidth consumption (partner: tor)
  * improve database performance
  * migrate to native crypto (libsodium)
  * scale to bramble groups with thousands of messages


"social" theme

  * forum moderation
  * edit or delete own posts in private groups, forums and blogs
  * make private groups more flexible (e.g. transfer ownership, multiple moderators per group)
  * verify identities of contacts when meeting in person
  * image attachments for forums, blogs and private groups


"mesh" theme (speculative)

  * protocols for advertising, discovering nearby peers
  * protocol for syncing with ephemeral peers
  * sync encrypted bramble messages via mesh (unicast and multicast)
  * proof of concept mesh chatroom app
