### Building a new Tor binary

The first step for adding a new architecture for Tor is obtaining a reproducibly built binary. At Briar we use _tor-reproducer_ for this task. Here are some merge requests that added new architectures:
* [Linux/x86/64-bit](https://code.briarproject.org/briar/tor-reproducer/-/merge_requests/3)
* [Linux/armhf/ARMv7](https://code.briarproject.org/briar/tor-reproducer/-/merge_requests/13)
* [Windows/x86/64-bit](https://code.briarproject.org/briar/tor-reproducer/-/merge_requests/17)

Once you modified the build script of _tor-reproducer_, you can build the Docker image and run it. If this succeeds, you can pull out the Tor binary like this:
```
$ docker ps -a  # search the ID of the last container
CONTAINER ID   IMAGE                         COMMAND                  CREATED      STATUS                    PORTS     NAMES
e8c4960250a7   briar/tor-reproducer:latest   "./build-tor.py"         5 days ago   Exited (1) 5 days ago               suspicious_goodall

$ docker commit e8c4960250a7 temp  # make an image out of the last container
$ docker run -ti --entrypoint=sh temp  # start the image as a container; run this in a separate terminal
$ docker ps  # get the ID of the started container
CONTAINER ID   IMAGE                         COMMAND                  CREATED      STATUS                    PORTS     NAMES
e6016a16073c   ...

$ docker cp e6016a16073c:/opt/tor-reproducer/tor-android/prefix/bin/tor .
```
Depending on your architecture, the Tor binary might be called `tor.exe`.

To clean things up afterwards (Docker is using more and more disk space with each build), run this:
```
$ docker system prune
$ docker rmi temp
```

### Creating a Tor bundle for Briar

With the Tor binary obtained, you can now create a temporary Tor zip bundle to be used in Briar.

First, download the current version of Briar's Tor bundle from [its Maven repository](https://mvnrepository.com/artifact/org.briarproject/tor). Then unpack the .jar file. You'll see that it contains a set of .zip archives, one for `geoip` and one for each architecture. You can add your architecture by zipping the Tor binary you just obtained. Let's assume you're adding support for the M1 version of macOS, so you call the zip archive `tor_macos-m1.zip`. Once you've done that, create a new zip archive with the same name as the .jar file you downloaded, containing all the architectures and `geoip` archives.

### Including the Tor bundle in Briar

Place the Tor zip archive you just created in _bramble-java/libs/_. To let Briar use it, you need to replace its Tor dependency in _bramble-java/build.gradle_ like this:
```diff
diff --git a/bramble-java/build.gradle b/bramble-java/build.gradle
index 1a1a318718aae6e350c39220969a8b5684b3f73a..b081f37eefbc585c4d6a851267316b646b0daedb 100644
--- a/bramble-java/build.gradle
+++ b/bramble-java/build.gradle
@@ -17,7 +17,7 @@ dependencies {
 	def jna_version = '4.5.2'
 	implementation "net.java.dev.jna:jna:$jna_version"
 	implementation "net.java.dev.jna:jna-platform:$jna_version"
-	tor 'org.briarproject:tor:0.3.5.15'
+	tor fileTree(dir: 'libs', include: '*.zip')
 	tor 'org.briarproject:obfs4proxy:0.0.12-dev-40245c4a@zip'
 
 	annotationProcessor "com.google.dagger:dagger-compiler:$dagger_version"
```

Note that you also need to remove the respective line in _bramble-java/witness.gradle_ like this:
```diff
diff --git a/bramble-java/witness.gradle b/bramble-java/witness.gradle
index ea58c89bf7a6235075c883db173af733005671b8..1d2b7cd34fb432a4741ac6a2bbcc7312c66236da 100644
--- a/bramble-java/witness.gradle
+++ b/bramble-java/witness.gradle
@@ -25,7 +25,6 @@ dependencyVerification {
         'net.ltgt.gradle.incap:incap:0.2:incap-0.2.jar:b625b9806b0f1e4bc7a2e3457119488de3cd57ea20feedd513db070a573a4ffd',
         'org.apache-extras.beanshell:bsh:2.0b6:bsh-2.0b6.jar:a17955976070c0573235ee662f2794a78082758b61accffce8d3f8aedcd91047',
         'org.briarproject:obfs4proxy:0.0.12-dev-40245c4a:obfs4proxy-0.0.12-dev-40245c4a.zip:172029e7058b3a83ac93ac4991a44bf76e16ce8d46f558f5836d57da3cb3a766',
-        'org.briarproject:tor:0.3.5.15:tor-0.3.5.15.jar:2ff5b5a3b5eaa97d699629ad24ba9584b3199d0ffdb1ea7d8a02de3016b80e7a',
         'org.checkerframework:checker-compat-qual:2.5.3:checker-compat-qual-2.5.3.jar:d76b9afea61c7c082908023f0cbc1427fab9abd2df915c8b8a3e7a509bccbc6d',
         'org.checkerframework:checker-qual:2.5.2:checker-qual-2.5.2.jar:64b02691c8b9d4e7700f8ee2e742dce7ea2c6e81e662b7522c9ee3bf568c040a',
         'org.codehaus.mojo:animal-sniffer-annotations:1.17:animal-sniffer-annotations-1.17.jar:92654f493ecfec52082e76354f0ebf87648dc3d5cec2e3c3cdb947c016747a53',

```

See commit [a85a92cdf10e](https://code.briarproject.org/briar/briar/-/commit/a85a92cdf10e3ca46935d984c71fb65697689475?merge_request_iid=1533) of merge request [!1533](https://code.briarproject.org/briar/briar/-/merge_requests/1533/diffs?commit_id=a85a92cdf10e3ca46935d984c71fb65697689475) for an example of doing this.

### Using the new Tor architecture in Briar

To finally make use of the new architecture of Tor, you need to modify the method [`createPlugin()`](https://code.briarproject.org/briar/briar/-/blob/9f0e5a2dc2e8ef12c1cd7425999f557f99b7b34a/bramble-java/src/main/java/org/briarproject/bramble/plugin/tor/DesktopTorPluginFactory.java#L97) of the class [`DesktopTorPluginFactory`](https://code.briarproject.org/briar/briar/-/blob/9f0e5a2dc2e8ef12c1cd7425999f557f99b7b34a/bramble-java/src/main/java/org/briarproject/bramble/plugin/tor/DesktopTorPluginFactory.java#L36).

Depending on the architecture there might be more changes necessary. Note that some Briar clients like `briar-headless` [strip Tor architectures per Gradle build flavor](https://code.briarproject.org/briar/briar/-/blob/release-1.3.8/briar-headless/build.gradle#L47). See the following merge requests that added support for new architectures in Briar:
* [Linux/aarch64 and Linux/armhf/ARMv7](https://code.briarproject.org/briar/briar/-/merge_requests/1376)
* [Windows/x86/64-bit](https://code.briarproject.org/briar/briar/-/merge_requests/1533)