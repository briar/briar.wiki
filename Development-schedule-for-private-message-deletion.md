#### Phase 1: Delete all private messages in a conversation

* Add ConversationClient method for getting a set of message IDs to delete
* Add real implementation for MessagingManager, no-op implementations for other clients
* Add menu item for triggering deletion
* Add dialog for explaining that invitation/introduction messages can't be deleted
* Show dialog if not all message IDs in conversation were returned by clients for deletion

Estimated work, including review and testing: 1 week

#### Phase 2: Delete all completed invitation/introduction sessions in a conversation

* Get message metadata for contact group, map all messages to their sessions
* Look up session states and message statuses
* Exclude messages belonging to open sessions
* Exclude messages belonging to sessions with sent-but-not-acked messages
* Return IDs of remaining messages
* Update dialog to explain that ongoing invitations/introductions can't be deleted
* Show dialog if not all message IDs in conversation were returned by clients for deletion

Estimated work, including review and testing: 3 weeks

#### Phase 3: Multi-select messages to delete

* Add multi-selection to conversation view
* Intersect selected message IDs with those returned by clients, delete the intersection
* Show dialog if not all selected message IDs were returned by clients for deletion

Estimated work, including review and testing: 1 week