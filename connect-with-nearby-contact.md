Ticket #1972 

Test instructions: 
* add two android devices running android 10 or 11 with the camera and location turned off on both phones, in device settings, and Bluetooth all connexions turned off in Briar app
* both phones should ask you to turn location and camera on and only proceed when they are on

MR1357 Test instructions:

* Add two contacts with the nearby option
* try all sorts of variations and complications when doing it:
  * revoke permissions in system settings for briar
  * rotate screen in odd places through the process (especially before pressing the Next button)
  * disable wifi and/or bluetooth of the phone and/or in briar itself (before starting to add contacts)

## Scenario 1 

Add nearby contact

Device settings:
Device 1 Briar access to location = On
Device 1 Briar access to camera = Off
Device 2 Briar access to location = On
Briar settings: 
Device 1 - connect via Bluetooth = On 
Device 2 - connect via Bluetooth = Off

### Steps to execute:
 
#### Device1: 
* tap the + sign on the contacts screen and select 'nearby contacts', then tap continue
* message appears: "allow briar to take photos and make videos" user has a choice of options: deny, allow only while using the app, and ask every time.  
* (This answer will be saved in the device settings > permissions manager > camera.  If "allow only when using the app" is selected, then the user is asked just once to allow Briar to use camera, and if "ask every time" is selected, then every time Briar wants to access camera, the user's permission will be requested again - although there is some discussion about how this should function in Android platform)

ANDROID DOCS SEEM TO BE SAYING THAT IT MEANS A ONCE ONLY PERMISSION https://www.android.com/android-11/#a11-privacy-security-article

Confirmation awaited (20/4/21) - steps to reproduce described in #2000 and #2002

* message "Briar is asking to allow phone to be visible to other devices for 120 seconds" - tap "allow"
* QR code is shown in half of the creen and the other half is a camera's screen, ready to take a photo of the QR code of the neaby contact's device.

#### Device2: 
* tap the + sign on the contacts screen and select 'nearby contacts', then tap continue
* message "Briar is asking to allow phone to be visible to other devices for 120 seconds" - tap "allow"
* QR code is shown in half of the screen and the other half is a camera's screen, ready to take a photo of the QR code of the neaby contact's device.
* The devices take photos of each other's QR code, 

### Expected results:
* connection is established and they appear in each other's contact lists, indicating that they are both online.  
* Contact name for each contact is the actual name the users use to log onto Briar app. (User is able to later change the names of the contacts in their contact lists)


[Back to testing](https://code.briarproject.org/briar/briar/-/wikis/Testing)














 