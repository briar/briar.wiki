* Dependency injection
  * Services registered with LifecycleManager
  * Hooks for adding/removing contacts registered with ContactManager
  * Hooks for adding/removing identities registered with IdentityManager
* Sign in
  * User enters password
  * Password used to decrypt database key
* Database opened
* Services started
  * PluginManager creates and starts plugins
* Main user interface shown
* Sign out
* Services stopped
* Executors shut down
* Database closed
* Shutdown hooks run (not guaranteed)
