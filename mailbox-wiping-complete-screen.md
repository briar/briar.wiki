Mailbox wiping can be done locally - from within the Mailbox app and remotely, ie from the linked Briar app. 

The wiping process started from the Briar side, can happen in one of the two ways: 
- remote wiping of the mailbox which is also online
- or if the process si run when the mailbox is offline, then unlinking will be done only on the Briar side and a separate Unlink operation will need to be done on the Mailbox side. 

Unlinking from the Briar side when mailbox is offline has the following screens: 
- From the  briar -> settings -> mailbox, tap on Unlink link (portrait and landscape)
- A dialogue box comes up asking the user to confirm. Tap on confirm (portrait and landscape)
- Then an information box comes up, informing the user that their Mailbox has been unlinked. The only tappable element here is Got it. (portrait and landscape)
- user tap Got it, and the following screen they see is the Briar settings screen. (portrait and ladnscape)

Also try to navigate back and forth at every point.


Unlinking from the Briar side when mailbox is online (remote wiping)
- user taps on Unlink (mailbox status screen)
- a dialogue box comes up asking the user to confirm that they want to unlink.  (portrait and landscape) User taps on Unlink.
- unlinking process starts on both devices (portrait and landscape)
- on the Briar device, the next screen that shows is the Briar settings (ie no more mailbox related screens or dialogue boxes)
- on the mailbox device, the mailbox wiping and stopping mailbox progress circle shows... and then there shoudl be the Wiping is complete screen (but not working currently, gets stuck in the progress ... 20/07/22) (portrait and ladnscape)

Unlinking from the mailbox side

- On Mailbox status screen, user taps on Unlink (portrait and landscape)
- A dialogue box comes up, asking the user to confirm.  User taps Unlink.
- A screen with Progress circle is shown (portrait and landscape)
- and the Wiping complete screen (portrait and ladnscape)
- the only tappable element on this screen is OK button.  
- tapping on OK, the mailbox screens and notifications disappear as the mailbox is not running any more.


[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)