When th mailbox user taps on the Check connection link on the status screen - and Briar is offline, what should the user see?  

- tap this link in portrait and landscape
- some kind of error screen should be given to the user
- navigate back and forth from that error screen
- and lastly bring Briar back online, and repeat. 

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)