IN this scenario, the expected bhaviour depends on how far the process of linking Briar and Mailblx has got before the reboot.

If the two apps were linked successfully, then after reboot the mailbox should autostart on the mailbox device, and on briar device in settings > mailbox, the status screen should be showing that the mailbox is running

If the linking was not complete,then on mailbox reboot, the QR code should be displayed and scanned again. 

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)