Add nearby contact via bluetooth, then switch the bluetooth + wifi off in Briar app 



Note: To be sure to connect via BT, switch the wifi off on both devices, so that the only way two contacts can be added is via BT. 
Switch location off on both devices. Deny Briar app access to location in the device settings > privacy > location.

* Then add nearby contacts. 
* Allow Briar access to location when requested 
* Once the contacts are successfully added, go to settings on Briar app and switch the BT , wifi and internet connections OFF.
* Then go to the newly added contact on each device and in the menu (upper right corner) selet Connect via Bluetooth option.
* Follow the onscreen instructions

Expected results: 
* Connection should be successfully re-established and 
* Contacts shoudl both receive correct onscreen messages that hte connection was successfully made via BT
* The contacts should be able to send each other messages







[Back to testing](https://code.briarproject.org/briar/briar/-/wikis/testing)