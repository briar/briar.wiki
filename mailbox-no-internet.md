When mailbox is offline, the following screen should be displayed to the user 

![Screenshot_2022-06-16_at_12.41.50](uploads/d4c87f90b16aaffe16e7c0a5caaca10a/Screenshot_2022-06-16_at_12.41.50.png)

- Tap the try again button several times, in landscape and portrait, 
- and try disabling the internet/wi-fi access for mailbox at the router, 
- disable the internet access on the mailbox device itself, 
- bring mailbox into the front or push it into the background, 
- navigate back and forth from this screen
- check notifications - they should change as the status changes

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)