**[Milestone A](https://code.briarproject.org/akwizgran/briar/milestones/1):**

* ~~A.1: Finish implementing the new transport protocol (BTP2) (#111)~~
* ~~A.2: Separate the data sync protocol (BSP) from its clients (messaging, forums, transport properties) (#112)~~
* ~~A.3: Threat modeling round (#113)~~
* ~~A.4: Establish initial contacts with potential partner organization (#114)~~

**[Milestone B](https://code.briarproject.org/akwizgran/briar/milestones/3):**

* ~~B.1: Improve Tor hidden service performance (#115)~~
* ~~B.2: New polling logic (#116)~~
* ~~B.3: Add contacts via QR codes (#117)~~
* ~~B.4: Introduce contacts to each other (#118)~~
* ~~B.5: User testing round (#119)~~

**[Milestone C ](https://code.briarproject.org/akwizgran/briar/milestones/4):**

* ~~C.1: Identicons (#120)~~
* ~~C.2: Improve UX for sharing forums (#121)~~
* ~~C.3: UI for threaded discussions (#122)~~
* ~~C.4: Encrypted crash reports and user feedback via Tor (#123, #124)~~
* ~~C.5: Document partner organization's use cases and user stories (#125, #126)~~

**[Milestone D](https://code.briarproject.org/akwizgran/briar/milestones/5):**

* ~~D.1: Blogs (#134)~~
* ~~D.2: RSS import (#135)~~
* ~~D.3: User testing round (#128)~~
* ~~D.4: Protocol documents for Bramble protocol stack, initial translations of app and website (#129, #130, #131)~~
* ~~D.5: Security testing round (#133)~~

**[Milestone E](https://code.briarproject.org/akwizgran/briar/milestones/6):**

* ~~E.1: Private groups (#127)~~
* ~~E.2: Emoji (#92)~~

**[Milestone F](https://code.briarproject.org/akwizgran/briar/milestones/7):**

* ~~F.1: Separate the Bramble protocol stack from the Briar messaging app (#136)~~
* ~~F.2: Graphic design for app and website~~
* ~~F.3: Installation guide and user manual (#138, #139)~~
* ~~F.4: User testing round (#140)~~
* ~~F.5: Roadmap for partner organization's Bramble-based application (#141)~~

**[Milestone G](https://code.briarproject.org/akwizgran/briar/milestones/8):**

* ~~G.1: OTF security audit of the app (#142)~~
* ~~G.2: Fix security issues identified by the audit~~
* ~~G.3: Final translations of the app, website and documentation (#143)~~
* ~~G.4: User testing round (#144)~~

**Release Briar 1.0**