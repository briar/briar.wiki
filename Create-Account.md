**Create Account on fresh install of Briar**

Install Briar 

- [ ] User is asked to enter their name 
- [ ] Next screen, user is asked to enter and re-enter their password
- [ ] If the passwords are not identical, user cannot proceed
- [ ] The bar under the passwords shows the users in a visual way whether passwords satisfy requirements.  
- [ ] When the bar is red or amber, the user cannot proceed.  User can proceed when the bar is yellow or green

- [ ] On second login, user is not asked for username, only for password
- [ ] Typing in the correct password lets the user in
- [ ] Typing in incorrect password does not allow user to proceed
- [ ] LInk 'I have forgotten my password' is available and on clicking it, user is given a warning message that their Briar account will be deleted and they will lose all their identities, contacts and messages.  Tapping on Delete deletes the account and starts a process of creating a new one
- [ ] New screen appears where the user can enter their username and from their proceed to the next screen to enter their password

- [ ] On entering the Briar after creating a new account, there are no messages or contacts the Briar on user's device

- [ ] Turning the device into landscape orientation during the setup process, during the database decryption, the process should continue successfully:
    entered text is kept
    progressbar is continuously displayed

- [ ] If Navigating back during the setup process, the app should continue to function successfully

**Tickets covered**

#1819 Crash if screen is rotated during setup and "Create Account" is tapped twice

I got the following crash on the Nokia 3.1 (Android 10) while trying to reproduce another bug. During setup, I rotated the screen to landscape and back to portrait after tapping "Create Account", which caused the progress wheel to disappear and the "Create Account" button to reappear. I tapped the button again and Briar crashed:

**MRs Covered**

MR1355 Test instructions

Condition display of progressbar on a isCreatingAccount LiveData 
* Precondition: fresh install, setting up a new account
 
    * Choose a name, tap next
    * Choose a password, tap next
      * Not testable on some devices which display "Create account" instead of "Next"
    * You are now on Background connections screen
    * Tap Back-button ◁
    * Ensure that password can be changed again
  * During setup process, rotate device and ensure that:
    * entered text is kept
    * progressbar is continuously displayed

[Back to Tetsing](https://code.briarproject.org/briar/briar/-/wikis/Testing)