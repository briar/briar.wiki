Briar settings: 
Settings > Connections > 
- Connect to contact via bluetooth = off
- Connect to contacts on the same wi-fi network = off
- Connect to contacts via internet = on

Device settngs: 
- Wi-fi = on
- Bluetooth = off

General: It is at all times during testing -- like after actions taken, events occuring
etc -- relevant to rotate the device and ensure that:

- Text entered in fields, switches toggled etc remain so
- Visibility of progressbars and buttons are the same
- Labels on buttons remain the same
- Opened dialog boxes remain so

Scenarios: 
1. Both devices online at the same time
- [ ] Use two devices with above settings and exchange their connection codes(send via email, sms, other chat apps etc) using the share button
- [ ] Verify that connection is successfully created and the new contact's name appears on top of the list on Contacts screen.  This should happen within seconds if both devices are online. 





[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)






  



