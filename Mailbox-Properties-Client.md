
The mailbox properties (or updates) client is a [BSP client](https://code.briarproject.org/briar/briar-spec/blob/master/protocols/BSP.md) that synchronises mailbox properties and supported mailbox api versions between pairs of devices. The properties describe how to connect to a mailbox associated with a device.

### Identifier

The client's identifier is `org.briarproject.bramble.mailbox.properties`. The major version is 1.

### Groups

The client uses a separate BSP group for communicating with each contact. The [group descriptor](https://code.briarproject.org/briar/briar-spec/blob/master/protocols/BSP.md#23-groups) is a [BDF list](https://code.briarproject.org/briar/briar-spec/blob/master/BDF.md) containing the unique IDs of the contacts' identities, sorted in ascending order as byte strings.

### Message types

**UPDATE** - The message body is a BDF list with 4 elements: `version` (int), `clientSupports` (list), and `serverSupports` (list), `properties` (dictionary).

`version` is incremented with each new update message.

`clientSupports` is a non-empty list of Mailbox API versions that the sender's Briar instance can talk to as a client. Each element (a version) is a list with 2 elements: `major` (int), `minor` (int).

`serverSupports` is a list of Mailbox API versions that the sender's Mailbox supports. The list must be empty if `properties` is empty (there is no mailbox), otherwise it must be non-empty. Each element (a version) is a list with 2 elements: `maj` (int), `min` (int).

The keys of the `properties` dictionary are strings. The dictionary of a well-formed update contains either no (0) entries, which means for the receiver that there is no mailbox available. Or, it contains all of the following entries:

* "onion" : String -- Onion of the mailbox (56 lower-case base32 chars, derived from the v3 Onion Service public key)
* "authToken" : byte[32] -- token used for authenticating with the mailbox
* "inboxId" : byte[32] -- folder id used when receiving data from the mailbox
* "outboxId" : byte[32] -- folder id used when delivering data to the mailbox

### Validity policy

* An update is valid if it is well-formed.

### Storage policy

* In the groups shared with contacts:
    * The local message with the highest version is stored.
    * The remote message with the highest version is stored.

### Sharing policy

* In the groups shared with contacts, all local messages are shared.
