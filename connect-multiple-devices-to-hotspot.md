#### Steps to execute 
**Device 1**
- Go to settings > share Briar app offline
- tap the Start Sharing button on the next screen
- screen displays the details of the network to which other devices need to connect

**Other devices**

- Go to device settings on each device and add a network the details of which are shown  by device 1

#### Expected results: 
- All devices successfully connect to the device1
- They remain connected to it when browser is started up 

**Device 1**
- Briar Toast message shown at the bottom of the screen 'Successfully connected. Show download info' after the first successfuly connection.  This message is not shown for other subsequent connections.  
- user can proceed by tapping the tappable 'show download info' on the message 
or 

- they can tap the Start app sharing button
- either way, the IP address of the download page is displayed on the next screen

**Other devices**

- all devices enter the IP address given by device 1 into the browser 
- all devices can successfully download the Briar app from teh website on device 1

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)

 
