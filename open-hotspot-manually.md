The objective of this test is to verify that Briar app successfully creates the hotpost to which another device is able to connect

Note; the onscreen text is being clarified #2080, so this tets may need to be updated with correct onscreen text inthe future, but the workflow as to what happens on each device remains correct.

#### Settings: 

**Device 1**
Wifi in app = ON
Wifi ondevice = ON


**Device 2**
Wifi in app = ON
Wifi ondevice = ON

#### Steps to execute:

**Device 1**
- in Briar app go to Settings > Actions > Share Briar offline
- tap Start sharing button
- screen opens that displays the wi-fi network details to be entered into the wi-fi settings of the device 2

**Device 2**
- go to device settings > connections > wi-fi
- add a wi-fi network by typing its name + password (available on device1) manually
- verify that the newly added wi-fi shows as  connected

Expected results: 

- new wi-fi network is created on device2 and connected (even though it has no access to internet, only to device1)




[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)











