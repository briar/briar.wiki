The purpose of this test is to verify that device2 can access the website on device1, via hotspot, and that is is able to download and instal the Briar apk file 


#### Settings:

**Device 1**
Hotspot open


**Device 2**
Connected to the device1's hotspot

#### Steps to Execute:

**Device 1**
- Display the IP address details on screen

**Device 2**
- Open the browser and type the exact IP address displayed in device1 into the address bar.
- Tap enter


#### Expected Result:
- Device 2 opens a screen showing the website from which Briar app download can start. 
- Tapping the Dowload Briar button starts the Briar apk file download
- When download complete, user is given the option to start Install (bottom of the screen)


[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)




