Please try to check every screen that you can find in Briar and look with eagle eyes if the layout is like you would expect it to be.

# RTL beta downloads:

* via official Google Play beta program:
 https://play.google.com/apps/testing/org.briarproject.briar.android
* https://briarproject.org/apk/briar-1.1.2.apk

# Examples for layout issues

![device-2018-10-02-164611](/uploads/ab3294a320b723ecb3f7986d6d591acf/device-2018-10-02-164611.png)

* Missing margin between forum icon and name

---

![device-2018-10-02-164558](/uploads/090d7d6d0062427393389d3b83041ca0/device-2018-10-02-164558.png)

* Reply button is on right side instead of left side
* Contact icon is above contact name, missing margins 
* send button is not mirrored

---

![device-2018-10-02-164823](/uploads/50def1e9bed04a657f67c4ed871a102b/device-2018-10-02-164823.png)

* Contact icon is above contact name, missing margins 
* Contact icon and name are on left side instead of right side
* text input has left margin that is too small

# Screens that are difficult to find

Many user interface elements are difficult to find in the app. They usually only show up after you added contacts, send messages, shared forums, etc.

There's some elements that are especially hard to find.
Please have a look at the following screenshots and see if they look alright for you.
These screens are difficult to find in the actual app. That is why we are providing screenshots here instead.

## App crashes

When Briar crashes, the user sees this message:

![device-2018-10-02-175354](/uploads/70f1af7fc7cd09e726721e49eafbab3b/device-2018-10-02-175354.png)

Clicking Send Report brings the user to:

![device-2018-10-02-175805](uploads/dd9a050e46b0630a836f10b986f5381a/device-2018-10-02-175805.png)

## Revealing Contact Relationships in Private Groups

When you have been invited into a private group, there's a hidden option for revealing contact relationships:

![device-2018-10-02-180219](uploads/f5cf48385d5e4bbf36b0860a0f5ce1ba/device-2018-10-02-180219.png)

## Importing RSS feeds

In the blogs section, you can import and manage RSS feeds.

![device-2018-10-02-180630](uploads/48430b43d75fd7666ef27f63b132d93e/device-2018-10-02-180630.png)
![device-2018-10-02-180703](uploads/3ba9680dd47628a80b52da5037d2d5c7/device-2018-10-02-180703.png)

## Error scanning QR codes

![device-2018-10-02-181420](uploads/51f3350d23a89836cfbdfbb9e305f39c/device-2018-10-02-181420.png)

## Introduction not possible

When you have two contacts, you can introduce them to each other. If there's already an introduction in progress, this screen will appear:

![device-2018-10-02-181612](uploads/0de9046621b7dd9c6aa08d1a0fb56198/device-2018-10-02-181612.png)

## Conversation messages

Are the icons mirrored correctly?

![device-2018-10-02-181812](uploads/b3815d6a8e2938ba90bf925f97f4782c/device-2018-10-02-181812.png)