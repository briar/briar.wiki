This test addresses a comment 



"I think we already do this, but it needs testing. When opening the mailbox after the device went to sleep, we should show a dialog asking for battery optimization exception again." in the ticket 

https://code.briarproject.org/briar/briar-mailbox/-/issues/120#note_68771 


 and ticket https://code.briarproject.org/briar/briar-mailbox/-/issues/145
 and ticket https://code.briarproject.org/briar/briar-mailbox/-/issues/120


In the ticket 120, there is a comment that says 

"When opening the mailbox after the device went to sleep, we should show a dialog asking for battery optimization exception again."

The sleep this comment is talking about is not simply the sleep when device is inactive for a few mins and the screen goes black.  Instead, it is a kind of sleep that Android invokes if it sees that an app uses too much memory, or battery etc, based on its own logic.  

To put a device into this Android invoked 'doze', issue this command

adb shell dumpsys deviceidle force-idle

(just in case I forget after awhile - adb commands need to be issued from the folder containing adb folder, which are in different locations on mac and windowds machines)

adb shell dumpsys deviceidle unforce

if permission is denied, it could be because the USB debugging is off for the device ie the Android Studio doesn't have the phone's permission to access it

To see if connection to device is OK, type in > ./adb devices (on mac) and .(backward slash)adb devices on windows?

**memo, useful for testing this:**

- enable battery optimization:

`adb shell dumpsys deviceidle whitelist -org.briarproject.mailbox`
the output: **Removed: org.briarproject.mailbox**

- disable battery optimization:

`adb shell dumpsys deviceidle whitelist +org.briarproject.mailbox`
the output is: **Added: org.briarproject.mailbox**




**Steps to execute **

- adb shell dumpsys deviceidle whitelist -org.briarproject.mailbox
output is - **Removed: org.briarproject.mailbox**
- adb shell dumpsys deviceidle unforce
- open mailbox app and let it start to status screen for example
- switch to a different app
- adb shell dumpsys deviceidle force-idle
- switch back to mailbox app
- dialog should appear then

With the above steps whilst the mailbox is running in the background, I have reproduced the dialogue box saying that mailbox is not able to run in the background, and showing the two buttons Cancel and Fix. 

Once the dialogue box appeared after the 'unforce' step, and once after the 'force-idle' step.  

What should happen after the user taps on Fix, is that do-not-kill-me fragment should be shown to the user.  However, what should the notifications say at this pont?  

The notification icon stays there all the time - but to check what notifications themselves say... and what they should be saying 

At the moment notifications say Briar Mailbox running - this will need to be adapted to the more appropriate message if the mailbox is not able to run in the background


[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)