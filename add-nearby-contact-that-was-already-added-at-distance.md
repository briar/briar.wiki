**Scenario 1**
- Add two contacts at a distance
- Give them the name that is their true username in Briar
- Then connect them nearby
- Message 'contact already exists'. 

**Scenario 2**
- Add two contacts at a distance, 
- Give each a name that is different from their nickname in Briar
- Try to add them neaby again
- Message 'contact already exists'. 

**Scenario 3**
- add neaby contacts
- then try to add the same contacts at a distance by giving each one their respective nickanmes in Briar app
- the contacts should not be added and the toast message should be correct

**Scenario 4**
- add neaby contacts
- then try to add the same contacts at a distance by giving each one a name that is different from their real nickname in Briar app
- the contacts should not be added and the toast message should be correct

**Scenario 5**
- add two cntacts at a distance and then delete one of them
- then try to recreate the same contacts by adding them to each other nearby... 
- what happens is: the nearby contact gets created on one device and the distance contac (that was nto deleted) is still present on the other device.  They cannot send each other messages. https://code.briarproject.org/briar/briar/-/issues/2 Known issue.
 




[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/Testing)