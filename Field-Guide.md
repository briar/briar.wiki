This is a *work in progress* for how Briar can be used during internet shutdowns.

**The Briar project is interested in receiving feedback (anonymous or not) about the use of Briar during internet shutdowns.**

* What worked well, what didn't?
* How useful was Briar?
* What would need to be added/improved for it to be more useful?

## Random thoughts

* Ideally form groups and practice before the shutdown happens
* Form pairs and use the "Connect via Bluetooth" feature to test pair-wise Bluetooth connectivity (might need distance from other pairs to avoid interference)
* Create private groups or forums and ensure that all members share/reveal the forum/group with each other to allow messages flowing along all edges of the social graph (mesh like)
* If some people didn't install Briar before the shutdown, the "share app offline" feature can be used to install Briar on their devices
* 🚴 couriers can be used to transport information between groups on USB sticks and SD cards
* Send feedback through the app when it crashes or after strange things happen. The feedback will be sent (encrypted and anonymously over Tor) when Briar has internet connectivity again


## Risks when using Briar at protests

During an internet shutdown, security forces might assume that anyone using a mobile phone is either taking photos, recording video, or using an offline communication app. So the simple act of **paying attention to their phone** during a shutdown is something protesters might want to be conscious of.

Network surveillance equipment can detect the Bluetooth and Wi-Fi signals emitted by nearby phones. We don't know if any government currently does anything more sophisticated than recording the MAC addresses of phones, which are unique identifiers that could be used to track individuals or to confirm (in real time or retrospectively) whether certain individuals are/were near a certain place at a certain time. This is similar to what can be done with IMSI catchers (or cell tower logs) regardless of whether the phones are using Bluetooth/Wi-Fi.

In theory, surveillance equipment could also record which MAC addresses were communicating with each other, at what times, and how much data was exchanged. Briar encrypts all the data and metadata exchanged via Bluetooth and Wi-Fi, so no messages or usernames would be exposed, but the fact that two phones were communicating would reveal that their owners were contacts. We don't know if currently available surveillance equipment collects this information, but it's possible.

Jamming of Bluetooth and Wi-Fi is likewise possible, but we've never heard reports of it happening. Apps like Briar would probably have to get a lot more popular before governments started buying equipment to jam them. However, we know that Roskomnadzor [was looking for contractors to develop anti-mesh network tools](https://zakupki.gov.ru/223/purchase/public/purchase/info/documents.html?lotId=12243338&purchaseId=9230637&purchaseMethodType=IS).

As with any tool used by protestors, infiltrators can pass received messages and identities to their handlers, and can use the tool to spread misinformation or disrupt conversations by spamming or trolling. These vulnerabilities are not specific to Briar though.

## Range boosting with Wi-Fi hotspots

* A backpack with a battery powered Wi-Fi hotspot and a decent antenna can help to get better range
* Use a Wi-Fi password for the hotspot and share it with all members of the group
* Have everyone use and test the Wi-Fi with Briar when there's still internet (so Briar can learn the LAN addresses of the devices it will need to connect to through the Wi-Fi hotspot)