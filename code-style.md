* Lines no longer than 80 characters
* Tabs for indentation, tab width of 4
* Egyptian brackets (`else/catch/finally` continue the same line as the preceding bracket)
* `if/else/for/while` may omit brackets if the body fits on a single line
* If the `if` uses brackets, the `else` should too (and vice versa)
* Use `foo` rather than `mFoo`, `_foo`, etc for field names
* Use `foo` rather than `this.foo` unless there's also a local `foo`
* `CONSTANTS_LIKE_THIS`, `variablesLikeThis`
* `AbcCamelCase` rather than `ABCCamelCase`

The configuration for Android Studio to format source files like this is checked
into version control so that the formatter should apply this style out of the box
(Using Ctrl+Alt+L).

# Avoid Staircase Indent

Android Studio does not remove manual line breaks or line breaks that have been added
by the formatter before. Sometimes, e.g. when adding arguments or renaming methods or variables,
this can lead to staircase-like indent such as this:

```
someVariable
    .someMethod(
        new LongClassNameWithSomeParameters(int foo, long bar));
```

Please avoid this and help the formatter by manually removing the first line break:

```
someVariable.someMethod(
        new LongClassNameWithSomeParameters(int foo, long bar));
```

# Tab Size in Gitlab

By default Gitlab expands tabs to 8 characters in source code and diff views
rather than 4 that we use in Android Studio. As a result the code
looks very different in the browser than in the IDE. You can configure your
personal tab width in your Gitlab [profile preferences](https://code.briarproject.org/profile/preferences)
(Scroll to `Behavior` → `Tab width`)

# Static Imports

Please use static imports of methods and fields such as `import static android.widget.Toast.LENGTH_LONG` wherever possible so that you can write `LENGTH_LONG` instead of `Toast.LENGTH_LONG`. Exceptions are cases where a static import reduces clarity such as writing `Builder` instead of `AlertDialog.Builder` or `MAX_VALUE` instead of `Integer.MAX_VALUE`.