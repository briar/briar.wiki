note: this test does not have to do with first install, but once created, it seems that the page title cannot be eaisly edited... so... ignore the 'first install'.  This test is to do with navigation to and from tge do-not-kill-me fragment

Steps to execute 


- Question in MM 5/5/22 - should it be possible to navigate back from the do-not-kill-me fragment when intro is not skipped?  Ie this fragment is shown after the 4th intro step, and on allprevious screens it is possible to navigate one screen back by tappin on back button.  But from this screen it is not possible.  Is that correct?  This makes sense when the intro is skipped and there is no previous screen tonavigate to, but if intro is not skipped, then there is previous step and it the user may want to go ack to that screen.  A decision needs to be made whether this will be implemented or not - to keep track of this issue a ticket was reported https://code.briarproject.org/briar/briar-mailbox/-/issues/119. 
- 
[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/Testing)