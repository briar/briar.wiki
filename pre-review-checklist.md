## Java

#### Thread safety
* Classes should be immutable where possible
* Classes that may be used by multiple threads must be thread-safe
* If a class is not thread-safe, annotate it @NotThreadSafe
* Fields that are used by multiple threads must be volatile or guarded by locks
* If a field or method is guarded by a lock, add a comment, e.g. `// Locking: foo`

#### Visibility
* Minimise the visibility of classes, fields and methods
* Fields should usually be private, with getters and setters if needed
* Fields should be final where possible
* Inner classes should be static where possible

#### Constructors and Dependencies
* Never allow `this` to escape the constructor
* Complex dependencies should be injected or constructed by factories
* Avoid static methods, except for simple and ubiquitous tasks
* Use executors rather than creating threads where possible

#### Exceptions
* Use checked exceptions rather than special return values to indicate errors
* Unchecked exceptions should only be used for programming errors
* If you catch an InterruptedException in a synchronous method, interrupt the current thread so the caller learns about the interrupt
* RuntimeExceptions thrown by our own code are expected to crash the app, and RuntimeExceptions thrown by libraries should either be caught immediately or allowed to escape and crash the app - but shouldn't be allowed to propagate through layers of our own code before being caught.

#### Blocking
* Don't call blocking methods from the UI thread
* Don't call blocking methods from event handlers

#### Locks
* Avoid using locks where possible
* Use dedicated lock objects rather than `synchronized`
* Don't call into other classes while holding locks
* Don't start database transactions while holding locks

## Android UI

#### Icon Drawables

* Use `app:srcCompat` instead of `android:src` to define drawables in XML files
* Use `setImageResource()` to set icons instead of `setImageDrawable()`
* If you need to set a drawable directly, but can't use `setImageResource()`, use `VectorDrawableCompat.create()` to get the drawable
* Don't use vector(-only) drawables for notification icons (unless that's supported now)
* Test icon display on API < 21