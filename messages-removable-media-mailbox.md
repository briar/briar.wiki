If messages are exported to removable media, but not uploaded and acked yet, then they can also be uploaded to mailbox and vice versa. 

https://code.briarproject.org/briar/briar/-/issues/2265

This depends on 'latency period', which means that messages exported to the removable media can also be uploaded to the mailbox before the mailbox 'latency period' expires.  

Let's say that mailbox latency period is 14 days, and the removable media 28 days.  

Then we have the following scenarios - 

**Sender**

| is online | msg type | has MB | MB online | exports msg | before/after MB upload | receives acks | ack medium |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | 
| yes | text | yes | yes | yes | after | yes | MB | 
| yes | photo | yes | yes | yes | after | yes | Rem Media | 
| yes | profile pic | yes | no | yes| before | yes | Rem Media  | 
| no | make intro | yes | yes | yes | before | yes | MB | 

**Recipient1**

| is online | msg type | has MB | MB online | exports ack | before/afterMB upload | 
| ------ | ------ | ------ | ------ | ------ | ------ |
|no | text | yes | yes | yes | na | 
| no | photo | yes | yes | yes | after | 
| no | profile pic| no | yes | yes | after | 
| yes | make intro | yes | yes | no | na | 

**Scenario 1**
Recipient receives message from the removable media, but sends ack via mailbox
In this scenarios,  a text message is created by the sender, and they have a MB which is available online.  So the message they created is uploaded to their mailbox.  
- The message is not downloaded - let's say that the intended recipient is offline and cannot download the message
- The sender can export the same message onto the removable media.
- The sender goes offline after this
- The recipient can upload the message from the removable media. 
- The recipient comes back online
- The recipient's mailbox is online, which means that the ack their Briar app generates will be uploaded onto their MB
- Before sender comes back online, recipient is able to export the ack onto the removable media
- Sender comes back online and received the ack from the recipient's mailbox
- The message that the sender sent to the recipient now has 2 ticks
- Sender tries to import the ack from the removable media, but cannot (as the ack was already downloaded from the MB)

**Scenario 2**

**Sender**
| is online | msg type | has MB | MB online | exports msg | before/after MB upload | receives acks | ack medium |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | 
| yes | photo | yes | yes | yes | after | yes | Rem Media | 

**Recipient**

| is online | msg type | has MB | MB online | exports ack | before/afterMB upload | 
| ------ | ------ | ------ | ------ | ------ | ------ |
| no | photo | yes | yes | yes | after | 


Recipient receives message from sender's mailbox and sends ack via removable media
- a photo message is created by the sender, and this message is uploaded to their mailbox as the intended recipient is offline.
- Sender goes offline
- Recipient comes online, and receives a message from the sender's mailbox
- Thier own ack is generated automatically and ploaded to their own mailbox, as the sender is now offline. 
- The recipient exports the ack to removable media 
- The sender imports the ack from the removable media and the message gets the second tick
- The sender comes back online
- at this point the ack sent to them by the recipient's mailbox should be downloadd and deleted - but for the user this is transparent - they don't see any changes on their screen while this is being done.
- Now both recipient and the sender of the original message are online and they message each other directly, without messages passing through MB

**Scenarios 3**
In this scenario sender has a MB which is offline initially, so their only option is to export the messages for the intended recipient who is also offline initially. The intended recipient does not have the MB app, so they can only import the message from removable media.  
**Sender**

| is online | msg type | has MB | MB online | exports msg | before/after MB upload | receives acks | ack medium |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | 
| yes | profile pic | yes | no | yes| before | yes | Rem Media  |

**Recipient**

| is online | msg type | has MB | MB online | exports ack | before/afterMB upload | 
| ------ | ------ | ------ | ------ | ------ | ------ |
| no | profile pic | no | yes | yes | after | 

- sender sends a message via removable media, as their own mailbox is offline
- recipient is offline, does not have MB and imports the message from the removable media. 
- at this point the sender's mailbox comes back online, and the exported message gets uploaded to the sender's MB
- it tries to forward the message to the recipient, who is still offline
- sender goes offline
- recipient comes back online
- sender's mailbox will now try to forward them the message that they already uploaded from the removable media 
- For the user, this is transparent, but the message should be donwloaded and deleted
- the recipient exports the ack
- the sender imports the ack from the removable media
- the sender comes back online and can message the recipient directly, and recipient receives messages, generates acks and sends messages as normal.
- recipient comes back online

**Scenario 4**

In this scenario we check what will happen in a more complex situation where some recipients have a mailbox and some do not

**Sender**

| is online | msg type | has MB | MB online | exports msg | before/after MB upload | receives acks | ack medium |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | 
| no | make intro | yes | yes | yes | before | yes | MB | 

**Recipient1**

| is online | msg type | has MB | MB online | exports ack | before/afterMB upload | 
| ------ | ------ | ------ | ------ | ------ | ------ |
| yes | make intro | yes | yes | no | na | 

**Recipient2**

| is online | msg type | has MB | MB online | exports ack | before/afterMB upload | 
| ------ | ------ | ------ | ------ | ------ | ------ |
| no | make intro | no| na | yes | na | 


The message sender is offline, and they want to make intro between two contacts. 
- sender exports the 'make intro' message to recipient1, who is online
- recipient1 imports the 'make intro' message from removable media, and accepts the intro
- sender comes online and their 'make intro' message to recipient2 is uploaded to their mailbox.
- sender receives the ack from recipient 1
- sender goes offline
- recipient 2 is offline, therefore they cannot receive the 'make intro' message from the sender's malbox
- sender exports the 'make intro' message onto removable media 
- recipient 2 imports the 'make intro' message from removable media
- recipient2 comes back online
- recipient2 denlines the 'make intro' but this cannot be sent, as the sender is offline - so this decline can be exported either to removable media or to mailbox. 
- recipient 2 installs the mailbox and pairs it with their briar app
- the decline 'make intro' message is uploaded to their mailbox
- sender comes back online
- they receive the decline via the recipient 2's mailbox
- 




[Back to testing](https://code.briarproject.org/briar/briar/-/wikis/Testing)