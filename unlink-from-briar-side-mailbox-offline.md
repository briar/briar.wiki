Steps to execute: 

- Switch the internet off on the Mailbox device
- Go to the Briar device and tap on the Unlink on the mailbox status page
- (check the briar notifications and see if they change during this process)
- Tap on 'got it' in the dialog box when it comes up
- go to briar > settings > mailbox and verify that a new connection can be made with another instance of mailbox on another device (and check the briar notifications)
- go to the mailbox device, and try to unlink without switching the internet back on
- mailbox should display the 'offline' screen - check the mailbox notifications at this point.
- go to device settings and switch the internet on
- then go to mailbox status page and tap unlink (check the notifications)
- after unlinking, tap on Finished (check notifications)
- Verfiy that QR code is displayed successfully when the app is restarted and that new connection can be made with another instance of briar (or the same one after successful unlink)

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)
