The 'waiting to come online' contacts should time out after certain time (48 hours).

* After that the contact stays in the Pending list of contacts but its status changes to Failed.
* Recreate the contact at a distance after a time-out and verify that it can be created successfully by both contacts exchanging codes
* Once created, contacts can send each other messages

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)