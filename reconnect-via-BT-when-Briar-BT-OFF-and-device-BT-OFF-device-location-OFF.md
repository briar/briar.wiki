Verify that bluetooth connection can be made when bluetooth setting in the device itself as well as in the Briar app is OFF + location setting on the device is OFF.

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)