This test will verify that the Briar app and mailbox behaviour is as expected when a sender sends invitation to join their private group to the contact who is offline, and who may the MB app installed, or not, and who may export their acceptance or decline via removable media or via mailbox, or answer directly by connecting via internet or BT. 

- invite multiple contacts - some of whom are offline some online 
- export the invitation to removable media 
- invite via internet, but reply via MB
- invite multiple contacts via BT, reply via mailbox, exporting the replies and via internet.
- sender offline, their MB comes online later, then their contact comes online (after the invitation has been uploaded to sender's MB)
- sender doesn't have MB, but the invitee has 
- ender does not have the MB, invitee installs  and pairs it after the invitation has already been created


**Scenario 1**

**Sender**

| is online | msg type | has MB | MB online | exports msg | before/after MB upload | receives reply | reply medium |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | 
| yes | invitation to private group | yes | yes | no | n/a | yes | MB | 



**Recipient1**

| is online | msg type | has MB | MB online | exports ack | before/afterMB upload | 
| ------ | ------ | ------ | ------ | ------ | ------ |
|no | invitation to private group | yes | yes | no | n/a | 

In this scenario, the sender who sends the invitation is online, and has the MB installed and paired with their Briar app.  The contact whom they want to invite is offline, and also has a MB installed and paired.

- sender sends the invitation to their private group to the contact who is offline
- invitation is uploaded to their mailbox
- sender goes offline
- recipient comes back online
- they receive the invitation from the sender's mailbox
- they accept the invitation 
- their acceptance is uploaded to their MB
- recipient goes offline
- sender received reply from the recipient's MB



**Scenario 2**

**Sender**

| is online | msg type | has MB | MB online | exports msg | before/after MB upload | receives reply | ack medium |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | 
| yes | invitation to private group | yes | yes | no | n/a | yes | any | 



**Recipient1**

| is online | msg type | has MB | MB online | exports reply | before/afterMB upload | 
| ------ | ------ | ------ | ------ | ------ | ------ |
|no | invitation to private group | yes | yes | no | n/a | 


- sender sends the invitation to their private group to the contact who is offline
- invitation is uploaded to their mailbox
- sender goes offline
- recipient comes back online
- they receive the invitation from the sender's mailbox
- they accept the invitation 
- their acceptance is uploaded to their MB
- recipient remains online
- sender receives reply either from the recipient's MB or cirectly from the recipient's Briar app.  This is transparent for the user.  **TBD if it is necessary to verify that that messages are donloaded and deleted by mailbox either way.  If it is necessary, then how can this verification be done? **

**Scenario 3**

**Sender**

| is online | msg type | has MB | MB online | exports msg | before/after MB upload | receives reply | ack medium |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | 
| yes | invitation to private group | yes | yes | no | n/a | yes | any | 



**Recipient1**

| is online | msg type | has MB | MB online | exports reply | before/afterMB upload | 
| ------ | ------ | ------ | ------ | ------ | ------ |
|no | invitation to private group | yes | yes | no | n/a | 


- sender sends the invitation to their private group to the contact who is offline
- invitation is uploaded to their mailbox
- sender remains online
- recipient comes back online
- they receive the invitation from the sender's mailbox, or via direct message from sender's Briar app
- they decline the invitation 
- their reply is delivered to the sender via direct message from their Briar app to the sender's Briar app, as they are both online. This is transparent for the user.  **TBD if it is necessary to verify that that messages are donloaded and deleted by mailbox either way.  If it is necessary, then how can this verification be done? **

**Scenario 4**

**Sender**

| is online | msg type | has MB | MB online | exports msg | before/after MB upload | receives reply | ack medium |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | 
| yes | invitation to private group | yes | yes | no | n/a | yes | any | 



**Recipient1**

| is online | msg type | has MB | MB online | exports reply | before/afterMB upload | 
| ------ | ------ | ------ | ------ | ------ | ------ |
|yes | invitation to private group | yes | yes | no | n/a | 


- sender sends the invitation to their private group to the contact who is online
- invitation is sent directly to the recipient
- sender goes offline
- recipient replies, and their replyis uploaded to their Mailbox.
- recipient goes offline. Both users are offline now, but their mailboxes are online.
- sender comes back online 
- sender receives the reply messsage via recipient's MB.



[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/Testing) 
