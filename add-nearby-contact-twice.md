**Steps to execute**

* Add a nearby contact on two devices
* The attempt to add the same two contact via nearby method 

**Expected results**

* The same contacts are not added twice
* User gets a notification message saying that the contacts they tried to add already exists

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)