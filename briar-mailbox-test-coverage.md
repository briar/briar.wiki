
| Briar Mailbox ID | Title | Regression Test | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
|https://code.briarproject.org/briar/briar/-/issues/1011 | Offline message delivery (mailbox) | n/a| Epic | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/1804 | Define REST API for communication between Briar and Mailbox | n/a | Epic | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/1805 | Briar pairing with mailbox | n/a | Epic | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/1806 | Manage mailbox connections | n/a | Epic | ------ | ------ | ------ | Duplicate |
| https://code.briarproject.org/briar/briar/-/issues/1807 | Upload data to mailbox | n/a | Epic | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/1808 | Download data from mailbox | n/a | Epic | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/1809 | Subscribe to RSS feeds offered by mailbox | n/a | Epic | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/1810 | Download RSS feeds from mailbox | n/a | Epic | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/1811 | Update Bramble protocol stack to support syncing via mailbox | n/a | Epic | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |

| Briar Mailbox ID | Title | Regression test | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2156 | Upgrade OkHttp to 3.14.x | Open | To test | To do | ------ | ------ | Priority2? |
| https://code.briarproject.org/briar/briar/-/issues/2161 | Design UI for mailbox setup: pairing briar with mailbox app | To test | Tested | https://code.briarproject.org/briar/briar/-/wikis/onboarding-UI   | yes | OK | ------ |
| ------ | ------ | To test | ------ | https://code.briarproject.org/briar/briar/-/wikis/user-leaves-setup | ------ | ------ | ------ |
| ------ | ------ | To test | ------ | https://code.briarproject.org/briar/briar/-/wikis/open-battery-settings | ------ | ------ | ------ |
| ------ | ------ | To test | ------ | https://code.briarproject.org/briar/briar/-/wikis/device-offline-warning | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2162 |Implement UI for pairing Briar with mailbox | Closed | Tested | https://code.briarproject.org/briar/briar/-/wikis/mailbox-is-running https://code.briarproject.org/briar/briar/-/wikis/scan-qr-code | yes | OK | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2163 | Design status UI for mailbox connection | To test | Tested | https://code.briarproject.org/briar/briar/-/wikis/mailbox-is-running https://code.briarproject.org/briar/briar/-/wikis/time-last-connected | yes | ok | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2164 | Design UI for unpairing the mailbox | To test | To test | https://code.briarproject.org/briar/briar/-/wikis/unlink-mailbox | yes | ok | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2167 | Decode QR code for pairing with mailbox | To test | To test | see here https://code.briarproject.org/briar/briar/-/wikis/testing paragraph Mailbox Pairing | yes | OK | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2168 | Implement backend for pairing Briar with mailbox | n/a | To test | see here https://code.briarproject.org/briar/briar/-/wikis/testing paragraph Mailbox Pairing | yes | OK | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2169 | Store hidden service address and auth token for own mailbox in Briar DB | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2170 | Method for connecting and authenticating to own mailbox | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2171 | Store time of last attempted and successful connections to own mailbox | To test | To Test | https://code.briarproject.org/briar/briar/-/wikis/time-last-connected | yes | OK | ------ |

| Briar Mailbox ID | Title | Regression test | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2172 | Implement status UI for mailbox connection | To test | To test | see here https://code.briarproject.org/briar/briar/-/wikis/testing chapter Mailbox Status| yes | OK | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2173 | Implement UI for unpairing the mailbox | To test | To test | see here https://code.briarproject.org/briar/briar/-/wikis/testing chapter Unlinking Mailbox | yes | OK | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2174 | Check own mailbox when coming online | To test | To test | tested implicitely by https://code.briarproject.org/briar/briar/-/wikis/messages-uploaded-to-mailbox-correctly | yes | OK | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2175 | Implement UI for warning user when own mailbox is unreachable | To test | To test | https://code.briarproject.org/briar/briar/-/wikis/mailbox-not-reachable | yes | OK | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2180 | Design sync client for mailbox properties | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2181 | Implement sync client for mailbox properties | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2182 | Method for retrieving contact list from own mailbox | n/a | n/a | will be tested implicitely by #2188 | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2183 | Method for adding a contact to own mailbox | To test | To test | see here https://code.briarproject.org/briar/briar/-/wikis/testing chapter Contact Management | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2184 | Update mailbox properties when adding a contact to the mailbox | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2185 | Update mailbox properties when unpairing mailbox | n/a | n/a | ------ | ------ | ------ | ------ |

| Briar Mailbox ID | Title | Regression Test | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2186 | Method for connecting and authenticating to a contact's mailbox | To test | To test | This is tested by https://code.briarproject.org/briar/briar/-/wikis/testing Message upload and download | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2187 | Method for deleting a contact from own mailbox | To test? | To test | To do | ------ | ------ | how to test this? |
| https://code.briarproject.org/briar/briar/-/issues/2188 | Update mailbox's contact list when connecting to own mailbox | To test | To do | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2189 | Check contacts' mailboxes when coming online | To test | To Test | To do | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2190 | Method for resetting retransmission times | n/a | ??? | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2191 | Reset retransmission times when a contact's mailbox properties change | n/a | To do | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2192 | Reset retransmission times when unpairing own mailbox | n/a | To test | To do | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2207 | Method for checking own mailbox's status | To Test | To test | maybe covered implicitely by other tests related to status? | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2221 | Retire support for Android 4 | n/a | Epic | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2225 | Error handling for mailbox downloads | n/a | ???? | ------ | ------ | ------ | ----- |

| Briar Mailbox ID | Title | Regression Test | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2226 | Error handling for mailbox uploads | n/a | To test | To do  | ------ | ------ | rejected |
| https://code.briarproject.org/briar/briar/-/issues/2228 | Mailbox client manager | n/a | n/a | ------ | ------ | ------ | Black box |
| https://code.briarproject.org/briar/briar/-/issues/2229 | Mailbox client superclass | n/a | n/a | ------ | ------ | ------ | Black box |
| https://code.briarproject.org/briar/briar/-/issues/2230 | Add DB methods for tracking pending uploads | n/a | n/a | ------ | ------ | ------ | Black box |
| https://code.briarproject.org/briar/briar/-/issues/2231 | Method for uploading a file to a mailbox | n/a | n/a | Implicitely tested by other scenarios | ------ | ------ |  |
| https://code.briarproject.org/briar/briar/-/issues/2232 | Method for downloading a file from a mailbox | n/a | n/a | Implicitely tested by other scenarios | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2233 | Method for deleting a file from a mailbox | n/a | n/a| Implicitely tested by other scenarios | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2234 | Abstract task for calling an API endpoint | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2242 | Migrate OkHttp to bramble-core | n/a | n/a | ------ | ------ | ------ | Black box |
| https://code.briarproject.org/briar/briar/-/issues/2243 | Tests for OkHttp client calls | n/a | n/a | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |


| Briar Mailbox ID | Title | Regression test | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2250 | Refuse to start app on Android 4 beyond expiry date | To test | To test | To do | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2251 | Show a warning on Android 4 that Briar will expire | To test | To test | To do | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2257 | Method for wiping mailbox | To test | To do | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2261 | Include mailbox API version in local and remote mailbox properties | n/a | n/a | maybe implicitely tested by other scenarios| ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2265 | Replace ETA with max latency in retransmission logic | n/a | To test | To do | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2267 | Broadcast event when recording connection status of own mailbox | n/a | ??? | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2269 | Use full camera preview when scanning QR codes | To test | To test | To do | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2289 | Mailbox client for a contact's mailbox | n/a | n/a | ------ | ------ | ------ | Back box |
| https://code.briarproject.org/briar/briar/-/issues/2290 |Mailbox client for our own mailbox | n/a | n/a | ------ | ------ | ------ | Black Box |
| https://code.briarproject.org/briar/briar/-/issues/2291 | Mailbox upload worker | n/a | n/a | ------ | ------ | ------ | Black box |


| Briar Mailbox ID | Title | Status | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2292 | Mailbox download worker for a contact's mailbox | ??? | To test | To what level of detail to test this? | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2293 | Mailbox download worker for our own mailbox | ??? | To test | To what level of detail to test this? | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2294 | Mailbox worker for updating our own mailbox's contact list | ??? | To what level of detail to test this? | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2295 | Let MailboxPropertyManager broadcast event when contact's mailbox props are updated | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2296 | Defer marking messages as sent/acked until file is uploaded | n/a | ??? | Is this black box for devices? | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2297 | Adapt status screen when Briar is not connected to Tor | Open | To test | To do | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2298 | Fetch and store mailbox's supported API versions when pairing mailbox | n/a | n/a | To do | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2299 | Method for fetching mailbox's supported API versions | n/a | To test | To do | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2299 | Method for fetching mailbox's supported API versions | n/a | n/a | ------ | ------ | ------ | Black box |
| https://code.briarproject.org/briar/briar/-/issues/2301 | Update contacts about change in mailbox versions that client supports | n/a | To test | To do | ------ | ------ | ------ |


| Briar Mailbox ID | Title | Status | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2302 | Update contacts about change in mailbox versions that our mailbox (server) supports | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2309| Troubleshooting wizard for own mailbox being unreachable | To test | To test| To do | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2311 | Remind user to wipe mailbox if it's unreachable when unpairing | To test | To test | To do | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2314 | Usability testing for mailbox connection issues | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2315 | Usability testing for mailbox setup and pairing | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2316 | Usability testing for Mailbox app | n/a | n/a | moved to Mailbox repo | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2319 | Show warning if own mailbox's API version is incompatible | Open | To test | To do | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2320 | FormatException when loading mailbox API version metadata | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2326 | Briar's mailbox status screen should fetch the mailbox's supported API versions | ??? | To do | ------ | ------ | ------ | related to 2319 |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |

| Briar Mailbox ID | Title | Regression test | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2336 | DatabaseComponent#getNextSendTime() should consider latency | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2337 | Briar on PIxel 2 able to link with two mailbox devices | To test | To do | ------ | ------ | ------ | bug |
| https://code.briarproject.org/briar/briar/-/issues/2340 | Crash when unlinking briar and mailbox + wifi off | To test | To do | ------ | ------ | ------ | bug |
| https://code.briarproject.org/briar/briar/-/issues/2342 | Briar mailbox setup screen stuck sometimes | To test | To do | ------ | ------ | ------ | bug |
| https://code.briarproject.org/briar/briar/-/issues/2343 | End-to-end integration tests for communication via mailbox | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2346 | Scanning Mailbox QR code uses lower half of camera image? | n/a| ------ | ------ | ------ | ------ | Not an issue |
| https://code.briarproject.org/briar/briar/-/issues/2352 | Don't upload to mailbox when directly connected to contact | n/a | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2354 | After remote wiping - add a small confirmation message that wiping was successful | To test | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2356 | Investigate flaky unit tests | n/a | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2358 | Rotating screen while "Your Mailbox has been unlinked" dialogue box is displayed causes blank screen | To test | ------ | ------ | ------ | ------ | ------ |


| Briar Mailbox ID | Title | Status | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2360 | Mailbox Problem notification causes crash on Android 4 | To test | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2363 | Mailbox unreachable screen on Briar side - landscape - Samsung 6810 (small screen) links overwriting each other | To test | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar/-/issues/2367 | Navigating back from Connection screen to Mailbox setup screens in Briar - second mailbox setup screen overwrites the first one | To test | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |


| Briar Mailbox ID | Title | Status | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |


| Briar Mailbox ID | Title | Status | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |