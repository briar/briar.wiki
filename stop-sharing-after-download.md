#### Steps to execute 
- Tapping on 'stop sharing' button closes the hotspot and the wifi network is not available any more.

#### Expected results 
- Verify that no device can connect with the sharing device after the 'stop sharing' button is tapped
- The hotspot screens disappear after the 'stop sharing' button is tapped

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)

