The purpose of this test is to verify that hotspot can be successfully opened by scanning the QR code.

Note: this can only be done using devices that have the QR scanning tool in their settings > connection > add network, or devices that have a QR scanning app installed, or devices that are able to take a photo of a QR code and successfully interpret it (even if they have no separate QR reader app installed)

#### Settings: 

**Device 1**
device settings wifi=ON
Briar app settings wifi = ON

**Device 2**
device settings wifi=ON
Briar app settings wifi = ON

#### Steps to Execute:

**Device 1**
- Go to Settings > Share Briar offline
- Tap Start sharing button
- New screen shows the details of Wi-Fi network that the device2 needs to connected to 
- Go to QR code tab

**Device 2**
- Go to Settings on the device2 (not Briar settings, but the device settings)
- Go to Connections (or Network and internet) > Wi-Fi
- Add another Wi-fi Network
- If the device has a little QR code symbol next to the Add network option, tap that symbol
- This should open the QR code scanner tool
- Scan the QR code from the device 1
- A new wi-fi network should apear in the list of available wi-fi network, and its name should correspond to the name stated on the screen on device 1 that shows the details of the wi-fi/hotspot to connect to
- No password should be required to be entered by the user, as that is all part of the QR code 
- Verify that this new wi-fi network is connected (even though it has no access to internet)
- Navigate away from that screen, and then back and verify that the newly created wi-fi network is still connected



**Expected Results**
- wi-fi network is created and connected successfully

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)