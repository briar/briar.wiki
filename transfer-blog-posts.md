#### Steps to execute: 
**Settings:**
- Connections > Bluetooth, Internet and Wifi = OFF

**Steps** 
- On a device that is now offline (as its connections were switched off) create a blog post 
- Export data from that device to 2 contacts with whom the blog as shared. 
- Go to the conversation screen of the first contact, and then go to menu > connections > transfer data > send data > choose a file to export 
- Give the file a name and finish the export. If using the USB key, unmount or eject the USB by going to notification and tapping on Eject.  If this is not done, it may corrupt the file and render it unreadable for the recipient device.
- export to on contact via USB and to the other via SD card
- on the recipient's device, go to the conversation screen of the contact whose data you wish to import
- Go to the menu > connections > transfer data > receive data > choose a file to import and select the file that was exprted in the previous steps
- Tap on import

**Expected results**

- The  blog post should be imported correctly 
- blog posts shoudl be disaplyed on screen only once
- tapping on bloggers name should display a list of all blog entried written by that blogger
- tapping on blog post contents should display the details of that blog posts

**Variations**
- repeat previous steps for the second contact with whom the blog was shared
- send export and import several blogposts and reblogs

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)
