When Mailbox is running and linked to its corresponding Briar app, both Briar and Mailbox have Mailbox status screen.  On each screen there is a red coloured Unlink link at the bottom of the screen.

UNlinking can be done from the Briar side or from the Mailbox side.  There are three basic cases here;

- If Mailbox is online, then Briar will wipe it remotely, and the when looking at the Mailbox screen, a user will see Wiping progress wheel, and the the 'Wiping Complete' screen.  
- If Mailbox is offine, or Briar is unable to reach it, and if the user taps Unlink on the Briar screen, there will be a dialogue box informing the user that the unlinking was done and what they need to do on the Mailbox side. 
- If the unlinking is done from the Mailbox side, then it needs to be also repeated from the Briar side.  Briar will not be able to connect to mailbox, and the troubleshooting screen will guide the user to unlink from Briar side and perform the linking again.  

Of course, many scenarios are possible here, with rotating screens at different point, apps in background or foreground, navigting away from screens, with do-not-keep-activities setting being ON or OFF, apps being online or offline etc... So.. here follow scenarios to perform these tests:

These were all executed.  To help plan what tests to execute, I used a very primitive spreadsheet - see attached.  It could be helpful in the future to plan further scenarios in this context, or to maybe decide what tests need to be executed again at a later stage.

[mailbox_wiping.ods](uploads/193633cddb760ecc543ca961147f0ad9/mailbox_wiping.ods)


| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: Samsung A01 Core Android  | Nokia 3.2 Android 10 | OK |
| 10 do-not-keep-activities = ON | 10 do-not-keep-activities = ON | ---- |
| wipe from Briar | status screen active | feature request https://code.briarproject.org/briar/briar/-/issues/2354 |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: Samsung A01 Core Android  | Nokia 3.2 Android 10 | OK |
| 10 do-not-keep-activities = ON | 10 do-not-keep-activities = ON | ---- |
| wipe from Briar | Mailbox in the background while remote wiping lasts | OK |



| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: Samsung A01 Core Android  | Nokia 3.2 Android 10 | ----- |
| 10 do-not-keep-activities = ON | 10 do-not-keep-activities = ON | ---- |
| wipe from Briar | Mailbox in the background at start, then in foreground while remote wiping ongoing | NOK  https://code.briarproject.org/briar/briar-mailbox/-/issues/155 |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: Samsung A01 Core Android  | Motorola E2 Android 6 | OK |
| 10 do-not-keep-activities = ON | 10 do-not-keep-activities = ON | ---- |
| wipe from Briar | Mailbox in the background at start, then in foreground, then background again while remote wiping ongoing | OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: Samsung A01 Core Android  | Motorola E2 Android 6 | ----- |
| 10 do-not-keep-activities = ON | 10 do-not-keep-activities = ON | ---- |
| wipe from Briar | Mailbox app device screen asleep | NOK https://code.briarproject.org/briar/briar-mailbox/-/issues/155 |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: Samsung A01 Core Android  | Motorola E2 Android 6 | ----- |
| 10 do-not-keep-activities = ON | 10 do-not-keep-activities = ON | ---- |
| wipe from Briar | Mailbox screen active, tap back button during the remote wiping | NOK https://code.briarproject.org/briar/briar-mailbox/-/issues/155 |


| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: Samsung A01 Core Android  | Motorola E2 Android 6 | ----- |
| 10 do-not-keep-activities = ON | 10 do-not-keep-activities = ON | ---- |
| wipe from Briar | Mailbox screen tap stop button during the remote wiping | NOK https://code.briarproject.org/briar/briar-mailbox/-/issues/155 |



| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: HTC One M9 Android 7  | Samsung I9195S Mini Android 4.4.2 | OK |
| 10 do-not-keep-activities = ON | 10 do-not-keep-activities = ON | ---- |
| wipe from Briar | Mailbox screen rotate during remote wiping | OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: HTC One M9 Android 7  | Samsung I9195S Mini Android 4.4.2 | OK |
| 10 do-not-keep-activities = ON | 10 do-not-keep-activities = ON | ---- |
| wipe from Briar | Mailbox screen rotate after remote wiping | OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: HTC One M9 Android 7  | Samsung I9195S Mini Android 4.4.2 | OK |
| 10 do-not-keep-activities = ON | 10 do-not-keep-activities = ON | ----- |
| wipe from Briar | Background Foreground after remote wiping, but before the Wiping Coomplete screen tapped OK | the Wiping complete screen closes even if the OK button not tapped.  This is as expected.  OK |


| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: HTC One M9 Android 7  | Samsung I9195S Mini Android 4.4.2 | OK |
| 10 do-not-keep-activities = ON | 10 do-not-keep-activities = ON | ----- |
| wipe from Briar | Sleep after remote wiping, but before the Wiping Coomplete screen tapped OK |  On waking, the Wiping complete screen closes even if the OK button not tapped.  This is as expected.  OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: HTC One M9 Android 7  | Samsung I9195S Mini Android 4.4.2 | OK |
| 10 do-not-keep-activities = ON | 10 do-not-keep-activities = ON | ----- |
| wipe from Briar | Tap Unlink on mailbox side, during remote wiping from Briar | Wiping Complete.  OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: HTC One M9 Android 7  | Samsung I9195S Mini Android 4.4.2 | OK |
| 10 do-not-keep-activities = ON | 10 do-not-keep-activities = ON | ----- |
| Briar offline | Wiping mailbox while Briar is offline | Wiping Complete.  OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: HTC One M9 Android 7  | Samsung I9195S Mini Android 4.4.2 | OK |
| 10 do-not-keep-activities = Off | 10 do-not-keep-activities = Off | ----- |
| Wiping from Briar + rotate screen during wiping | Sceen active | Wiping Complete.  OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: HTC One M9 Android 7  | Samsung I9195S Mini Android 4.4.2 | OK |
| 10 do-not-keep-activities = Off | 10 do-not-keep-activities = Off | ----- |
| Unlink from briar after wiping complete on the MB side | Unlink from Mailbox | Wiping Complete.  OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: HTC One M9 Android 7  | Samsung I9195S Mini Android 4.4.2 | OK |
| 10 do-not-keep-activities = OFF | 10 do-not-keep-activities = OFF | ----- |
| Unlink from Briar side + navigate back immediately | Mailbox screen active | Wiping Complete.  OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: HTC One M9 Android 7  | Samsung I9195S Mini Android 4.4.2 | OK |
| 10 do-not-keep-activities = OFF | 10 do-not-keep-activities = OFF | ----- |
| Unlink from Briar, push Briar into background while wiping| Mailbox screen asleep | Wiping Complete.  OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: HTC One M9 Android 7  | Samsung I9195S Mini Android 4.4.2 | OK |
| 10 do-not-keep-activities = OFF | 10 do-not-keep-activities = OFF | ----- |
| Unlink from Briar and immediate logout | Mailbox screen asleep| Wiping Complete.  OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| Device: HTC One M9 Android 7  | Samsung I9195S Mini Android 4.4.2 | OK |
| 10 do-not-keep-activities = ON | 10 do-not-keep-activities = ON | ----- |
| Unlink from Briar when Mailbox offline | Mailbox offline | Wiping Complete.  OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| ------ | HTC E9 Android 5.2 | OK |
| ------ | 10 do-not-keep-activities = OFF | ----- |
| ------ | Wipe from Mailbox | Wiping Complete.  OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| ------ | HTC E9 Android 5.2 | OK |
| ------ | 10 do-not-keep-activities = OFF | ----- |
| ------ | Wipe from Mailbox, foreground, background, foreground | Wiping Complete.  OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| ------ | HTC E9 Android 5.2 | OK |
| ------ | 10 do-not-keep-activities = OFF | ----- |
| ------ | Wipe from Mailbox, screen asleep afterwards | Wiping Complete.  OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| ------ | HTC E9 Android 5.2 | OK |
| ------ | 10 do-not-keep-activities = OFF | ----- |
| ------ | Wipe from Mailbox, navigate back + restart Mailbox | OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| ------ | HTC E9 Android 5.2 | OK |
| ------ | 10 do-not-keep-activities = ON | ----- |
| ------ | Wipe from Mailbox, rotate while wiping | OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| ------ | HTC E9 Android 5.2 | OK |
| ------ | 10 do-not-keep-activities = ON | ----- |
| ------ | Wipe from Mailbox, rotate after wiping complete | OK |

| Briar | Mailbox | Result |
| ------ | ------ | ------ |
| ------ | HTC E9 Android 5.2 | OK |
| ------ | 10 do-not-keep-activities = ON | ----- |
| ------ | Wipe from Mailbox, sleep after wiping complete, but before tapping the OK buttonon Wiping Complete screen | OK |




[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)