
Seb's instructions:
Scenario 1 (restarting device starts mailbox on boot if mailbox was set up and started before reboot)

* Set up mailbox on a fresh device and pair it with briar.
* Restart device. Confirm that mailbox **does start** automatically after boot. Be patient, it can take 1-2 minutes for the mailbox to actually start.
* "App starts" in all cases here means that the notification with the mailbox logo does appear in the notification area, not that the app's activity gets launched.

Steps to execute:

Install mailbox and complete the set up - ie scan its QR code by the Briar app, and verify that the status screen shows 'milbox is running', as does the notification drawer. 

Then reboot the mailbox device.

Mailbox should start by itself - ie the notification drawer should show the mailbox is running. 
 
When the user taps the mailbox icon, the mailbox status screen should show 'mailbox is running'. 
At the same time, on Briar device, tapping Briar > settings > Mailbox brings up the same status screen, showing that the mailbox is running.

(During the reboot, if all goes well and reboot is done quickly enough, then there is no change in Briar - but if reboot lasts more than some threshold then Briar should show that the connectivity with mailbox is interrupted.  This is 'threshold' is still **TBD** (see MM discussion of June 01 in testing channel - separate test item exists for this.  )

for huawei device P40 Lite 5G EMUI version 12.0.à - the mailbox only restarts if the user goes to the device settings > apps > app launch and changes the 'manage autmatically' to **'manage manually'**.  In this case, the mailbox will restart by itself after device reboot.

**14/09/22 there are open questions about the powermanagement screen - when it should show and also whether to guide the user to the settings they need to change? TBD**

Retested 230.11.2022 - related tickets https://code.briarproject.org/briar/briar-mailbox/-/issues/124 and 
https://code.briarproject.org/briar/briar-mailbox/-/issues/163

The autostart does work, only the notifications are a problem, and in case of Huawei - it is the button Open Battery settings that does not work - see details in the ticket https://code.briarproject.org/briar/briar-mailbox/-/issues/124

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)

