

**Scenario 2 - Two devices can reconnect via bluetooth when BT setting on Briar app is OFF

Devices used: Samsung Mini API19 and Nokia 3.1 API29 (Android 10)

#### Steps to execute:

**Device1**

* Add contact at a distance
* Go to settings and switch the internet and wifi off in Briar app, Bluetooth setting is OFF
* The contact is shown offline
* tap on the contact and go to menu (upper right corner) and select Connect via bluetooth.
* Follow onscreen instructions

**Device2**

* Add contact at a distance
* Go to settings and switch the internet and wifi off in Briar app, Bluetooth settng is OFF
* The contact is shown offline
* tap on the contact and go to menu (upper right corner) and select Connect via bluetooth.
* Follow onscreen instructions

#### Expected results:

* Messages shown to both users are: 'Connecting via bluetooth', 'Connected via bluetooth' in case of successful connection
* Contacts are shown as online
* Contacts can send messages to each other
* Bluetooth setting in Briar app has been changed to ON during this process

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)