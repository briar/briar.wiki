In the circumstances of 
- the mailbox app being offline or 
- its rebooting taking longer time than the threshold (which is still **TBD**), 
- mailbox has been stopped by the user
- mailbox unlinked from its briar app
- mailbox deleted without unlinking


the Briar app is not going to be able to reach the mailbox. 

What should the user then see in Briar app? 

Should this 'message' be pushed to the user?  Or should it only be visible when the user Checks the Mailbox status from the Briar app status screen?

IN these cases the 'last connection' time should not be updated ...

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)

