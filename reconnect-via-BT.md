test contact -> connecting via bluetooth

* android 12+ should ask for nearby devices permission, but not location permission
* old android should ask for location permission
* when location permission is denied, a dialog should offer the user to go to app system settings

- If Briar has neaby devices permissions, reconnect via BT works OK and the user is able to send message to their contact via BT
- If briar has no permission for nearby devices, and the user wants to reconnect to their contact via BT, 
- After pressing the Start button, User is asked (via a dialog box) to Allow Briar access to nearby devices. 
- User can tap Allow and the process goes on OK



- If the user taps don't allow, there is a Nearby Devices Permission message to the user: "you have denied access to nearby devices... please consider granting access.
- User taps OK, and is taken to the device settings for Briar where they can Allow Briar access to nearby devices
- user returns to Briar and the process continues OK

on Android 7 instead of asking for nearby devices, Briar asks for access to location.  The behaviour is the same as above when the location is granted, or when it is not granted at first.
