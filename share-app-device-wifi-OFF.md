The objective of the test is to verify that Briar app handles the situation where the wifi on device is OFF - gracefully.  On different devices this means different things, depending on the Adroid features available for different API levels.  
On some devices, when Briar opens the hotspot, the wifi setting on device will be automatically restarted, and on others, the user will be asked to go to device settings and manually change wifi setting... 
Either way, the Briar app has to handle it graceully - with appropriate messages to the user and successful attempt at opening the hotspot once the wifi is switched ON.

#### Settings: 

**Device 1**
- Device settings > network and internet > Wifi = OFF
- Developer options - 'don't keep activities' = ON

**Device 2**
- Device settings > network and internet > Wifi = OFF
- Developer options - 'don't keep activities' = ON

#### Steps to execute

**Device 1**
- Go to Settings > Share Briar app offline
- Tap the 'Start Sharing' button
- Tap the 'Start app sharing button'

(note: text on these buttons is being redefined so they may have different names in the future, but their functions shoudl remain the same as now)

#### Expected results 

- The Briar app should do one of the two things: 
  - either it gives user a message to ask them to go to device settings and swich the wi-fi on, after which it continues the process of app sharing successfully 
  - or it restarts the wifi settings on the device automatically and continues the process of sharing successfully

This will depend on the device type and API level, Android version, and both behaviours are correct

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)
