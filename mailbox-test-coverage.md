| Mailbox ID | Title | Regression test | QA label | Test Scenario | Regression Test Result | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/1 | Manage app lifecycle | n/a | Epic  | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/2 | Mailbox pairing with Briar | n/a | Epic | ------ | ------ | ------ | ------|
| https://code.briarproject.org/briar/briar-mailbox/-/issues/3 | Publish hidden service | n/a | Epic | ------ | ------ | ------ | ------|
| https://code.briarproject.org/briar/briar-mailbox/-/issues/4 | Implement contact management API | n/a | Epic | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/5 | Implement file management API | n/a | Epic | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/6| Implement RSS API | n/a | Epic | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/7 | Status screen | n/a | Epic | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/8 | Receive RSS feeds shared by other apps | n/a | Epic  | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/9 | Register mailbox app signing key and package name with Google Play | n/a | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/10 | Create TLS certificates | n/a | Not Sponsor 6 | ------ | ------ | ------ | ------ |




| Mailbox ID | Title | Regression test | QA label | Test Scenario | Regression Test Result | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/11| Research constraints of using javalin as HTTP Server | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/12 | Create skeleton app for mailbox | n/a | n/a| ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/13 | Add Hilt | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/14 | Clarify (Decide?) License | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/15 | Decide about database library | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/16 | Research and decide database library | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/17 | First integration test| n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/18 | Upgrade dependencies | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/19 | Launcher icon not working on API 16 | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/20 | Upgrade to ktlint 0.42.0 | n/a | n/a | ------ | ------ | ------ | ------ |



| Mailbox ID | Title | Regression test | QA label | Test Scenario | Regression Test Result | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/21 | Convert to multi-module project | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/22 | Add lifecycle component | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/23 | Add Gradle witness (or something similar) | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/24 | Add TorPlugin to mailbox | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/25 | Activate ktlint on core and cli modules| n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/26 | Add integration test for the lifecycle component | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/27 |Clean up things copied over from briar we don't need in the end | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/28 | Implement LocationUtils | n/a | n/a | ------ | ------ | ------ | ------ |
| ------ | 29 seems to be missing | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/30| Basic database setup & migrations | n/a | n/a | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |

| Mailbox ID | Title | Regression test | QA label | Test Scenario | Regression Test Result | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/31 | Design UI for mailbox setup: pairing mailbox app with briar | n/a
 | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/32 | Implement UI for pairing mailbox with Briar | n/a | Tested | https://code.briarproject.org/briar/briar/-/wikis/first-install-do-not-kill-me-fragment     | n/a | ok | questions (not Sponsor 6) https://code.briarproject.org/briar/briar-mailbox/-/issues/119  |
|  https://code.briarproject.org/briar/briar-mailbox/-/issues/32| ------ | Tested | Tested | https://code.briarproject.org/briar/briar/-/wikis/user-allows-connections | OK| OK | ------ |
|  https://code.briarproject.org/briar/briar-mailbox/-/issues/32 | ------ | Tested | ------ | https://code.briarproject.org/briar/briar/-/wikis/manually-change-battery-optimisation-setting-to-off | OK | NOK | https://code.briarproject.org/briar/briar-mailbox/-/issues/122  low priority|
| https://code.briarproject.org/briar/briar-mailbox/-/issues/32| ------ | Tested | ------ | https://code.briarproject.org/briar/briar/-/wikis/manually-change-battery-optimisation-setting-to-ON | OK | OK | https://code.briarproject.org/briar/briar-mailbox/-/issues/120 |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/32| ------ | Tested| ------ | https://code.briarproject.org/briar/briar/-/wikis/app-in-background-while-do-not-kill-me-fragment-displayed | OK | OK | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/32| ------ | Tested | ------ | https://code.briarproject.org/briar/briar/-/wikis/user-leaves-setup | OK | OK | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/33 | Design mailbox status screen | n/a | n/a | will be tested in https://code.briarproject.org/briar/briar-mailbox/-/issues/41 | ------ | ------ | Duplicate |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/34 | Tor port | n/a | Tested | https://code.briarproject.org/briar/briar/-/wikis/tor-port | n/a | OK | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/35 | Use ktlintFormat? | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/36 | Store data in proper Linux data dir for cli app | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/37 | Create UI component for multi-step onboarding, or find a suitable library | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/38 | Power management setup UI | Tested | Tested | test scenarios as https://code.briarproject.org/briar/briar-mailbox/-/issues/32 | OK | OK | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/39 | Show onboarding on first launch to explain what mailbox is for | Tested | Tested | https://code.briarproject.org/briar/briar/-/wikis/onboarding-UI | OK | OK | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/40 | Store time of last connection from owner | Tested | Tested | https://code.briarproject.org/briar/briar/-/wikis/time-last-connected | OK | OK | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |

| Mailbox ID | Title | Regression test | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/41| Implement mailbox status screen | Tested  | Tested |  https://code.briarproject.org/briar/briar/-/wikis/mailbox-is-running | OK | OK | ------ |
| ------ | ------ | Tested | ------ | https://code.briarproject.org/briar/briar/-/wikis/scan-qr-code | Yes | OK | ------ |
| ------ | ------ | Tested | ------ | https://code.briarproject.org/briar/briar/-/wikis/interrupt-scanning-qr-code| OK | OK | ------ |
| ------ | ------ | Tested | ------ | https://code.briarproject.org/briar/briar/-/wikis/unlink-mailbox | OK | NOK | https://code.briarproject.org/briar/briar-mailbox/-/issues/155 https://code.briarproject.org/briar/briar/-/issues/2354 |
| ------ | ------ | Tested | ------ | https://code.briarproject.org/briar/briar/-/wikis/scan-incorrect-qr-code | OK | OK | ------ |
| ------ | ------ | Tested | ------ | https://code.briarproject.org/briar/briar/-/wikis/scan-valid-QR-code-more-than-once | OK | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/42 | Generate and store single-use auth token for pairing | n/a | n/a | tested implicitely by scenarios at https://code.briarproject.org/briar/briar-mailbox/-/issues/41  | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/43 | Store owner's and contacts' access rights to folders | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/44| Generate random IDs for folders and files | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/45 | Store contacts in mailbox DB | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/46 | Store private key of hidden service | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/47 | Hold a wake lock when the device is online to keep Tor running | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/48 | Encode QR code for pairing | n/a | n/a| Implicitely tested by https://code.briarproject.org/briar/briar-mailbox/-/issues/118 | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/49 | Lifecycle method to wipe mailbox | n/a | Tested | Tested by https://code.briarproject.org/briar/briar/-/wikis/unlink-mailbox at https://code.briarproject.org/briar/briar-mailbox/-/issues/41 | yes | see https://code.briarproject.org/briar/briar-mailbox/-/issues/41 | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/50 | API endpoint for pairing | n/a | Tested | https://code.briarproject.org/briar/briar/-/wikis/scan-valid-QR-code-more-than-once | yes | ok | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |

| Mailbox ID | Title | Regression Test | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/51 | API endpoint for listing files in specified folder | n/ad | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/52 | API endpoint for downloading a file | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/53 | API endpoint for deleting a file | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/54 | API endpoint for uploading a file to a folder | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/55 | API endpoint for listing readable folders with available files | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/56 | API endpoint for listing contacts | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/57 | API endpoint for adding a contact | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/58 | API endpoint for deleting a contact | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/59 | Design UI for wiping mailbox | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/60 | Implement UI for wiping mailbox | Tested | Tested | Implicitely tested by https://code.briarproject.org/briar/briar/-/wikis/unlink-mailbox | OK | see https://code.briarproject.org/briar/briar-mailbox/-/issues/41  | ------ |

| Mailbox ID | Title | Regression test | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/62 | API endpoint for wiping the mailbox | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/63 | Do not use DatabaseComponent abstraction | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/64 | Automate calculation of projected time spent for whole project based on time already spent and issue weights | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/65 | Make sure there are no vulnerabilities due to JSON deserialization | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/66 | Add API docs to ContactsManager | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/67 | Create read{} and write{} shortcut methods for database transactions | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/68 | Try to upgrade h2 | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/69 | Test everything is working on API 16 | ongoing | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/70 | API endpoint for checking mailbox status | Closed | n/a | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |

| Mailbox ID | Title | Regression test | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/71 | Check Java code style using CI | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/72 | Database has duplicate contacts in Integration tests | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/73 | Make sure to run `wipeMailbox()` wakefully during `onWipeRequest()` | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/74 | Auto-start mailbox app on boot | To Retest | Tested | https://code.briarproject.org/briar/briar/-/wikis/autostart-mailbox-on-reboot-when-mailbox-set-up/edit | NOK https://code.briarproject.org/briar/briar-mailbox/-/issues/163 | ------ | need Xiaomi and Huawei devices https://code.briarproject.org/briar/briar-mailbox/-/issues/124 |
| ------ | ------ | Tested| ------ | https://code.briarproject.org/briar/briar/-/wikis/autostart-mailbox-on-reboot-when-mailbox-set-up-not-completed-yet | ------ | ------ | ------ |
| ------ | ------ |Tested | ------ | https://code.briarproject.org/briar/briar/-/wikis/no-mailbox-autostart-on-reboot-when-QRcode-scan-canceled | OK | ------ | ------ |
| ------ | ------ | Tested | ------ | https://code.briarproject.org/briar/briar/-/wikis/no-mailbox-autostart-on-reboot-when-mailbox-stopped-by-user | OK | ------ | ------ |
| ------ | ------ | n/a | ------ | https://code.briarproject.org/briar/briar/-/wikis/reboot-briar-device-after-QR-code-scan | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/75 | Auto-start mailbox app on boot | n/a | n/a | ------ | ------ | ------ | Duplicate |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/76 | Wiping the mailbox can cause deadlock | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/77 | Lifecycle cleanup | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/78 | Fix checkstyle | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/79 | Status notification always showing same text | Tested | Tested | https://code.briarproject.org/briar/briar/-/wikis/mailbox-notifications | OK | OK | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/80 | Consider starting mailbox before phone has been unlocked by user | n/a | n/a | ------ | ------ | ------ | not Sponsor 6 |

| Mailbox ID | Title | Regression test | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/81 | Upgrade ch.qos.logback dependency | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/82 | User feedback / bug / crash-log reporting mechanism | n/a | n/a | ------ | ------ | ------ | Epic, not Sponsor 6 |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/83 | Add setting for opting in or out of Tor bridges | n/a | n/a | ------ | ------ | ------ |not Sponsor 6|
| https://code.briarproject.org/briar/briar-mailbox/-/issues/84 | Exception logging broken | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/85 | Allow wiping after pairing/linking has been aborted | Tested |Tested | https://code.briarproject.org/briar/briar/-/wikis/no-mailbox-autostart-on-reboot-when-QRcode-scan-canceled | OK | OK | From the comments in the ticket it seems that this is tested by Cancel Setup - seelink to test  |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/86 | Show a different notification when mailbox needs linking |  Tested | Tested | https://code.briarproject.org/briar/briar/-/wikis/mailbox-notifications | OK | OK | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/87 | Don't autostart mailbox when user had actively stopped it before | Tested | Tested | https://code.briarproject.org/briar/briar/-/wikis/no-mailbox-autostart-on-reboot-when-mailbox-stopped-by-user | OK | ok | ------ |
| ------ | ------ | Tested | Tested | https://code.briarproject.org/briar/briar/-/wikis/autostart-mailbox-on-reboot-when-mailbox-set-up-not-completed-yet| OK | ok | ------ |
| ------ | ------ | Tested | Tested | https://code.briarproject.org/briar/briar/-/wikis/no-mailbox-autostart-on-reboot-when-QRcode-scan-canceled | OK | ok | ------ |
| ------ | ------ | Tested | Tested | https://code.briarproject.org/briar/briar/-/wikis/no-mailbox-autostart-on-reboot-when-mailbox-stopped-by-user | OK | ok | ------ |
| ------ | ------ | n/a | ------ | implicitely tested by https://code.briarproject.org/briar/briar/-/wikis/mailbox-is-running | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/88 | Make it possible to enable trace logging on the CLI | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/89 | Adapt theme of DoNotKillFragment | Tested | To test | To do | OK | OK | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/90 | Wait for hidden service to be reachable before showing QR code | n/a | n/a | ------ | ------ | ------ | ------ |

| Mailbox ID | Title | Regression test | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/91 | Mailbox stores Tor state in different directory from app state | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/92 | Dialog scrim color on dark theme | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/93 | NetworkOnMainThreadException in MailboxService#onDestroy() | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/94 | Add illustrations to onboarding screens | Tested | To test | test????? | OK | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/95 | Implement network error screen during onboarding | Tested | To test | https://code.briarproject.org/briar/briar/-/wikis/device-offline-warning | OK | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/96 | Replace hardcoded strings in layouts | n/a | To test | https://code.briarproject.org/briar/briar/-/wikis/mailbox-strings-translations | see discussion with Seb in MM | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/97 | Design UI - fix bugs found during implementation | n/a | n/a | ------ | ------ | ------ | Rejected |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/98 | Map lifecycle / tor plugin states to translatable and useful strings shown in StartupFragment | n/a | n/a | ------ | ------ | ------ | These are strings shown during the progress screen (publishing onion service etc) |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/99 | Improve successful linking screen | Tested | Tested | https://code.briarproject.org/briar/briar/-/wikis/mailbox-success-screen | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/100 | Refactor TorPlugin into a library | n/a | n/a | ------ | ------ | ------ | ------ |

| Mailbox ID | Title | Status | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/101 | Get rid of remaining java.util.logging and replace with slf4j | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/102| Delete stale files | n/a| n/a | ------ | ------ | ------ | handled by automatic tests MM 30/09/2022 |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/103 | API endpoint for getting the API versions supported by the mailbox | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/104 | Include supported API versions in pairing response | n/a | n/a | ------ | ------ | ------ | covered by automated tests, MM 30092022 |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/115 | Fetch and store mailbox's supported API versions when pairing mailbox | n/a | n/a | ------ | ------ | ------ | Duplicate |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/105 | Limit the size of uploaded files | n/a | n/a | ------ | see chat with Seb in MM 18052022 | ------ | "------ "I'd say that one is one of those that don't need hands-on testing. We've got unit tests for that which work automatically."|
| https://code.briarproject.org/briar/briar-mailbox/-/issues/106 | Tor complains about missing files when wiping via API | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/107 | Crash when creating database on Android 4 | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/108 | Crash when creating QR code on Android 4 | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/109 | Crash when showing pairing success screen on Android 4 | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/110 | Show more progress information during startup | n/a | n/a | ------ | ------ | ------ | ------ |

| Mailbox ID | Title | Regression test | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/111 | No progress indicator during startup on Android 5 | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/112 | Success indicator is a blank green circle on Android 5| n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/113 | Clear UI when mailbox is wiped remotely | Tested | Tested| https://code.briarproject.org/briar/briar/-/wikis/mailbox-wiping-complete-screen | OK | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/114 | Mailbox hangs at "Starting services" when relaunching after remote wipe | Tested | Tested | https://code.briarproject.org/briar/briar/-/wikis/mailbox-wiping-complete-screen | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/116 | Usability testing for Mailbox app | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/117 | Status screen should warn when device is offline | To test | Tested | https://code.briarproject.org/briar/briar/-/wikis/mailbox-no-internet | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/118 | Tapping notification creates duplicate instance of status screen | Tested | To test |  | OK | ------ | Bug |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/119 | Navigating back after the 4 intro scrreens or from the 'do not kill me' | n/a | n/a | ------ | ------ | ------ | not Sponsor 6 |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/120 | Battery optimisation setting - should the user be informed if it is reset (by device itself or user)? | n/a | n/a | ------ | ------ | ------ | Not Sponsor 6 |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |

| Mailbox ID | Title | Status | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/121 | Make additional check for connectivity before starting services | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/122 | Skip the battery settings screen if no settings need to be changed | To test | To test | https://code.briarproject.org/briar/briar/-/wikis/manually-change-battery-optimisation-setting-to-off| ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/123 | Display different notification texts and icon in error state (network problem) | n/a | To test | https://code.briarproject.org/briar/briar/-/wikis/mailbox-notifications | ------ | ------ | Rejected |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/124 | Handle Huawei and Xiaomi auto-start restrictions | To test | To test | test????? | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/125 | Allow contacts to call status endpoint | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/126 | Potentially display special screen on status screen when last connection from owner was long ago | n/a | n/a | ------ | ------ | ------ | Rejected |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/127 | Use better mechanism than counting number of descriptor uploads to determine published state of Tor | n/a | n/a | ------ | ------ | ------ | Extensively tested on a branch|
| https://code.briarproject.org/briar/briar-mailbox/-/issues/128 | Cancelling setup is not clearing "Mailbox starting" notification | To test | To test | https://code.briarproject.org/briar/briar/-/wikis/cancel-setup | Yes | OK | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/129 | Accessibility optimizations | n/a | n/a | ------ | ------ | ------ | post MVP |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/130 | Use flexible layouts for small screens | Open | To test | To do | ------ | ------ | TO test but also to keep open pending usability tests |

| Mailbox ID | Title | Regression test | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/131 | Linking success screen and wiping complete screen don't look right in landscape| To test | To test  | https://code.briarproject.org/briar/briar/-/wikis/mailbox-success-screen https://code.briarproject.org/briar/briar/-/wikis/mailbox-wiping-complete-screen | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/132 | Check that system clock is reasonable when starting | TO test | To test | https://code.briarproject.org/briar/briar/-/wikis/mailbox-system-clock-in-the-future https://code.briarproject.org/briar/briar/-/wikis/mailbox-system-clock-in-the-past| ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/133 | Initialise last connection time during pairing | To test | To test | https://code.briarproject.org/briar/briar/-/wikis/time-last-connected | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/134 | Rotating devices during startup causes crash | To test | To test | https://code.briarproject.org/briar/briar/-/wikis/user-allows-connections-and-rotates-screen | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/135 | After linking briar and mailbox, mailbox showing 'never' as the last connetion | n/a| n/a | ------ | ------ | ------ | Duplicate |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/136 | Adapt Xiaomi power setup for MIUI 12.5 | To test | To test | ------ | ------ | ------ | Michael|
| https://code.briarproject.org/briar/briar-mailbox/-/issues/137 | Upgrade to Tor 0.4.5.12 | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/138 | Show a warning dialog if Tor detects clock skew | To test | To test | https://code.briarproject.org/briar/briar/-/wikis/mailbox-system-clock-in-the-future https://code.briarproject.org/briar/briar/-/wikis/mailbox-system-clock-in-the-past | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/139 | Cannot find geoip.zip | n/a | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/140 | When mailbox Offline screen is displayed notification is not correct | To test | To test | https://code.briarproject.org/briar/briar/-/wikis/device-offline-warning | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |


| Mailbox ID | Title | Regression | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/141 | Move fragment navigation to activity | Open | To test | ------ | ------ | ------ | miss info |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/142 | With Do-not-keep-activities enabled, after finishing onboarding, the "Wiping completed" activity gets shown | Open | To test | https://code.briarproject.org/briar/briar/-/wikis/restart-mailbox-after-wiping-do-not-keep-activities | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/143 | Upgrade Ktor library to version 2.x | Open | n/a | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/144 | MainActivity should reset everything to an initial state when recreated with a fresh lifecycle | Open | To test | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/145 | Briar Mailbox was unable to run in the background: crash when clicking "fix" | Open | To test | https://code.briarproject.org/briar/briar/-/wikis/reopen-mailbox-after-device-goes-to-sleep https://code.briarproject.org/briar/briar/-/wikis/manually-change-battery-optimisation-setting-to-ON | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/146 | Top of the battery icon cut off in landscape orientaiton - do-not-kill-me fragment | Open | To test | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/147 | Research accessibility testing | n/a | n/a | ------ | ------ | ------ | post MVP |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/148 | UI for sending feedback and crash reports | n/a | n/a | ------ | ------ | ------ | not Sponsor 6 |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/149 | Send feedback and crash reports via Tor| n/a | n/a | ------ | ------ | ------ | not Sponsor 6 |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/150 | Rotating device before onboarding becomes visible makes init screen reappear and user is stuck there | To test | ------ | ------ | ------ | ------ | bug|


| Mailbox ID | Title | Regression Test | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/151 |Success screen doesn't show after linking with Briar | To test | ------ | ------ | ------ | ------ | bug |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/152 | Mailbox Intro screens in Landscape not readable Samsung 6810 | To test | ------ | ------ | ------ | ------ | Bug |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/153 | At first intro screen, if mailbox app pushed into the background then foreground - it skips the rest of the intro screens | To test | ------ | ------ | ------ | ------ | bug |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/155 | Remote wiping when Mailbox screen asleep => Wiping Complete screen presents twice | To test | ------ | ------ | ------ | ------ | bug |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/156 |WipeComplete screen shows same message for both local and remote wiping | To test | ------ | ------ | ------ | ------ | I think this was solved, but to test and then close if OK, check with Seb |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/157 | Tapping the Unlink (briar side) and Stop (mailbox side) at the same time - mailbox stuck in StoppingMailbox | To test | ------ | ------ | ------ | ------ | bug |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/158 | CLI does not respond to Ctrl+C any longer | ???? | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/159 | Stuck on init fragment when navigating away quickly during start | To test | ------ | ------ | ------ | ------ | bug |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/160 | Wiping complete screen - button hides text in landscape orientation| ------ | ------ | ------ | ------ | ------ | ------ |

| Mailbox ID | Title | Regression Test | QA label | Test Scenario | Test Executed | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/161 | About screen | n/a | ------ | ------ | ------ | ------ | not Sponsor 6 |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/162 | Unable to start mailbox: SavedStateHandle issue | n/a | ------ | ------ | ------ | ------ | will be implicitely tested by many tests |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/163 | Notification does not update when app is in background | To test | ------ | ------ | ------ | ------ | bug |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/164 | AS cannot resolve version variables in build.gradle | n/a | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/165 | TestMailbox is using real TorPlugin that bootstraps in each test | n/a | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/166 | Investigate if in-progress uploads will be listed and are available for download| n/a | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/167 | Continuation of illustration revamp | ???? | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/168 | Dead code in StatusFragment | To test | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/169 | Add Briar Mailbox to our F-Droid repo | n/a | ------ | ------ | ------ | ------ | ------ |
|https://code.briarproject.org/briar/briar-mailbox/-/issues/170 | Add Briar Mailbox to Google Play | n/a | ------ | ------ | ------ | ------ | ------ |

| Mailbox ID | Title | Regression Test | QA label | Test Scenario | Regression Test Result | Test Result | Miss info |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/171 | Reproducible builds | n/a | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/172 | Update target SDK to 33 | n/a | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/173 | Enable proguard minification | n/a | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/issues/174 | Upgrade logback and slf4j after logback-android released version 3.0 | n/a | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| https://code.briarproject.org/briar/briar-mailbox/-/merge_requests/123/diffs?commit_id=75cc880a16f997cf896a7270b3579abf48b8b5a2 | ------ | ------ | ------ | ------ | ------ | ------ | ------ |


[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)

