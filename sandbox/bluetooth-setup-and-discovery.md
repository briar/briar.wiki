# Use Cases

## Assumptions about users

Fair?

- they know that Briar can only deliver a message when the other contact
  appears as online

- they have some vague concept of Bluetooth, and that BT is one way of
  sending a message to a contact

## Case: user A wants to send something to nearby contact B

We don't have any Internet-connection or even Wifi, so we'll use this BT.

- I open the conversation with contact B,
  - to send a message
  - make an introducton
  - to see that the group invitation/forum sharing/etc that I made
    has not been sent off (little clock icon visible)

- We both need to have BT turned on! I turn BT on, and tell B to do the same.

- I don't see B as online!
  - Alternative: B already appears as online, so I can send something. DONE.

- I tap the "Connect via Bluetooth" button in the conversation
  (or understandable icon)

- Screen comes up:
  ```
  Make all contacts nearby go to this screen now and press start roughly at the
  same time

  [Start]
  ```
  - Alternative: B now appears as online, and a toast highlights this fact to me

- TODO: I tell B "how to go to this screen"

- I tap `Start` and tell B to do the same

- Screen: indicates that something is in progress

- Screen: shows that contact B has been discovered, `OK` button
  - Alternative: screen also shows that my contact C has been discovered

- I tap `OK`

- I see that B appears as online. DONE.
