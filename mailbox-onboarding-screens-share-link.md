## Scenario 1

**Device 1**

- In Briar app, go to settings, tap the Mailbox option
- On the first Mailbox setup screen, tap Continue button
- Verify that the next Mailbox Setup screen opens
- tap back button
- Previous screen opens
- Tap continue again 
- Second Mailbox screen opens
- tap Share Download link
- a modal window opens that allows the user to copy links
- and to open a messaging platform they want to use to send the links
- Rotate the screen
- tap back button
- User is returned to Mailbox setup Screen 2 in landscape
- tap the Share Download link again and rotate the screen back to portrait
- copy the links given
- paste them into a message to one of the contacts in Briar app

**Device 2**
- Open the link received
- Verify that Mailbox can be downloaded from there

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)