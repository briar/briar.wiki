Go to briar > settings > mailbox and open the mailbox status screen (or perform linking previously if needed)

- tap on Unlink, and when the dialog box comes up that asks the user to confirm, 
- rotate the device, 
- naigate back from there, 
- double tap unlink button and then rotate, 
- switch the internet off immediately after tapping the Unlink button

- all of these situations should be handled gracefully on both briar side and mailbox side

- unlinking from briar side triggers mailbox wiping on the mailbox device - and that can be interrupted too in the same way as above described interruptions.  
- The notifications on the mailbox device should correspond to what the user sees on screen

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)