### Add nearby contact via bluetooth, then go to 'Connect via bluetooth' for that contact

If users see each other online  and they perform 'connect via bluetooth', then they just get a message: successfully connected via bluetooth.  

If they are offline to start with, and then perform 'connect via bluetooth' they are given onscreen instructions (saying that both people should tap start at the same time)

If one user is offlineand the other online - they should both get approparite onscreen messages:  'successfully connected via bluetooth', and the offline user should be able to reconnect with their contact, both contacts should see each other as online and send each other messages.

Expected results:
Contacts should see each other online and be able to send each other messages.

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)