### Objectives

* Allow users to add contacts remotely, as well as by meeting in person
* Support features that are common in other messaging apps: image attachments, a PIN lock, and a dark UI theme
* Make it easier for organizations such as OCCRP to integrate Briar blogs and messages into their publishing workflows
* Reduce power consumption
* Support the use of Tor bridges in countries such as China where access to Tor is blocked
* Establish a reproducible build process so users can be sure the app they are using was built from the published source code

### Deliverables

1. Adding contacts remotely (#1230)
    * Protocol and UX for adding contacts remotely via Tor, extensible to other transports
    * User testing
    * Update user manual

2. Image attachments (#1237)
    * Update sync protocol to support syncing large messages
    * Update database and sync API to support storing and retrieving large messages
    * Update all existing sync clients to use new database and sync API
    * UX for sending and displaying image attachments
    * User testing
    * Update user manual

3. Account sign in improvements (#1245)
    * Remind user to sign in after device is restarted
    * Optional PIN lock feature, enabling app to be locked while remaining signed in
    * User testing
    * Update user manual

4. Dark theme (#976)
    * Dark UI theme, easier to use in low light
    * User testing
    * Update user manual

5. Headless server/desktop app (#1254)
    * Enable OCCRP to publish Briar messages and blogs from their existing tools
    * Refactor Tor plugin to remove Android dependencies
    * Implement background service
    * Implement basic CLI for adding contacts, sending private messages and blog posts
    * User testing

6. Power management improvements (#1260)
    * Detect and mitigate power management features affecting background services
    * UX for adding Briar to whitelists where required to maintain background connectivity
    * Option to disable background connectivity when running on battery power
    * User testing
    * Update user manual

7. Research how to reduce battery impact of Tor hidden services (#1263)
    * Research and document Tor’s behaviour in all Android power management states
    * Identify changes to allow device to spend more time asleep without impacting hidden service availability
    * Work with Tor developers to ensure proposed changes do not impact anonymity

8. Tor bridges (#647)
    * Support for Tor bridges based on Guardian project’s work
    * Use OONI data to create list of locations where bridges should be used
    * Include fresh bridge and location lists in each release
    * Automatically enable bridges when current location requires them
    * UX to enable bridges manually if needed
    * User testing
    * Update user manual

9. Reproducible builds (#1272)
    * Entire app, including Tor binaries, to be built reproducibly from source
    * Document build process

### Personnel

The team will consist of six people: the project lead, two developers, a user experience designer, a usability expert, and a technical writer.

1. Adding contacts remotely
    * Project lead: 25 days
    * Developer: 20 days
    * Designer: 5 days
    * Usability: 2 days
    * Writer: 1 day
    * Total: 53 days

2. Image attachments
    * Project lead: 50 days
    * Developer: 65 days
    * Designer: 5 days
    * Usability: 2 days
    * Writer: 1 day
    * Total: 123 days

3. Account sign-in improvements
    * Project lead: 5 days
    * Developer: 6 days
    * Designer: 6 days
    * Usability: 2 days
    * Writer: 1 day
    * Total: 20 days

4. Dark theme
    * Developer: 5 days
    * Designer: 5 days
    * Usability: 1 day
    * Writer: 1 day
    * Total: 12 days

5. Headless desktop/server app
    * Developer: 20 days
    * Usability: 2 days
    * Total: 22 days

6. Power management improvements
    * Project lead: 15 days
    * Developer: 20 days
    * Designer: 6 days
    * Usability: 2 days
    * Writer: 1 day
    * Total: 44 days

7. Research how to reduce battery impact of Tor hidden services
    * Project lead: 30 days
    * Developer: 30 days
    * Total: 60 days

8. Tor bridges
    * Developer: 25 days
    * Designer: 5 days
    * Usability: 2 days
    * Writer: 2 days
    * Total: 34 days

9. Reproducible builds
    * Developer: 20 days
    * Total: 20 days

### Tickets

[Tickets for Sponsor 1](https://code.briarproject.org/briar/briar/issues?label_name[]=Sponsor+1)