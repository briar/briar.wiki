#### Steps to execute:

**Device 1**
- go to briar settings > share app offline
- Start sharing
- screen displays the details of wifi the device 2 needs to connect to

**Device 2**
- Device settings > internet and network > wifi + add wifi network
- type in the wifi SID or scan the QR code to connect to wifi/hotspot opened by device 1

**Device 1**
- tap 'start app sharing' button
- IP address that the device 2 needs to type into their browser is shown on screen

**Device 2**
- open the browser and type in the IP address given by device 1 (or scan the QR code)
- verify that the website opens, with a big green Download Briar button
- Tap Download Briar button
- Verify that download starts

**Device 1**
- Stop sharing before the download on device 2 is complete 



#### Expected results:

- Device 2 Browser loses connection to the wifi network of device 1
- Device 1 can open a new hotspot, and device 2 has to connect to this new hotspot 
- Device 2 has to type in the IP address into its browser again and start the download again... 
- Device 2 download is successful, briar can be successfully installed

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)

