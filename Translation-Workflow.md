Translating Briar is very easy. Just [sign up for an account](https://www.transifex.com/signup) at Transifex, if you have not done so already. After you are logged in, go to the [Briar translation project](https://www.transifex.com/otf/briar/). There you can click the "Join Team" button and join the respective language team.

What you translate there will then be included in the next release of Briar. 

Please also see the [wiki page of Localization Lab about Briar](https://wiki.localizationlab.org/index.php/Briar).