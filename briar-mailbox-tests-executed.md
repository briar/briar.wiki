These are test that are executed to retest the Proguard Minification ticket [173](https://code.briarproject.org/briar/briar-mailbox/-/issues/173)



## Mailbox installed on device 
**Xiaomi, Android 11, linked to Briar debug on Huawei Android 10**

- [x] [4 onboarding screens](https://code.briarproject.org/briar/briar/-/wikis/onboarding-UI)
- [x] [User "Allow connections ](https://code.briarproject.org/briar/briar/-/wikis/user-allows-connections)
- [x] [Scan QR code](https://code.briarproject.org/briar/briar/-/wikis/scan-qr-code)
- [x] [Mailbox Success screen](https://code.briarproject.org/briar/briar/-/wikis/mailbox-success-screen)
- [x] [Mailbox notifications](https://code.briarproject.org/briar/briar/-/wikis/mailbox-notifications)
- [x] [unlink mailbox](https://code.briarproject.org/briar/briar/-/wikis/unlink-mailbox)
- [x] [Mailbox is running](https://code.briarproject.org/briar/briar/-/wikis/mailbox-is-running)
- [x] [Time last connected](https://code.briarproject.org/briar/briar/-/wikis/time-last-connected)
- [x] [Stop mailbox](https://code.briarproject.org/briar/briar/-/wikis/stop-mailbox)
- [x] [Mailbox not reachable by Briar](https://code.briarproject.org/briar/briar/-/wikis/mailbox-not-reachable)
- [x] [Mailbox has no access to internet](https://code.briarproject.org/briar/briar/-/wikis/mailbox-no-internet )


**SAMSUNG A01s Core, Android 10, linked to briar debug HTC E One Android 5 **

- [x] [4 onboarding screens](https://code.briarproject.org/briar/briar/-/wikis/onboarding-UI)
- [x] [User "Allow connections ](https://code.briarproject.org/briar/briar/-/wikis/user-allows-connections)
- [x] [Scan QR code](https://code.briarproject.org/briar/briar/-/wikis/scan-qr-code)
- [x] [Mailbox Success screen](https://code.briarproject.org/briar/briar/-/wikis/mailbox-success-screen)
- [x] [Mailbox notifications](https://code.briarproject.org/briar/briar/-/wikis/mailbox-notifications)
- [x] [unlink mailbox](https://code.briarproject.org/briar/briar/-/wikis/unlink-mailbox)
- [x] [Mailbox is running](https://code.briarproject.org/briar/briar/-/wikis/mailbox-is-running)
- [x] [Time last connected](https://code.briarproject.org/briar/briar/-/wikis/time-last-connected)
- [x] [Stop mailbox](https://code.briarproject.org/briar/briar/-/wikis/stop-mailbox)
- [x] [Mailbox not reachable by Briar](https://code.briarproject.org/briar/briar/-/wikis/mailbox-not-reachable)
- [x] [Mailbox has no access to internet](https://code.briarproject.org/briar/briar/-/wikis/mailbox-no-internet )

**Note6 ulefone Anroid 12, linked to briar debug on Android 7**
- [x] [4 onboarding screens](https://code.briarproject.org/briar/briar/-/wikis/onboarding-UI)
- [x] [User "Allow connections ](https://code.briarproject.org/briar/briar/-/wikis/user-allows-connections)
- [x] [Scan QR code](https://code.briarproject.org/briar/briar/-/wikis/scan-qr-code)
- [x] [Mailbox Success screen](https://code.briarproject.org/briar/briar/-/wikis/mailbox-success-screen)
- [x] [Mailbox notifications](https://code.briarproject.org/briar/briar/-/wikis/mailbox-notifications)
- [x]  [unlink mailbox](https://code.briarproject.org/briar/briar/-/wikis/unlink-mailbox)
- [x] [Mailbox is running](https://code.briarproject.org/briar/briar/-/wikis/mailbox-is-running)
- [x] [Time last connected](https://code.briarproject.org/briar/briar/-/wikis/time-last-connected)
- [x] [Stop mailbox](https://code.briarproject.org/briar/briar/-/wikis/stop-mailbox)
- [x] [Mailbox not reachable by Briar](https://code.briarproject.org/briar/briar/-/wikis/mailbox-not-reachable)
- [x] [Mailbox has no access to internet](https://code.briarproject.org/briar/briar/-/wikis/mailbox-no-internet )




