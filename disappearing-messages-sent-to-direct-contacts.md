#1893 (closed) 
To send self-destructing messages to a contact, a user has to switch the Disappearing Messages setting ON for each contact individually.
- [ ] user selects a contact from their contact list, and goes to the upper right corner on the screen and taps the three little dots -> which opens a menu of options.
- [ ] select the option Disappearing Messages and set it to ON
- Send a message to contact

Expected results:
Sender's device
- [ ] When sender starts typing the message - they get a little exploding bomb icon in the test input field - to remind them that they are typing a self-destructing message
- [ ] The sender receives and automatically generated notice message saying: Your messages will disappear after 7 days.  Tap to learn more.  
- [ ] Tapping on 'tap to learn more' opens the Disappearing messages setting screen, which has a link Learn more
- [ ] Tapping on Learn more opens the information screen which exaplins to the user how this setting functions. 
- [ ] Information screen has one tappable option: Got it
- [ ] Tapping on Got it, closes the information screen and returns the user back to the Disappearing messages setting screen.  
- [ ] From there, user can navigate back to messages screen.
- [ ] ON messages screen, the message that the user has just composed has a clock and a bomb icon.  Once that message is delivered, the two ticks replace the clock. 
**NOTE: clock means the message is composed but not sent, one tick means the message is sent and two ticks mean the message has been delivered.  The one tick stage is seen only briefly, and two ticks sho almost immediately, unless the message is sent by removable media (and mailbox in the future). IN case of message being exported onto removable media, one tick is seen until such time as the recipient imports the message.**

- [ ] The self-destructing message that the user just sent disappears after the time expires.



Recipient's device
- [ ] There is a new message showing on the sender's avatar in the contact list
- [ ] Tapping on the name of the contact who sent the self-dest message, open the screen that contains the following: 
- [ ] in messages that have arrived, there the small bomb icon
- [ ] there is an automatically generated notice: '[username]'s messages will disappear after 7 days
- [ ] This atuomatically generated message appears after a change of Disappearing messages setting, and not before every message.  If several messages are sent with the ssame Disappearing Messages setting, then there is only one automaitcally generated notice.  
- [ ] The Disappearing Messages setting on the recipients's device, mirrors the setting on the sender's device (if this was changed since the last exchange of messages between them - there is a separate test for that)
- [ ] When the recipient wants to reply to the self-destructing message they received, and they start typing, the bomb icon appears in their text input field.
- [ ] Both the received message and the sent message disappear after their respective timers expire.
- [ ] Messages do not disappear before they are read.  The timer start after the messages have been read.

Scenario2: If the sender then goes and changes the Disappearing Setting on their device to OFF, and sends another message to the same contact, 

Expected results:
Senders device
- [ ] A normal text message is sent to the contact, without any automatically generated notice messages, and without the little bomb icon in the text input field.
- [ ] The message does not disappear.

Recipient's device
- [ ] A normal text message is received, and when the user replies to it there is no bomb icon in the text input field
- [ ] recipient's Disappearing Messages setting mirrors the sender's and in this case it goes OFF
- [ ] Messages do not disappear
- [ ] If the recipient tries to reply, there aren't bomb icons in the text input field. 

Scenario3: 

- [ ] Sender sets the Disappearing Messages setting to ON on their device and sends a disappearing message to their contact. 
- [ ] Sender then sends another disappearing message before the timer for the first one expires. 
Expected result: 
- [ ] Each message disappears after it has been read and their timer expires.

Scenarios4:
- [ ] Sender sends several messages, some disappearing some not, to the same contact. 
- [ ] After each message the sender changes the Disappearing Messages setting.

Expected results: 
- [ ] Each time the setting is changed, the Disappearing Messages setting on recipient's device mirrors sender's and messages behave accordingly.  

Scenarios5:
- [ ] Sender sends several messages, some disappearing some not, to the same contact. 
- [ ] After each message the sender changes the Disappearing Messages setting.
- [ ] Recipient replies to each message, and every other time they change the  Disappearing Messages setting on their device before replying. 

Expected results: 
- [ ] Messages behave according to the Disappearing Messages setting of the device that last changed that setting.

Scenarios6:
- [ ] Sender sends several messages to several contacts, some with disappearing messages on, some off.  
- [ ] Recipients reply to all messages, and change their own Disappearing Messages setting every other time. 

Expected results: 
- [ ] Messages behave according to the Disappearing Messages setting of the device that last changed that setting.





[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/)











