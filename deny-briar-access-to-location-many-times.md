### Fresh Install - Deny Briar access to the location - twice in a row (android 11)

**Device Settings:** 
Device 1 location services = Off -> Off (deny the app permission) -> 'deny' the whole device use of location - > allow device use of location -> allow Briar access to location
**Briar settings:** 
Device 1 - connect via Bluetooth = On 
Device 2 - connect via Bluetooth = On

Expected results: 

Contacts should not be created.

This has to do with android popups changing between Android 10 and Android 11. The options on them are different - but Briar messaging remains the same ... To re-test and describe here - see tickets #2000 and #2002?

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)