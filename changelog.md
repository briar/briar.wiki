### Changelog

Dates and release notes for all production releases of the Briar Android app. For the Desktop program see [its respective changelog](https://code.briarproject.org/briar/briar-desktop/-/wikis/Changelog).

### [1.5.14](https://code.briarproject.org/briar/briar/commits/release-1.5.14)

2025-03-05

* Update translations, add Bengali
* Update list of Tor bridges
* Upgrade Tor to 0.4.8.14
* Replace obfs4proxy and snowflake with lyrebird

### [1.5.13](https://code.briarproject.org/briar/briar/commits/release-1.5.13)

2024-08-31

* Update translations
* Fix button colour in mailbox status screen
* Add themed icon

### [1.5.12](https://code.briarproject.org/briar/briar/commits/release-1.5.12)

2024-08-07

* Update translations, add Spanish (Cuba)
* Update the list of Tor bridges
* Redesign account setup and login screens
* Upgrade to Material Design 3
* Follow system's light or dark theme by default

### [1.5.11](https://code.briarproject.org/briar/briar/commits/release-1.5.11)

2024-05-10

* Update translations, add Portuguese (Portugal)
* Fix a crash when sharing the app offline

### [1.5.10](https://code.briarproject.org/briar/briar/commits/release-1.5.10)

2024-03-24

* Update translations
* Update list of Tor bridges
* Upgrade snowflake to 2.9.1
* Fix Tor connectivity via snowflake

### [1.5.9](https://code.briarproject.org/briar/briar/commits/release-1.5.9)

2024-02-16

* Update translations
* Update list of Tor bridges
* Upgrade Tor to 0.4.8.9
* Increase length of password for Wi-Fi hotspot when sharing app offline
* Use new Android 12 tapjacking protections

### [1.5.8](https://code.briarproject.org/briar/briar/commits/release-1.5.8)

2023-10-23

* Update translations
* Update list of Tor bridges
* Upgrade Tor to 0.4.7.15
* Fix connectivity issue with Tor bridges
* Don't ask for Bluetooth permission when enabling Tor/Wi-Fi connections

### [1.5.7](https://code.briarproject.org/briar/briar/commits/release-1.5.7)

2023-09-27

* Update translations
* Fix a crash when getting the Bluetooth address
* Fix a crash at startup
* Fix a crash when sharing a briar:// link

### [1.5.6](https://code.briarproject.org/briar/briar/commits/release-1.5.6)

2023-08-28

* Remove support for Android TV because Google no longer allows apps that support Android TV to be published as APKs

### [1.5.5 (beta release)](https://code.briarproject.org/briar/briar/commits/beta-1.5.5)

2023-08-22

* Update translations
* Update list of Tor bridges
* Upgrade Tor to 0.4.7.14
* Target Android 13

### [1.5.4](https://code.briarproject.org/briar/briar/commits/release-1.5.4)

2023-06-08

* Fix a crash when linking a Mailbox

### [1.5.3](https://code.briarproject.org/briar/briar/commits/release-1.5.3)

2023-05-24

* Use new handshake protocol when adding contacts at a distance

### [1.5.2 (beta release)](https://code.briarproject.org/briar/briar/commits/beta-1.5.2)

2023-05-18

* Update translations
* Update list of Tor bridges
* Drop support for Android 4

### [1.5.1](https://code.briarproject.org/briar/briar/commits/release-1.5.1)

2023-05-15

* Update translations
* Add support for communicating via Briar Mailbox
* Use "no personalized learning" flag for all keyboard input
* Fix error message when adding a contact that already exists
* Fix a crash when requesting Bluetooth discoverability

### [1.4.23](https://code.briarproject.org/briar/briar/commits/release-1.4.23)

2023-03-09

* Update translations
* Update list of Tor bridges
* Upgrade obfs4proxy to 0.0.14-tor2
* Upgrade Snowflake to 2.5.1
* Upgrade Tor's OpenSSL to 1.1.1t
* Fix a crash when connecting via Bluetooth

### [1.4.22](https://code.briarproject.org/briar/briar/commits/release-1.4.22)

2023-02-23

* Update translations
* Add support for importing RSS feeds from XML files, for offline news distribution

### [1.4.20](https://code.briarproject.org/briar/briar/commits/release-1.4.20)

2023-01-30

* Update translations
* Update list of Tor bridges
* Upgrade Tor to 0.4.7.13
* Make links clickable (thanks Katelyn Dickey)
* Show progress bar while removing contact (thanks Airplane Mode)
* Show correct status when trying to share forums/blogs/groups with contacts that don't support them

### [1.4.19](https://code.briarproject.org/briar/briar/commits/release-1.4.19)

2023-01-04

* Update translations
* Fix two crashes related to Bluetooth permissions
* Show an explanation if the wrong kind of QR code is scanned

### [1.4.18](https://code.briarproject.org/briar/briar/commits/release-1.4.18)

2022-12-22

* Update translations
* Fix a crash when the Tor process exits
* Fix a crash at startup on Android 13

### [1.4.17](https://code.briarproject.org/briar/briar/commits/release-1.4.17)

2022-12-07

* Update translations
* New illustrations by Ura Design
* Target Android 12
* Use new Bluetooth permission instead of location permission on Android 12
* Show a hint that both people must scan each other's QR codes or add each other's links

### [1.4.15](https://code.briarproject.org/briar/briar/commits/release-1.4.15)

2022-10-28

* Update list of Tor bridges
* Use uTLS for meek to improve Tor connectivity in Iran and China

### [1.4.14 (alpha release)](https://code.briarproject.org/briar/briar/commits/alpha-1.4.14)

2022-10-21

* Update list of Tor bridges

### [1.4.13 (alpha release)](https://code.briarproject.org/briar/briar/commits/alpha-1.4.13)

2022-10-19

* Update translations
* Use uTLS for Snowflake to improve Tor connectivity in Iran and China
* Don't crash when the Tor process crashes

### [1.4.12](https://code.briarproject.org/briar/briar/commits/release-1.4.12)

2022-10-04

* Update translations, add Georgian translation
* Update list of Tor bridges
* Upgrade Tor to 0.4.5.14
* Upgrade obfs4proxy to 0.0.14
* Use Snowflake bridges in China, Iran and Turkmenistan
* Add separator above "sign out" button (thanks FlyingP1g)
* Add link to privacy policy (thanks FlyingP1g)
* Show trust indicators in contact list (thanks johndoe4221)

### [1.4.11](https://code.briarproject.org/briar/briar/commits/release-1.4.11)

2022-09-06

* Update translations
* Update list of Tor bridges
* Add "about" screen (thanks FlyingP1g)

### [1.4.10](https://code.briarproject.org/briar/briar/commits/release-1.4.10)

2022-07-11

* Update translations
* Fix a crash when the app is relaunched after failing to shut down
* Try to fix a crash when Tor crashes at startup

### [1.4.9](https://code.briarproject.org/briar/briar/commits/release-1.4.9)

2022-07-04

* Update translations
* Update list of Tor bridges
* Use obfs4 and meek bridges in Iran and China
* Improve power management setup for Xiaomi/Redmi devices
* Allow more space for text in feedback and crash reports (thanks FlyingP1g)

### [1.4.8](https://code.briarproject.org/briar/briar/commits/release-1.4.8)

2022-06-13

* Update list of Tor bridges
* Update emoji
* Show orange connection status when connection to Tor network is lost
* Crash if the Tor process crashes, to help find the cause
* Fix a crash when attaching images or choosing a profile picture

### [1.4.7](https://code.briarproject.org/briar/briar/commits/release-1.4.7)

2022-05-12

* Update translations
* Fix crashes when opening or saving files
* Fix a crash when launching the app
* Fix a crash when reopening the app after using removable drives
* Fix two bugs affecting connectivity via Tor

### [1.4.6](https://code.briarproject.org/briar/briar/commits/release-1.4.6)

2022-04-13

* Update translations
* Update list of Tor bridges
* Upgrade Tor to 0.4.5.12
* Upgrade obfs4proxy to 0.0.12
* Fix a crash during account setup on Huawei devices
* Fix a crash when launching the app
* Compact the DB when signing out
* Change icon for adding members to a private group

### [1.4.5](https://code.briarproject.org/briar/briar/commits/release-1.4.5)

2022-02-27

* Update translations
* Update list of Tor bridges

### [1.4.4](https://code.briarproject.org/briar/briar/commits/release-1.4.4)

2022-02-09

* Update translations
* Fix a crash when attaching images or choosing a profile picture
* Fix a crash when sharing a private group with a contact who has been removed and re-added

### [1.4.3](https://code.briarproject.org/briar/briar/commits/release-1.4.3)

2021-12-20

* Upgrade Tor to 0.3.5.17
* Update translations
* Update list of Tor bridges
* Use non-default bridges in Russia
* Fix a crash when the hotspot for sharing the app fails to start
* Report amount of data used in crash reports and feedback

### [1.4.1](https://code.briarproject.org/briar/briar/commits/release-1.4.1)

2021-11-15

* Share the app with people nearby without internet access
* Exchange encrypted messages via SD cards and USB sticks
* Update translations

### [1.3.8](https://code.briarproject.org/briar/briar/commits/release-1.3.8)

2021-09-24

* "Connect via Bluetooth" feature for devices where Bluetooth doesn't work automatically
* Update translations
* Fix missing RSS icons in blog posts
* Fix a crash when Briar is killed in the background with the RSS feeds screen open

### [1.3.6](https://code.briarproject.org/briar/briar/commits/release-1.3.6)

2021-07-30

* Update translations
* Fix a crash when showing onboarding
* Fix a bug when an introduction doesn't complete
* Show a warning if the system clock is unreasonable

### [1.3.5](https://code.briarproject.org/briar/briar/commits/release-1.3.5)

2021-06-30

* Update translations
* Upgrade Tor to 0.3.5.15
* Fix a crash when viewing image attachments
* Fix error message when deleting a private group or forum

### [1.3.4](https://code.briarproject.org/briar/briar/commits/release-1.3.4)

2021-06-07

* Update translations
* Update list of Tor bridges
* Add instructions for protecting Briar from Xiaomi's power manager
* Fix a bug with disappearing message bubbles
* Fix a bug with scrolling to new blog posts

### [1.3.3 (alpha release)](https://code.briarproject.org/briar/briar/commits/alpha-1.3.3)

2021-05-03

* Update translations
* Target Android 11

### [1.3.2 (alpha release)](https://code.briarproject.org/briar/briar/commits/alpha-1.3.2)

2021-04-26

* Image attachments for private messages
* Profile pictures
* Disappearing messages
* Update settings screen
* Update translations
* Add Briar to Huawei app launch list during setup
* Fix a bug with the onboarding for the connections screen
* Refresh contact list when a contact is renamed
* Fix a crash when opening the main menu

### [1.2.20](https://code.briarproject.org/briar/briar/commits/release-1.2.20)

2021-04-02

* Update translations, add Myanmar translation
* Update list of Tor bridges
* Fix a bug that caused a button in the toolbar to be duplicated
* Fix a crash when sending feedback
* Fix a crash when connecting to contacts via Bluetooth

### [1.2.18](https://code.briarproject.org/briar/briar/commits/release-1.2.18)

2021-03-29

* Update list of Tor bridges
* Fix a crash when the screen is rotated during account creation
* Fix a bug that prevented toolbar buttons from being shown
* Fix a crash when unlocking the app

### [1.2.16](https://code.briarproject.org/briar/briar/commits/release-1.2.16)

2021-02-27

* Show ongoing notification in status bar on Android 8
* Fix a crash when a forum post is received

### [1.2.15 (alpha release)](https://code.briarproject.org/briar/briar/commits/alpha-1.2.15)

2021-02-18

* Update translations
* Upgrade Tor to version 0.3.5.13
* Restore scroll position when reopening a forum or private group
* Fix Tor connectivity in China
* Fix a crash when pressing the back button in the blog feed
* Fix a crash when adding a contact
* Fix a crash when creating account
* Fix language selector bug
* Fix disappearing link bug
* Fix crash reporter bug

### [1.2.14 (beta release)](https://code.briarproject.org/briar/briar/commits/beta-1.2.14)

2021-01-29

* Update translations
* Update list of Tor bridges
* Fix updating of private group member count

### [1.2.13](https://code.briarproject.org/briar/briar/commits/release-1.2.13)

2021-01-28

* Update translations
* Update app icon on Android 8 and later
* Update Tor to version 0.3.5.12
* Improve feedback screen and crash reporter
* Fix connectivity issues on IPv6-only networks
* Fix invalid contact links when language is set to Turkish

### [1.2.12](https://code.briarproject.org/briar/briar/commits/release-1.2.12)

2020-11-13

* Prefer Bluetooth when adding contacts in person
* Update translations

### [1.2.11](https://code.briarproject.org/briar/briar/commits/release-1.2.11)

2020-10-23

* Target Android 10
* Update translations
* Fix a crash when the device doesn't support Bluetooth

### [1.2.10](https://code.briarproject.org/briar/briar/commits/release-1.2.10)

2020-10-14

* Show more information about how Briar connects to contacts
* Use Tor v3 hidden services
* Improve Bluetooth and Wi-Fi connectivity
* Update translations
* Don't turn Bluetooth on or off automatically
* Fix duplicate unlock screen on Android 10+

### [1.2.9](https://code.briarproject.org/briar/briar/commits/release-1.2.9)

2020-07-14

* Improve Bluetooth and Wi-Fi connectivity
* Show orange icon while connecting to Tor network
* Simplify connection settings
* Update translations
* Upgrade Tor to 0.3.5.10
* Fix a bug that caused the user's account to be lost when upgrading
* Fix the position of the pending contacts snackbar

### [1.2.7](https://code.briarproject.org/briar/briar/commits/release-1.2.7)

2020-02-18

* Update translations
* Show "no internet" snackbar in pending contacts list when Tor is disabled
* Fix a crash when selecting messages to delete
* Fix crashes in private group list and blog feed
* Fix a crash when no web browser is installed

### [1.2.5](https://code.briarproject.org/briar/briar/commits/release-1.2.5)

2020-02-06

* Private messages can be deleted
* Encrypt database with a hardware-backed key if available
* Don't show logo in navigation drawer on small screens
* Improve Bluetooth connectivity on Android 8.0
* Update translations
* Upgrade Tor to version 0.3.5.9
* Fix crash when opening settings screen

### [1.2.4](https://code.briarproject.org/briar/briar/commits/release-1.2.4)

2019-11-06

* Contacts can be added by exchanging links without meeting in person
* Add translations for Bosnian and Swahili
* Update translations
* Upgrade obfs4proxy to version 0.0.11
* Fix crash when theme is set to system default
* Fix crash when custom notification sound can't be loaded
* Fix crashes when system dialogs aren't available
* Fix various bugs with keyboard behaviour
* Fix duplicate items in contact list
* Fix Tor bridges on Android 10
* Raise minimum Android version to 4.1

### [1.1.9](https://code.briarproject.org/briar/briar/commits/release-1.1.9)

2019-07-04

* Add translations for Icelandic, Korean, Lithuanian and Traditional Chinese
* Update translations
* Fix a crash when sending an empty message

### [1.1.8](https://code.briarproject.org/briar/briar/commits/release-1.1.8)

2019-06-28

* Update translations
* Fix a crash when a contact uses an alpha testing release
* Fix a crash when closing the crash report dialog
* Improve layout of crash report dialog on small screens

#### [1.1.7](https://code.briarproject.org/briar/briar/commits/release-1.1.7)

2019-05-16

* Update translations
* Improve layout of forums and private groups
* Protect Briar from being killed by Nokia's power manager
* Fix a crash when relaunching Briar from recent apps
* Fix a crash when sharing private groups, forums or blogs
* Fix a crash when giving a contact a long alias
* Fix a crash when rotating the screen in a private group
* Fix a bug that didn't show notifications for new RSS posts
* Fix a bug that disabled the send button when rotating the screen
* Fix a bug that re-showed an onboarding message when rotating the screen
* Fix various bugs with accepting and declining introductions
* Fix a bug that showed the contact list during a database upgrade

#### [1.1.6](https://code.briarproject.org/briar/briar/commits/release-1.1.6)

2019-03-26

* Add support for using Tor in China without a VPN
* Upgrade Tor to 0.3.5.8
* Fix a crash when relaunching Briar after signing out
* Fix a crash when changing a contact's nickname
* Fix a crash on Samsung devices running Android 7
* Fix a bug that caused the screen to flicker when Briar was in the background
* Add a setting to disable internet (Tor) connections when running on battery
* Make text input fields work in landscape mode
* Remember the scroll position in forums, private groups and blogs
* Show correct text when an existing contact is introduced
* Show correct sent/seen status for introduction messages
* Fix a link in the error message when adding contacts fails
* Don't request location permission on Android 5 or older
* Don't scroll conversation to bottom when a message arrives
* Remove Wi-Fi IP address from crash reports
* Update private group's sharing status when creator's join message arrives
* Fix a layout bug in the list of private groups
* Move private group member list button to menu
* Update sharing status screens while they're open
* Fix a bug that prevented the empty contact list icon from appearing
* Make the external link warning dialog scrollable
* Remove unnecessary snackbars when posting forum and private group posts
* Don't show notifications for the user's own blog posts
* Improve the panic button response
* Fix a bug that caused unread messages to be marked as read
* Show new contacts at the top of the contact list
* Update translations, add Azerbaijani, Macedonian and Ukrainian

#### [1.1.5](https://code.briarproject.org/briar/briar/commits/release-1.1.5)

2018-11-20

* Contacts can be renamed
* Added pluggable transport support for Tor
* Disabled Tor's connection padding when using mobile data or battery
* Fixed a bug that caused recently used emoji to be shown in the wrong order
* Updated translations

#### [1.1.4](https://code.briarproject.org/briar/briar/commits/release-1.1.4)

2018-11-06

* Support adding contacts via Bluetooth on Android 8.1 and newer
* Fixed layout issues with right-to-left languages
* Fixed a crash when key agreement fails
* Updated dark theme settings on Android 8.0 and older
* Updated translations

#### [1.1.3](https://code.briarproject.org/briar/briar/commits/release-1.1.3)

2018-10-17

* Support for right-to-left languages
* Improved settings for app lock
* Upgraded Tor to 0.3.4.8
* Keep screen on when scanning QR codes
* Compact database every 30 days
* Localised crash screen, how numbers are written
* Fixed bug with app lock icon
* Fixed integration with Ripple panic button app
* Fixed crash when memory is low
* Fixed bug with internet indicator
* Updated translations

#### [1.1.1](https://code.briarproject.org/briar/briar/commits/release-1.1.1)

2018-09-14

* Dark theme
* New look for private conversations
* Lock the app without signing out
* Reminder to sign in after restarting phone or upgrading app
* Support for Tor bridges
* Support for using Tor with a VPN in China and Iran
* Updated emoji
* Updated translations
* Fixed a bug that could cause the user's account to be lost

#### [1.0.13](https://code.briarproject.org/briar/briar/commits/release-1.0.13)

2018-07-31

* Fixed a bug that prevented Briar 1.0.12 from connecting to contacts

#### [1.0.12](https://code.briarproject.org/briar/briar/commits/release-1.0.12)

2018-07-30

* Upgraded Tor to version 0.2.9.16
* Updated translations
* Added experimental support for Chromebooks

#### [1.0.11](https://code.briarproject.org/briar/briar/commits/release-1.0.11)

2018-07-04

* Fixed a crash when going back to the setup screen
* Fixed a bug that prevented Briar from connecting to the internet
* Fixed a bug that caused the screen overlay warning to be shown
* Added button to show password
* Don't show protected apps button on newer Huawei devices
* Don't hide UI on low memory if app is in the foreground
* Use message icon rather than mail icon
* Updated translations

#### [1.0.9](https://code.briarproject.org/briar/briar/commits/release-1.0.9)

2018-06-20

* Fixed a bug that caused Briar to sign out automatically on Huawei devices
* Updated translations

#### [1.0.8](https://code.briarproject.org/briar/briar/commits/release-1.0.8)

2018-06-08

* Fixed a bug that prevented Briar from connecting to the internet
* Added a language selector to the settings screen
* Updated translations, added Occitan translation

#### [1.0.6](https://code.briarproject.org/briar/briar/commits/release-1.0.6)

2018-06-01

* Fixed a crash when reblogging on Android 7
* Fixed a bug that caused messages to be marked as read
* Updated translations, added Polish translation

#### [1.0.5](https://code.briarproject.org/briar/briar/commits/release-1.0.5)

2018-05-22

* Updated translations
* Fixed a bug that could cause introductions to fail
* Fixed a bug that could cause accounts to be lost

#### [1.0.4](https://code.briarproject.org/briar/briar/commits/release-1.0.4)

2018-05-17

* Fixed a security issue - all users should upgrade
* Updated translations, added new translations for Farsi and Asturian
* Improved the appearance of the settings screen
* Removed unused READ_LOGS permission

#### [1.0.3](https://code.briarproject.org/briar/briar/commits/release-1.0.3)

2018-05-14

* Fixed a crash when rotating the screen during account creation
* Ensure new account is written to disk immediately
* Ensure connections are closed when removing a contact
* Clear the UI when device memory is critically low

#### [1.0.2](https://code.briarproject.org/briar/briar/commits/release-1.0.2)

2018-05-09

* Updated translations
* Improved account creation screen

#### [1.0.1](https://code.briarproject.org/briar/briar/commits/release-1.0.1)

2018-04-30

* Disabled automatic sign out when memory is low
* Fixed a bug that caused private messages not to appear
* Updated translations

#### [1.0.0](https://code.briarproject.org/briar/briar/commits/release-1.0.0)

2018-04-29

* First release
* End-to-end encrypted private messaging, private groups, forums and blogs