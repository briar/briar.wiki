#### Settings: 

**Device 1**
Device settings wi-fi = ON
Briar settings wi-fi = OFF, bluetooth = OFF, internet = OFF


**Device 2**
Does not have Briar installed yet

#### Steps to execute

**Device 1**
- go to settings > share Briar app offline 
- Tap the Start Sharing button
- the wifi details are displayed on the screen 

**Device 2**
- go to device settings and add the network whose details were given on the device 1 screen

**Device 1**
- tap the 'Start app sharing' button (the name of this button may change in the future)
- screen disaplys the IP address to be entered into the browser of the device 2

**Device 2**
- enter the IP address into the address bar of the browser on the device 2
- tap the Download Briar button



#### Expected results

- Download is successful on device 2
- Device 1 - all the connection settings on Briar as still off
- users can add each other as nearby contacts 
- users can send each other messages


[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)