Discussion in MM




But in case where sender's mailbox is also offline (maybe flat battery, for example), but the recipient's mailbox is online - will the sender's messages simply then be uploaded to the mailbox that is online?  

I think the assumption is that mailboxes are always online and I think the idea was to only use contact mailbox if they have one. Don't know if @akwizgran already thought about this case, when it is unexpectedly offline.

12:33 PM






And what happens to messages on mailbox after they are forwarded to intended recipients?

The recipient actively downloads them and after a successful download issues a deletion of the messages.

if the mailbox was paired before adding the contact then i think you're right, the mailbox properties would be synced unless the initial bt connection was lost, and so the contact would later be able to upload messages to the mailbox

for contact that was created by BT and never contacted before

Depends what this means. Had they had sufficient connected time via BT? If yes, I guess the mailbox properties would get synced and then used. But if you cut the BT too early, it might not.

3:12 PM
in that scenario we'd expect that adding a contact via bt or adding the contact remotely would work in the same way (with, in both cases, a risk of the initial connection being lost, in which case the mailbox wouldn't be usable for communicating with the new contact until another direct connection had been made, to finish transferring the mailbox properties)

3:13 PM
but there's also another scenario, where the contact is added before pairing the mailbox. in that case we wouldn't expect the mailbox to be usable for communicating with the contact until a direct connection with the contact had been made

3:15 PM
i don't think we can reliably reproduce the situation where the initial connection is lost, so i suggest we don't try to cover that situation with manual tests. but we could cover the other two:

mailbox is paired, then contact is added. expected result: mailbox can be used for communicating with contact (ie user and contact can communicate via the mailbox when not online at the same time)
contact is added, then mailbox is paired. expected result: mailbox can't be used until a direct connection to the contact has been made (ie user and contact have to be online at the same time at least once after the mailbox is paired, and after that they can communicate via the mailbox when not online at the same time)

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/Testing)