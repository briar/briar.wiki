Ticket #1821

Related are: #1960, #1961, #1962, #1963 and #1972

Comments in #1961:

* Both devices have Bluetooth turned off before testing the feature
* The devices are already connected to each other via Bluetooth before testing the feature (to achieve this, turn off wifi and add the devices as contacts nearby)
* At least one of the devices is running Android 10 or higher with the device's location setting turned on/off
* At least one of the devices is running Android 6 or higher and has/has not previously added a contact nearby, therefore has/has not requested the location permission
* Start the connect via Bluetooth workflow, then after accepting the system Bluetooth discoverability dialog, quickly return to the contact list and try to add a contact nearby, and vice versa

**Scenario 1 - two devices with new Briar installation, no contacts on either.  They connect via distance connection (exchanging codes)**

Devices used: Samsung Mini API19 and Nokia 3.1 API29 (Android 10)

#### Steps to execute:
**Device1**
- Add contact at a distance
- Go to settings and switch the internet and wifi off in Briar app, Bluetooth setting is ON
- The contact is shown offline
- tap on the contact and go to menu (upper right corner) and select Connect via bluetooth.
- Follow onscreen instructions

**Device2**
-  Add contact at a distance
- Go to settings and switch the internet and wifi off in Briar app, Bluetooth settng is ON
- The contact is shown offline
- tap on the contact and go to menu (upper right corner) and select Connect via bluetooth.
- Follow onscreen instructions

#### Expected results:
- Messages shown to both users are: 'Connecting via bluetooth', 'Connected via bluetooth' in case of successful connection
- Contacts are shown as online
- Contacts can send messages to each other


[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)

