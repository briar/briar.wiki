Seb's instructions: 

Scenario 4 (restarting device does not start mailbox on boot if setup has been started but cancelled during qr-code stage)

* Reset data for mailbox app or uninstall and reinstall.
* Start mailbox app, proceed until qr code is visible.
* Cancel setup using the cancel button
* Restart device. Confirm that mailbox app **does not start** up automatically after boot

Steps to execute: 

As above. 

Canceling can be done immediately before scanning, during scanning, or immediately after. 
It could also be done on Briar side - at the same points as on mailbox side. 
Or it could be canceled on both ends at the same time (inasmuch as this is possible to do when manually executing and therefore proably not being able to do it at the exact same milisecond)

We're looking here that there be no crashes - that any annomalies or problems are handled gracefully. 

[Back to testing](https://code.briarproject.org/briar/briar/-/wikis/testing)