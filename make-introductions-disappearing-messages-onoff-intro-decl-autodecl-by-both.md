Test objective: verify that behaviour of all messages on all devices is as expected when the sender has the Disappearing messages ON for one contact, OFF for the other contact, and both of the contacts auto decline the intro

Setup

Contacts: 
Sender's device: 
Sender <-> Recipient1 (disappearing messages = OFF) 
Sender <-> Recipient2 (disappearing messages = ON)

Recipient1 device:
Recipient1 <-> Recipient2 conversation disappearing messages = ON
Recipient1 <-> Sender conversation disappearing message = ON (but should become OFF when it mirrors the setting it receives from the Sender

Recipient2 device:
Recipient2 <-> Recipient1 conversation disappearing messages = OFF
Recipient2 <-> Sender conversation disappearing message = OFF (but should become ON when it mirrors the setting it receives from the Sender


contact already exists between Recipient1 and Recipient2 (which is not necessarily known to Sender)

Steps to execute:

* [ ] Sender selects a contact (Recipient1) and then selects the Make introductions option for that contact.
* [ ] Sender selects the other contact that they want to introduce Recipient1 to.

Expected results:

Sender's device

* [ ] Sender types in some text and sends the introduction message
* [ ] Sender's text message is accompanied by the automatically generated message "you have asked to introduce Recipient1 to Recipient2" and this auto msg does not contain the bomb icon
* [ ] Sender's messages does not disappear from their screen.

Recipient1 changes the Disappearing messages setting on their device to ON before Declining, then taps 'decline'
* [ ] When Recipient1 declines the introduction, sender's device has these messages in the conversation screen with Recipient1:
* [ ] Automatically generated message "Recipient1 declined the introduction to Recipient2" with the little bomb icon.
Recipient 2 changes the Disappearing messages setting on their device to OFF before tapping Decline
* [ ] After Recipient2 declines the introduction, in the conversation screen with Recipient2, Sender sees these messages:

Sender's device: 
* [ ] Automatically generated message "Recipient2 declined the introduction to Recipient2" (withOUT the little bomb icon, as Recipient2 has changed their Disappearing Messages setting to OFF )
* [ ] Recipient1's messages sent by them after the change of setting on Rec1 device - disappear after the timer expires
* [ ] Recipient2's messages + atuomatically generated messages related to the recipient2 that have been sent by them after the change of Dsiappearing Messages settging - do not disappear

Recipient1's device

* [ ] Recipient1 > Contact list > there is a new message for them from Sender
* [ ] Recipient1 opens that message and they see:
* [ ] Text message from Sender accompanied by: "Sender has asked to introduce you to Recipient2. Recipient2 already exists in your contct list, but the Sender may not know that.  You can still respond" Accept and Decline are tappable. (There is NO bomb icon on this message.)
* [ ] Rcipient1 changes their Disappearing messages setting to ON 
* [ ] Recipient1 taps Decline.
* [ ] Recipients 1 sees another automatically generated message: "Youhave declined the introduction to Recipient2" + little bomb icon
* [ ] Recipient2 is not added again to the Recipient1's contact list.
* [ ] The above messages generated after the Rec1 changed their disappearing messages setting, do disappear.


Recipient2's device

* [ ] Recipient2 > Contact list > there is a new message for them from Sender
* [ ] Recipient2 opens that message and they see:
* [ ] Text message from Sender accompanied by: "Sender has asked to introduce you to Recipient1. Recipient1 is already in your contact list, but since Sender might not know that, you can still respond?" Accept and Decline are tappable. There is a little bomb icon on this message.
* [ ] Recipient2 changes their Disappearing Messages setting to OFF
* [ ] Recipient2 declines.
* [ ] Recipient2 gets another automatically generated message: "you have declined the introduction to Recipient1." The bomb icon is NOT present at the bottom of this message (because of the cnage of setting in previous steps).
* [ ] Recipient1 is NOT added again to the Recipient2's contact list
* [ ] Messages generated after the change of Disappearing Messages setting do not disappear.  The one received initially from sender - with the bomb icon, does disappear.


