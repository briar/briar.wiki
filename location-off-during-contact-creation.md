

During the creation of nearby contact - switch off the app's location permission

This will restart Briar app, and it will be necessary to login again.  

After that, process continues OK.



- [ ] Scenario 6

Receive text messages + self-destructing messages, change of profile picture from an existing contact whilst making connection with another nearby contact

This is OK.

- [ ] Scenario 7

Delete one contact and try to reconnect with them - they have not deleted the contact that is trying to reconnect with them

This is OK.  The user who deleted their contact gets shown message: Contact 1 is added, and the user who hasn't deletedtheir contact gets the message 'Contact 2 already exists... ' and the contact is not created again

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)
