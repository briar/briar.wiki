This screen should be showing after the mailbox and briar app are linked - ie the mailbox QR code is scanned by Briar

![Screenshot_2022-05-30_at_13.03.26](uploads/f1e07c97a1233a08ef3bf3a0bb67a9f9/Screenshot_2022-05-30_at_13.03.26.png)

At the same time when the notification bar is pulled down, it should be showing 

![Screenshot_2022-05-30_at_13.04.50](uploads/7045f5067fedc7e733a417073f7fba48/Screenshot_2022-05-30_at_13.04.50.png)

The purpose of this test is to tap on every link and every button of this screen, in portrait and landscape, on different devices

The tappable buttons and links are: 

Stop button on the Mailbox screen - when this button is tapped, the mailbox exits.  There are no mailbox notifications when mailbox is stopped. 

Stop the app, and launch it again (in which case it should not connect automatically, because it was rpeviously stopped manually, see ticket https://code.briarproject.org/briar/briar-mailbox/-/issues/87

Last connection time should be correct

And lastly, tap the Unlink link at the bottom of the screen, which should lead to ... 

![Screenshot_2022-05-30_at_13.15.09](uploads/4cac09470166158f3449d663f285d29e/Screenshot_2022-05-30_at_13.15.09.png)


Repeat on different devices, portrait and landscape, navigate back and forth, double tap the Unlink link on the status screen etc.  

The difference between the mailbox status screen on the mailbox side and Briar side: 
- on the mailbox device, mailbox status screen contains the stop button. On the Briar side of things, the mailbox status screen contains Check Connection Link.  Which means that on the mailbox device, the user can choose to stop mailbox but we cannot do that from the Briar device.  On the other hand, we are able to check the Briar-Mailbox connection from the Briar device, but we cannot do that from the Mailbox device. 


[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)




