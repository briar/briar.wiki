1. Privacy-connectivity tradeoff
    * We don't want to reveal that we're part of the network
    * But we can only communicate with those who know

2. Privacy-efficiency tradeoff
    * We don't want to reveal what data we're sending and receiving
    * But sending everything to everyone doesn't scale

3. Security-connectivity tradeoff
    * A device can use more of the network's resources than it contributes
    * We can use stable identities to manage resource allocation
    * But this limits us to exchanging data with known identities

4. Side-effects
    * Members of a group may receive messages in different orders
    * We can use rules to resolve conflicts and ensure eventual consistency
    * But processing a message can have side-effects that can't be reordered or undone

5. Consensus manipulation
    * Members of a group may receive messages in different orders
    * We can use rules to resolve conflicts and ensure eventual consistency
    * But the rules can be abused to manipulate the outcome

Problem 4 is one of the things that makes multi-device support difficult - although @grote's leader/follower idea could sidestep the problem.

We ran into problem 5 when working on private groups, and it's also affecting Matrix's room consensus algorithm at the moment.

At a very generic level, problems 4 and 5 can be solved with some kind of consensus mechanism in which a quorum of group members agree on an ordering of the messages. One of the key questions a mechanism like this needs to answer is who gets to be part of the quorum. If we can come up with a solution to this that's reusable across clients, it would be a very useful building block for Bramble-based systems.