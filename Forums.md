General

It is at all times during testing -- like after actions taken, events occuring
etc -- relevant to rotate the device and ensure that:

- Text entered in fields, switches toggled etc remain so
- Visibility of progressbars and buttons are the same
- Labels on buttons remain the same
- Opened dialog boxes remain so




Forum Functionality
- [ ] A user can create new forum
- [ ] New forum displays correct name after creation
- [ ] Forum creator is able to create apost within the new forum
- [ ] Forum creator is able to share the forum, and select the contact with whom they want to share it
- [ ] Forum creator is able to send an accompanying message together with invitation to forum
- [ ] Selected contacts receive forum invitation as well as accompanying message
- [ ] Invited contacts can accept or decline the invitaito to forum
- [ ] Accepting the invitaiton to forum gives them access to the forum posts
- [ ] Whether they accept or decline, the forum creator receives an automatically generated message confirming either acceptance or decline decision
- [ ] Invited contacts can reply to the creator's posts
- [ ] Invited contacts are able to see new posts by creator (without any additional action ont he part of forum creator)
- [ ] Invited contacts receive notificaiton when forum creator creates new posts/replies in the forum
- [ ] Invited contacts are able to create new posts and the forum creator gets notified
- [ ] When a person replies to the post or creates a new post, on their contacts' devices, their name is disaplayed with the number of posts/replies they wrote (the same way as when they write private messages)
- [ ] Invited contacts to forum are able to invite their own contacts to the forum
- [ ] All messages in the forum are visible to all invited people (portraint and ladnscape orientation)
- [ ] Profiel photos/avatar of forum members are only visible to their own direct contacts, and not to forum members who are not direct contacts (they see a generic avatar instead of the real one)
- [ ] forum replies are disaplyed in threads (portraint and landscape orientation)
- [ ] Forum creator is able to leave forum and the forum continues to exist
- [ ] Forum members can leave forum and forum continues to exist
- [ ] Forum creator is invited back to the forum by other members, they accept, and are able to continue as though they never left
- [ ] Forum name does not diaply the avatar of the forum creator, but the first letter of their name, whereas the forum posts do display the avatar of the user who created post (only visible to their direct contacts)
- [ ] scroll up and down the list of posts in portrait and landscape orientation 
- [ ] check share status for forum - it should list all the direct contacts of the device with whom a particular forum was shared


Not implemented at this time

- [ ] Forum messages cannot be deleted 
- [ ] Not possible to attach attachments to forum posts
- [ ] Not possible to unshare forum with the person with whom it was previsouly shared

Devices used

This is tested on 5 real devices, as the interaction between them is the main thing tested which would be difficult to achieve using Android Studio emulator. In this scenario 
- Some of them were direct contact with each other, and they had mutual contacts, and 
- some of them were connected only to one contact and didn't have any mutual contact with anybody else.  
- Some of them were invited to forums, and some of them were not even though they were direct contacts with some forum members. Scenarios - in the attachment.

[Forum_scenarios_10022021.pdf](uploads/932109a2e159b45a7c631ec49c4a38b8/Forum_scenarios_10022021.pdf)




