Test objective: verify that behaviour of all messages on all devices is as expected when the sender has the Disappearing messages ON for both contacts, but one of the contacts declines the intro

Set up: 

Contacts: 
Sender <-> Recipient1 (disappearing messages = ON) and 
Sender <-> Recipient2 (disappearing messages = ON), 
no contact between Recipient1 and Recipient2, disappearing messages on their devices is ON.

Steps to execute: 
- [ ] Sender selects a contact (Recipient1) and then selects the Make introductions option for that contact. 
- [ ] Sender selects the other contact that they want to introduce Recipient1 to. 

Expected results: 

Sender's device
- [ ] Sender can add an option text message to the introduction.  The text input field here does not contain the bomb icon nor the placerholder text (new disappearing message) like it does when a private disappearing message is being created - this decision was made in order to avoid ambiguity in cases where introductions are sent to multiple contacts, some of which may be disappearing and some not.
- [ ] Sender sends the introduction message
- [ ] Sender gets an automatically generated notice that their messages will disappear after 7 days and there is a 'Tap to learn more'.  
- [ ] Sender's text message is accompanied by the automatically generated message "you have asked to introduce Recipient1 to Recipient2" and this auto msg contains the bomb icon 
- [ ] Sender's messages disappear from their screen after the timer expires.
- [ ] When Recipient1 accepts the introduction, sender's device has these messages in the conversation screen with Recipient1:
- [ ] Automatically generated message "Recipient1's messages will disappear after 7 days.  Tape to learn more."
- [ ] Automatically generated message "Recipient1 accepted the introduction to Recipient2" with a little bomb icon.
- [ ] After Recipient 2 declines the introduction, in the conversation screen with Recipient2, Sender sees these messages: 
- [ ] Automatically generated message "Recipient2 declined the introduction to Recipient2" (with the little bomb icon)
- [ ] Recipient1's messages + automaticalluy generate messages related to the recipient1 disappear.  
- [ ] Recipient2's messages + atuomatically generated messages related to the recipient2, do not disappear (as their Disappearing Messages setting on the sender's device was OFF)

Recipient1's device
- [ ] Recipient1 > Contact list >  there is a new message for them from Sender 
- [ ] Recipient1 opens that message and they see: 
- [ ] Automatically generated message "Sender's messages will disappear after 7 days; Tap to learn more." 
- [ ] Text message from Sender accompanied by: "Sender has asked to introduce you to Recipient2.  Do you want to add Recipient2 to your contact list?" Accept and Decline are tappable. There is a little bomb icon on this message.
- [ ] Recipient1 accepts the introduction
- [ ] They see an automatically generated message: "Your messages will disappear after 7 days. Tap to learn more."
- [ ] Another automatically generated message: "You accepted the introduction to Recipient2. Before Recipient2 gets added to your contacts, they need to accept the intro as well.  This may take some time."  The small  bomb icon is present at the bottom of this message.
- [ ] Recipient2 is not added yet to the Recipient1's contact list.
- [ ] The above messages disappear after the timer expires.

Recipient2's device
- [ ] Recipient2 > Contact list >  there is a new message for them from Sender 
- [ ] Recipient2 opens that message and they see:
- [ ] Automatically generated message 
- [ ] Text message from Sender accompanied by: "Sender has asked to introduce you to Recipient1.  Do you want to add Recipient1 to your contact list?" Accept and Decline are tappable. There is a little bomb icon on this message.
- [ ] Recipient2 declines the introduction
- [ ] Another automatically generated message: "You declined the introduction to Recipient1."  There is NO bomb icon is present at the bottom of this message.
- [ ] Recipient1 is NOT added to the Recipient2's contact list 
- [ ] The above messages disappear after the times expires.
- [ ] Default Disappearing messages setting for all new contacts is OFF.  

Repeat this scenario by swapping the roles of Recipient1 and Recipient2: Recipient1 declines, and Recipient2 accepts the introduction.
Repeat once more and let both contacts accept the intro.



[Back to testing](https://code.briarproject.org/briar/briar/-/wikis/Testing)


 

