Delete an already created contact on one device and re-create it with the same code that was used previously. Used the Copy and Paste buttons underneath each field.
* Contact is not going to be created.  It will only be created if both contacts are deleted and re-added.
* The newly created contacts should be showing as online
* Contact can send each other messages, blogs, forums, profile pictures etc

[Back to testing](https://code.briarproject.org/briar/briar/-/wikis/testing)