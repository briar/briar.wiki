Addressing the ticket https://code.briarproject.org/briar/briar-mailbox/-/issues/142

Current understanding: 

- Set the do-not-keep-activities to ON
- Install the mailbox app and link it with the briar app on another device 
- Wipe the mailbox 
- Restart the mailbox
- After walking through the intro screens, the QR code screen should show

(in the ticket 142, it seems that the 'wiping complete' screen shows = bug)


"it means, when you start the mailbox in a state where the app itself "thinks" it has never run on this device before (doesn't matter if it really has just been installed for the first time, has been wiped using the local or remote wiping or the app data has been cleared in the system settings), then there is a bug that happens right after finishing the onboarding if the do-not-keep-activities option is enabled. It shows the wiping-completed screen although it really should not be shown there and independent of whether the app has been wiped before or not" (Seb in MM, 22/07/22, Testing channel)

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)
