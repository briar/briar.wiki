#### Steps to execute: 

- ON the device that has donloaded and installed the Briar app from the hotspot of another device, create the user account
- set up the name
- enter and confirm password
- Allow briar to run in the background
- Create account

#### Expected results: 
- User account is created successfully 
- user can log out and log back in successfully

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)