https://code.briarproject.org/briar/briar/-/merge_requests/1710

* bluetooth plugin should never be enabled when the permission is missing
* enabling bluetooth plugin should require permission (via settings *and* connection screen)

- if nearby devices = OFF for Briar app - then the BT connection (Briar > settings > Connections) has to always be off.  
- every time this setting is turned off in device settings, the BT connection is also turned off in Briar on Android 12 

If the location and camera settings are OFF in older androids, the user can still switch the BT in Briar connections on and off without explicitely giving permissions to Briar to access anything - simply by moving the switch between the on and off possitions. 

However, in Android 12 if the Nearby devices setting is off, teh BT connection in Briar has to always be off.  To be able to switch it on, the user is obliged to explcitely give Briar permission to access Nearby Devices.  

Result: OK, build 25f99bd7decfb1494462e54c7f92d2a11e025df0


[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)