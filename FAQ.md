List of questions:

[[_TOC_]]

For questions related to Briar Mailbox, see [the respective FAQ](https://code.briarproject.org/briar/briar-mailbox/-/wikis/FAQ).

# Will there be an iOS version of Briar?

Short answer: Probably not.

Long answer: [We're looking into whether an iOS version is feasible](#445).
Briar needs to run in the background to receive messages from contacts, and iOS has
much tighter **restrictions on background apps** than Android (though
Android's getting stricter).

A typical iOS messaging app would use a push notification to wake the
app when a message is received, but this exposes metadata to Apple's
push notification service and the app developer's push gateway.

To reduce the metadata leakage, the sender of the message could connect
to the push gateway via Tor - but the push gateway and APNS would still
know who was receiving the notification. In a publish-subscribe network
like Briar, if a bunch of people always receive notifications whenever
the Revolutionary Planning Council updates its blog, then even if we
don't know who sent the notifications, we've learned something sensitive
about the recipients.

If we don't use push notifications then the best Apple allows us to do
is wake up every 15 minutes and check for messages. But maybe the sender
won't be online when we check (their 15 minute intervals might not be
aligned with ours - clocks aren't perfect). So we need somewhere for the
sender to store the message until our next check.

We're working on a piece of software called a [mailbox](#1011) that will receive
encrypted messages over Tor and store them until the owner collects
them. It's designed to run on a spare Android device, laptop, or
Raspberry Pi that's plugged into power and internet. Once that's ready
it will make an iOS app more feasible. But we are not sure if the 15 minute
delay will be a deal-breaker.

# How can I delete my account?

Your account is only stored on your device, so there's nothing to delete from our servers. For Android, you can completely delete your account by uninstalling the app. For Android or desktop, you can completely delete your account by signing out of Briar, opening Briar again, clicking "I have forgotten my password" at the password prompt, and then clicking "Delete".

# What's the difference between a private group, a forum and a blog?

A private group is a group chat where the admin decides who to invite. A forum is similar, but anyone can invite new members.

A blog is a bit like a Telegram channel: you can write posts that all your contacts can see, and they can reshare individual posts or invite their contacts to subscribe to your feed.

If you import an RSS feed it's converted into a Briar blog, so you can share individual posts or invite your contacts to subscribe to the feed.

# When do posts in forums/groups/blogs get shared with others?

A post will be shared with someone if all of these conditions are met:

1. they're your contact, and
1. both of you subscribe to the forum/group/blog where the message was posted, and
1. both of you have chosen to let the other one know that you subscribe (by inviting the other one to join the forum/group/blog, or accepting an invitation from the other one).

When the other person receives the post, they apply the same rules to share it with their own contacts, and so on across the social graph.

In more technical terms, each forum/group/blog has a distribution graph where the nodes are subscribers and the edges are a subset of contact relationships (specifically, the subset that have chosen to reveal the subscription to each other).

# Can I have multiple identities/profiles/accounts?

Short Answer: No, that is not supported by Briar.

Long Answer: In a p2p network it's hard to prevent identities from being linked to each other if they live on the same device.
Consider the scenario where you have two identities, `A` and `B`. A contact of `A` and a contact of `B` get together and try to find out whether `A` and `B` are the same person. What information could they use?

* First, the times when `A` and `B` are online. In a p2p network we can't hide this from our contacts.
* Second, the network addresses that they use to communicate with `A` and `B`. If we use Tor then we can have a separate hidden service address for each identity, so that's fine. But with Wi-Fi and Bluetooth, the contacts can compare the addresses we gave them and see that it's the same device.
* Third, they could look for information leaks at the application layer. For example they could try to introduce `A` to `B`, and see if the protocol behaves differently than it would if `A` and `B` were on different devices. If we supported multiple identities, we'd have to be very careful to avoid any leaks like this in our application-layer code.

# Can I use the same account with several devices?

Short answer: No, that's not possible.

Long answer: Allowing the same account to be used on more than one device is a hard problem to solve in delay-tolerant p2p networks. The difficult part is how to reconcile conflicting actions performed on different devices, such as accepting an invitation on one device and declining it on another.
Our best idea to address this is to have a single authoritive main device that your other devices connect to in order to access your account.

# How do I back up my account?

Short Answer: This is not possible at the moment, but planned.

Long answer: Storing any key material in the backup would defeat forward secrecy. The problem is that if the backup falls into the wrong hands at some point in the future, it can be used to decrypt all your traffic since the time when the backup was made (assuming the adversary recorded the encrypted traffic at the time). This violates forward secrecy, which is one of our security goals. So we need to modify the protocols to provide forward secrecy in this scenario before implementing a backup feature. The progress is tracked in ticket #110.

# Is it safe to publish my `briar://` link publicly?

Your Briar link contains a public key and it is safe to publish in the same way as a PGP public key. If you want to contact someone via Briar, both of you need to add each other's links.

# Can people track my online status if I publish my `briar://` link?

No, your online status isn't exposed by publishing your `briar://` link. Only your contacts can tell whether you're online.

For a more technical explanation: Each time you add a contact, a separate Tor hidden service is created whose address depends on both your own and your contact's public key. See the [Bramble Rendezvous Protocol](https://code.briarproject.org/briar/briar-spec/-/blob/master/protocols/BRP.md#2-the-rendezvous-protocol) for more information. Once the contact has been added, Briar sends them the address of the long-term Tor hidden service that's used for receiving connections from your contacts.

# Does Briar provide anonymity?

No. Briar does not conceal your identity from your contacts. It provides **unlinkability** but not **anonymity**. This means nobody else can discover who your contacts are, but your contacts *may* be able to discover who you are.

For example, Briar shares your Bluetooth address with your contacts so they can connect to you via Bluetooth. Your contacts could use this information to confirm a guess about your identity. Your Bluetooth address is shared even if you add a contact remotely by exchanging `briar://` links.

Other device information shared with contacts in order to connect with them:

* The five most recent IPv4 LAN addresses and ports for the Wi-Fi interface
* The most recent IPv6 link-local address for the Wi-Fi interface (on some Android devices the IPv6 link-local address is based on the hardware MAC, so it has similar privacy issues to the Bluetooth address)
* The address of Briar's Tor onion service

None of this should reveal anything about your location or identity on its own, but it could be used to confirm a guess about your identity (e.g. "network logs showed that the suspect's laptop received address `192.168.0.222` from the router in the cafe, which is consistent with the LAN IP address we received from the anonymous whistleblower").

# Has Briar been independently audited?

Yes, Briar was audited by Cure53 in 2017. You can read the audit report here:

https://briarproject.org/raw/BRP-01-report.pdf

All the issues identified by the audit were fixed before the first public release of the app.

Security audits are expensive so we can't commission an audit for every release, but our current grant includes funding for another audit in 2023.

# Does Briar include malware, spyware, trackers or backdoors?

No. Briar doesn't include malware, spyware, trackers or backdoors - and we can prove it!

We have a reproducible build process that can prove that the application published on our website (and in Google Play and F-Droid) corresponds exactly to the published source code. You can read more about reproducible builds here:

https://reproducible-builds.org/

We encourage anyone who's interested in this issue to reproduce the Briar application from source for themselves. If you'd like to do that, here are the instructions:

https://code.briarproject.org/briar/briar-reproducer/-/blob/master/README.md

# My firewall shows that Briar is connecting to a lot of different IP addresses - should I be concerned?

Don't be concerned. Briar uses the [Tor network](https://support.torproject.org/about/) to connect to your contacts privately and securely. All of the IP addresses that Briar connects to are Tor relays. You can look up information about each IP address on the Tor project's website:

https://metrics.torproject.org/rs.html

# What permissions does Briar request on Android, and why?

Briar asks for the following permissions:

* "Let app always run in background" (on some phones this is called "Ignore battery optimizations"). Briar needs this permission so it can receive messages while the app is in the background.
* Permission to access the camera. Briar asks for this permission when adding a contact who's nearby so that Briar can scan a QR code. Briar doesn't use the camera at any other time.
* Permission to access your location. Briar asks for this permission when adding a contact who's nearby so that Briar can discover nearby Bluetooth devices. This permission is required by Android, because knowing that a certain Bluetooth device is nearby might reveal your location. Briar doesn't store, track or share your location in any way other than discovering Bluetooth devices.

# What do the ### symbols next to people's nicknames mean?

Three green # symbols indicate a contact who you added by scanning QR codes. Two orange # symbols indicate a contact who you added by exchanging links, or by an introduction from another contact. One red # symbol indicates a person who's not your contact.

# Do we all need to be online at the same time for introductions to work?

No. If you introduce Alice to Bob, they don't have to be online at the same time in order to get added as a contact to each other. Just like with private messages, all you need to do is make sure to be online at the same time as Alice and Bob after they both agreed to the introduction, in order to send them their respective contact information.

# Is there a timeout for introductions?

No. Unlike with adding contacts at a distance and its 48 hours timeout, there's no such thing for introductions.

# Why doesn't Briar let me take screenshots?

Briar prevents screenshots because this is the only way to prevent the operating system from including a screenshot in the recent apps list. The screenshots in the recent apps list may contain sensitive content, and they're stored outside your Briar account, so they could be accessed without your Briar password.

Android 12 has a new way for apps to ask the operating system not to include the app's content in the recent apps list, so in some future version of the app we may be able to allow users to take screenshots.

# Is it possible to use Briar with a VPN?

You can use Briar with a VPN as long as the VPN doesn't make its connections through the Tor network.

If you're using Orbot as your VPN, it should already have a built-in exception that allows Briar to make its own connection to the Tor network, bypassing Orbot. If you're using Orbot as your VPN on GrapheneOS and you enable the "Block connections without VPN" setting in GrapheneOS then Briar will not be able to connect to the Tor network. You should either disable this setting or use a different VPN.