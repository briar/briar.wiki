These tests are run in response to comments in  MR !1515

The objective is to verify that the user gets transfered to correct settings screens on devices where that is the case (which means newer devices, as older ones do not transfer the user to device settings)

### API 16
**Devices:** 
Samsung GT S7710, Android version 4.1.2 
Samsung GT S6810P, Android verison 4.1.2

Location setting not applicable to this API level
If Wifi setting is turned off for the device, starting the hotspot (tapping the Start Hotspot button) restarts the wifi and opens the hotspot.  Other devices are able to connect to the hotspot.



### API 19
#### Device 
**Samsung I9195 MiniAndroid version 4.4.2**
Device wifi setting = OFF
When the Briar's Start Hotspot button is tapped, the hotspot details screen shows.  This process switches the device wifi setting back to ON. 

If the device wi-fi setting is ON, on tapping the Start Hotspot button the wifi details screen shows in the normal way. 

This device doesn't have the possibility to switch the location off.
When the hotspot is created, other devices (HTC ONe M9, for example) are able to 'see' it and connect to it.  


### API 23
#### Device
**Motorola Moto E (2nd generation) Android version 6** 

Wifi setting on device is set to OFF.  

Device wifi setting = OFF 
When the Briar's Start Hotspot button is tapped, the hotspot details screen shows.  This process switches the device wifi setting back to ON.  

If the device setting is ON, and there is a wifi network connected, then the user gets this message: “Warning.  Wi-fi connection is active.  Do you want to disconnect and connect to Direct device?"

The user is given this message only the first time after they log into the Briar app – if they try to start hotspot again later on, but without having logged out and then back in, they don't get this message. 

Device Setting Location = OFF, this setting does not seem to be taken into account (perhaps it will be in future versions of Android?)

Location permission for the app: OFF
When this setting is off, the hotspot can be created without the user being asked to give the app permission to use its location.  This is as expected for this API level.

### API 24
**Device**
HTC One M9

When wifi-setting is off on the device and the user taps the Start Hotspot button in Briar, the hotspot details screen is shown next.  (This process switches the device's wifi setting back on)

Location settingon device = OFF, and the Briar app permission to use location = OFF

After the user taps the Start Hotspot button, the next screen shows the hotspot details  - user is not asked to give Briar permission to use the location.

Samsung 9195 Mini is able to detect the hotspot and connect to it.

### API 29
#### Devices
**Samsung Galaxy A01 Core, Android version 10 **

WIFI setting on device = OFF
User gets this message: “To create a wifi hotspot,Briar needs to use W-fi.  Please enable it.

User can tap on Continue. 

At the bottom of the screen a new fragment appears allowing the user to change the wifi setting there and then, or the user can tap on See More and Done. 

If tapping done, without actually changing the setting, the same messages is shown again “To create a wifi.... please enable it” Tapping Continue brings user to the same place again.  

Tap 'See more' – user is transferred to the device wifi settings screen, where they can change the wifi setting – but tapping the back button does not bring the user back to Briar (which is as expected). 

If the user does change the wifi setting on the fragment at the bottom of the screen, then the process of  scanning for available networks starts, and then the user can tap on Done. 

After tapping on Done, the hotspot details screen shows.  This is all OK.

**Location setting for the device is OFF**

This setting doesn't seem to be taken into account, but maybe it will be in future version of Android

**Briar app doesn't have permission to access the device's location**

after tapping the Start Hotspot button, a dialog box comes up, asking the user to Allow Briar debug to access this device's location.  The user has options: Allow only while using the app, Deny and Deny and don't ask again. 
- Tapping on Deny – brings the user back to the Start Hotspot screen
- Tapping on Start Hotspot button, brings up this message for the euser: ”Location permission.  To create a wi-fi hotspot, Briar needs permission to access your location.  Briar does not store your location or share it with anyone.” User can only tap on Continue
- Tapping on Continue, the same dialog box comes up again... “Allow Briar debug to access this device's location? “ The options are: Allow while in use, Deny and Deny and ask again... Tapping on Deny, repeats the same steps as the two steps above.  
- Tapping on “Deny and don't ask again” brings the user back to Start Hotspot screen.  User taps on Start Hotspot button
- Now a new message comes up for the user: “You have denied access to your location, but Briar needs this permission to create a W-Fi hotspot.  Please consider granting access” The user can tap on OK or Cancel.  
- Tapping on Cancel cancels the opening of hotspot.  (and there was a crash here, which I could not repeat)
- Tapping on OK transfers the user to Permission manager for the app, where the user can allow the location permission for Briar
- After that, the user returns to Briar, and taps on Start Hotspot button, whereby the hotspot details screen opens.



**Nokia 3.1,  Android Version 10**

**Wifi setting on device = OFF**

Message to user: To create a Wi-Fi hotspot, Briar need to use wifi – please enable it. 

User is able to switch the wifi setting at the bottom of the screen where a wifi-setting fragment of the screen appears, (screenshot device-2021-09-21-150714.png)

If user taps done without actually enabling it, the initial user message reappears: 

“To create a Wi-Fi hotspot, Briar need to use wifi – please enable it. “

Then on the same screen as above, user can tap See More and they are taken to the device setting screen – correctly.

Or user can actually switch the wifi ON, in which case there is a search for available wifi networks and automatic connection to a known network

After that the user can still tap on the See More button, which takes them to the device settings (which now shows wifi = ON correctly) or user can tap on Done, which takes them to the next Briar screen which displays the Wifi details. Screenshots device-2021-09-21-151150.png and device-2021-09-21-151421.png

Motorola can connect to the hotspot and on Nokia, user is given the message '1 device connected

**Location permission**

Behaviour the same as for the Samsung Galaxy A01 Core (only no crash here)

#### API 30 
**Device**
Pixel 2, Android 11

**Wifi setting on device = OFF**

Behaviour is the same as on the Nokia 3.1 and samsung A01 Core.  

**Location setting on device = OFF and Location permission for the app = OFF**

When the user taps on Start  Hotspot button, a dialog comes up "Allow Briar Debug to access device's location?" The options given to the user are: 
- While using the app
- Only this time
- Deny
These options are different from API 29
Tapping on Only this time, opens the hotspot details screen.  A user might expect to be asked for that permission again the next time, but they don't.  The next time, the location permission is already on - so it seems that the button 'only this time' does not function like it says, but that is outside the remit of this test.
When the user taps 'Only this time' or 'While using the app', they are transferd to the app permissions screen (correctly) where they can give the app the location permission.  Back in Brir app, the hotspot detail screen is correctly shown after the location permission is given and the user has tapped on the Start Hotspot button again.

If the user taps Deny on the dialog, then they are shown this message: "You have denied access to your location, but Briar needs this permission to create a Wifi hotspot.  Please consider granting access".  Clicking in Cancel return the user to Start Hotspot screen, and tapping on OK transfers the user to the app permission screen where they are able to change the location permission Briar needs.

If on the app permission screen the user selects Only this time, when they return to Briar app, they are asked for permission again - ie the dialog comes up again.  The user can then select 'Ask every time' or 'While the app is in use' - both options lead hte user to the hotspot details screen.

However, if the user selects deny, they are given this message again: "You have denied access to your location, but Briar needs this permission to create a Wifi hotspot.  Please consider granting access".

One the access is granted, the hotspot details screen shows, and other devices are able to detect it and connect to it

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)