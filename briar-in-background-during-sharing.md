If user leaves Briar app to run in the background during the 'sharing app offline' process, this process will stop and other devices will lose connection to the hotspot.  When the user returns back to Briar, they will need to 'Start sharing' again.  This will open a hotspot again, to which other users can connect.

#2087 see for reference

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)

