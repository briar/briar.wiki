There are a few possibilities in this scenario: 

### Scenario 1

- Import the same message file from the removable drive that has already been imported

#### Expected results:

- The messages contained in the message file cannot be imported twice.  If the user tried to do this, they are given an on screen error message 

#### Scenario 2

- Import a renamed file containing the messages that have already been imported

#### Expected results:

- The messages contained in the message file cannot be imported twice.  If the user tried to do this, they are given an on screen error message 

#### Scenario 3

- User 1 exports some messages to user2,
- Before the messages are imported by user2, user1 re-establishes the connection with user2 and messages are delivered atuomatically
- User2 then tried to import the messages that were saved on the USB stick, but which have already been delivered automatically after that

#### Expected results:

- Messages contained in the message file cannot be imported because they have already been delivered to the user2 when the connection was re-established.  If the user2 tries to do this, they are given an on screen message 

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)