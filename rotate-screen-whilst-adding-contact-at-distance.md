1. Perform copying of code whilst the device is in portrait and pasting whilst device is in landscape.  Give the contact name in portrait, then tap Add contact in landscape.  
* Contact should be created successfully

2. Copy and paste the codes (using the popup Paste button), give the new contact a name, and tap Add contact, then immediately - home button.  Restart briar from recently used apps.  
* Contact should be created successfully

3. Copy and paste the codes (using the popup Paste button) in landscape, give the new contact a name, and tap Add contact, then immediately - back button.  Restart briar from recently used apps in portrait.  
* Contact should be created successfully

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)