

During the smailbox etup, when the do-not-kill-me fragment is displayed, I tap onto Allow connections, and then I leave the mailbox screen (push it into the background) and go to another app, for example device settings to check something, when I get back to the mailbox screen it progresses into the 'progress screen' without me tapping on Continue button. This is as designed, and confirmed by Seb on 28.11.2022, in MM Testing channel

If I meave mailbox screens before tapping Allow connections, and then come back, the do-not-kill me fragment remains there and I can still tap the Allow connections. All good.

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)