## Steps to execute
**Device 1**
- in Briar app, go to 'add nearby contact'
- follow instructions
- at several point during this process, send messages to the device 1 from another device

## Expected result 
- the contact creation process should complete correctly
- messages should be received correctly

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/Testing)