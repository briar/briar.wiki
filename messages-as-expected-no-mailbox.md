Reference: [Mailbox scenarios](https://code.briarproject.org/briar/briar/-/wikis/Mailbox-Scenarios)


### Scenario 1: No mailbox

When X and Y don't have mailboxes:

* X and Y will only exchange messages and acks when X and Y are online at the same time

In this scenario, two contacts (X and Y) communicate without a mailbox. While Y is offline, X writes a message to Y. The message is not sent (clock icon) until X and Y are online at the same time. When X and Y are online at the same time, the message is sent and acked (two ticks).

* Install Briar on X and Y
* Add X and Y as contacts
* In Briar's connection settings, turn off wifi and Bluetooth connections on X and Y
* Take Y offline
* Expectation: Y is shown as offline in X's contact list (after a delay of up to 1 minute for X to notice that Y is offline)
* X writes a private message to Y
* Expectation: X's message is shown as not sent (clock icon) in X's conversation screen
* While keeping X online, bring Y back online
* Expectation: X is shown as online in Y's contact list and vice versa (after a delay of up to 2 minutes for X and Y to connect to each other)
* Expectation: X's message appears in Y's conversation screen
* Expectation: X's message is shown as sent and acked (two ticks) in X's conversation screen

The aim of this test is toverify that messages still behave as they did before the mailbox realted functionality was introduced into Briar code.

Executed with the following pairs of devices: 
briar build: 34815eb1a51d3aac2f0978d0066c694c963afc3b , 21/09/2022

- HTC E9 Android 5 and Huawei P40 Lite 5G Android 10 = OK
- Pixel 2 Android 11 and Samsung Mini I9195 Android 4.4.2 = OK
- HTC One M9 Android 7 and Motorola E2 Android 6 = OK


[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/Testing)