**Recreate the same contact with the same code when the contact is already created**

* When you try add the contact with the same code that was already added, there should be a message saying: "is this the same person" and the user can choose Yes or No.  
* If Yes is chosen, no new contact is created. If No, then another message is shown, saying 'this person is trying to impersonate your friend'.. and the contact does not get added.
* Either way, the contact cannot be added twice. 
* Verify that existing contact (created with the same code) can continue to communicate - ie send each other messages, blogs, forums etc.
* If contacts get deleted from both devices, then the same previously generated code can be used to create the same contact again on each device

[Back to testing](https://code.briarproject.org/briar/briar/-/wikis/testing)
