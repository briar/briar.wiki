Steps for enabling GPG signed commits in Android Studio:

* Find the ID of your signing key
  * Run `gpg -K you@example.com`
  * Look at the line starting with `sec`
  * The hex digits after the slash are the key ID, e.g. `ABCD0123`
* Add the key ID to your global `.gitconfig`:
  ```
  [user]
          name = you
          email = you@example.com
          signingkey = ABCD0123
  ```
* (Optional) If you have problems with the GTK password prompt not showing up, try adding the following line to your `.gnupg/gpg.conf`:
  ```
  no-tty
  ```
* Enable signed commits in the project's `.git/config`:
  ```
  [commit]
          gpgsign = true
  ```
