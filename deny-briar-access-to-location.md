### Deny Briar access to location once 

**Device Settings:** 
Device 1 (Pixel2 Android 11) location services = On (Ask every time)
Device 2 location services = Off (Deny) -> On (Allow only while using the app)
**Briar settings:** 
Device 1 - connect via Bluetooth = On 
Device 2 - connect via Bluetooth = On

setting 'Ask every time' (or Only this time on popup ) may mean that Android memorises the permission for awhile, but not clear for how long or when it expires... To avoid confusion, this setting will not be under test here until the ticket #2000 is clarified.

#### Steps to execute:
**Device 1** (Pixel2)
* Tap on + sign on contacts page 
* A message comes up saying: "Allow Briar to take pictures and record videos".  Options are: While using the app, Only this time, and Deny.  Tap on 'While using the app'
* A message shows saying "Allow Briar to access this device's location? Options are: While using the app, Only this time, and Deny.  Select ('_Only this time_') 'Allow when in use'.
* A message appears saying: "Briar wants to make your phone visible to other bluetooth devices during 120 seconds.  Options are Allow and Deny. Tap: Allow
* Tap on 'continue' when asked.



**Device 2** (Nokia 3.1 Android 10)
* Tap on + sign on contacts page, then select 'nearby' option
* A message shows saying "Allow Briar to access this device's location? Options are: While using the app, Deny and Deny and don't ask again.  Select 'only while using the app'.
* A message appears asking for permission to use the camera
* Tap on 'continue' when asked.
* A message appears saying: "Briar wants to make your phone visible to other bluetooth devices during 120 seconds.  Options are Allow and Deny. Tap: Allow

####Expected results:
* contacts are successful added and they appear in each other's contact lists, as online. 
* the device location settings are: Device 1 = unchanged ("allow while using the app"), device 2 has changed from the initial state of 'denied' to 'allow while using the app'.


(note: Ask every time option does not exist in Android 10 devices, nor in older ones.)



Steps described in #2001
Related ticket #2002

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)