**Steps to run:**

- verify that mailbox is up and running on a device, and that it is correctly linked with the briar app on another device
- then on the mailbox status screen on the mailbox device, tap Unlink
- verify that the mailbox app can be successfully linked with the Briar app on another device 
- then go to the Briar device from the first step and tap the Unlink on the mailbxo status screen. 
- Briar app should be successfully unlinked and therefore capable of being successfully linked with a mailbox on another device (or the mailbox app on the same device that has already been unlinked).

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)
 

