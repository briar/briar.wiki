One device  is online and the other connects later on ( a few hours later)
* User pastes the code of the contact who is offline at the moment
* At the bottom of the Contacts screen, there is a message to the user 'Pending contacts...' and the user can click on Show, which will display the name of the user whose 'add' is pending. 
* It is possible to delete this pending contact by tapping on the bin icon - delete it
* Recreate contact at a distance in the same way
* Contact should be created successfully when the other contact comes online pastes the code of the first user.

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)