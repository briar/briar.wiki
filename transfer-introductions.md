All three devices (A, B and C) are offline

#### Scenario 
- Person A wants to introduce persons B and C to each other.  
- This process sends a message from A to B and from A to C.  
- Both B and C's messages need to be exported - separately in two different files, so A  transfers data by Sending to B, and then repeats the process by Sending to C
- B imports and accepts the invitation and exports the acceptance message back to A.  
- A imports it.  
- C imports the A's invitation message, accepts it, and exports the acceptance message back to A.  
- A imports B's response, which adds another tick to the original introduction message tht A sent to B
- A imports C's response, which adds another tick to the original introduction message that A send to C
- A now knows that B and C want to have each other as contacts - but B and C are not added to each other's contact lists as they are still offline and they have no means of knowing if the other party accepted the intro or not (only A knows it at this point).  
- Bring all devices online, first the introduced ones (B and C) and then the introducer A. 
- When everybody comes online at the same time,  contacts are added to B and C's contact lists

Repeat scenario with bringing the B online at the same time as A, but not C; then bring the C online a few mins later.  

expected results are the same: contact B should be added to the list of contacts on device C, and contact C should be added to the list of contacts on device B

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/)