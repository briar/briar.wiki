The time last connected represent the time when Briar that is linked to the mailbox app in question, last came online. 

Briar checks for messages in mailbox when it comes online and then a short interval after that, but essentially, once it downloads the messages it does not check any more whilst it is online because the contacts are supposed to message briar directly (and not its mailbox) when briar is online. 

Briar may check periodically if it is able to reach mailbox, but that is not decided yet and when decision is made that will be a subject of a different test.

It would be good to test here that the time when briar comes online gets recorded and displayed correctly on the mailbox status screen. 

The briar can go offline for longer period of time - first longer than 12 hours to see that it saves the time > 12 hours correctly, then longer than 24 hours, and then longer than a week or a month... or even a year?

After a week it should switch to date.  Before that, it would be .. I guess 1 day ago, 2 days ago and so on.. The same at it is for messages (discussion in MM), 30/05/22, tetsing channel, 13:5à or thereabouts

Tested for mins ago, hours ago and days ago.  All OK. 28/7/2022

Re-tested 29/11/202, OK

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)