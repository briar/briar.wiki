#### Steps to execute:
- on a new installation received via a hotspot from another device, add a nearby contact (user of the sharing device + user of another different device that has non-shared installation of briar)
- add a contact at a distance 
- send messages to all new contacts 

#### Expected results: 
- new contacts are successfully added
- messages are successfuly sent to and from the briar installation received from a hotspot 

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/testing)
