General:

It is at all times during testing -- like after actions taken, events occuring
etc -- relevant to rotate the device and ensure that:

- Text entered in fields, switches toggled etc remain so
- Visibility of progressbars and buttons are the same
- Labels on buttons remain the same
- Opened dialog boxes remain so





Profile pictures

Perform all steps both in landscape and portrait and change orientation during the tasks, or immediately after them or before them.

- [ ] Settings -> under the users name there is a line that says 'Tap to change your profile picture'

- [ ] On tapping that line of text, a sreen opens that lets the user select a photo/picture they want to use for their profile.

- [ ] After user selects the picture, there is a popup that says: Change profile picture and 'only your contacts can see your profile image', plus the user's name and selected profile picture.  User can tap on 'change' or 'cancel'. 

- [ ] 'Cancel' button cancels the operation and returns the user to the previous screen: Settings.
- [ ] 'Change' button changes the profile picture for the user and returns the user to the Settings screen. 

- [ ] Repeat the above steps to cancel the change of profile pcitures 3 or 4 times (his will simulate the situation where the user cannot decide which picture to use as their prpfile picture)
- [ ] Select a picture and select 'change' on the pop up after the picture selection, and repeat those steps several times - again, to simulate the situation where the user cannot decide which picture to use for their profile
- [ ] once selected, the picture should be showing on the Settings page immadiately after selection

### Profile pictures in Private Groups

- [ ] the profile picture should not be shown as avatar on Private groups screen, for groups created by the user - on this screen the avatar is created by using the first letter of the user's name on their device
- [ ] Post created by the user in the private group should be show their profile pictures (and not avatars from the private groups screen)
- [ ] These profile pictures on posts should be visible only to direct contacts, but not to other members of private groups
- [ ] When a user changes their profile picture, the change should be reflected also in the private group messages.  However, the change is not reflected immediately but after a screne refresh (by tapping back button to get out of the screen and then gettng back in)

### Profile pictures in Forums


- [ ] Profile picture should not be shown as avatar on Forums screen, for forums created by the user - on this screen the avatar is created by using the first letter of the user's name on their device
- [ ] Posts created by the user in the forums should be showing their profile pictures to their direct contacts, but not showing to the other members of forums.

### Profile pictures in Blogs

- [ ] Blog posts created by the user should show their profile picture to their direct contacts but not to others (to whom the blog may have been re-blogged)
- [ ] user's replies to posts, as well as new posts should display user's profile picture correctly
- [ ] When user changes their profile photo, all their direct contacts should see the updated profile picture for that contact on their devices (even without direct messages being exchanged between them) - this happens after the screen is refreshed.

### Add nearby contact

Make a direct contact between people who didnt have direct contact before, and verify that their profile picture are now visible to each other (and they were not before)

### Make introduction

Perfom introduction between two contact who did not have direct contact before and verify that their profile pictures are now visible to each other (and they were not before)










