The onboarding screens are defined here https://www.figma.com/file/bFoueGka5aTwlfL4Ap920v/Mailbox?node-id=0%3A1

- to test all links - the back button on each screen, to test them in landscape as well as portrait orientation. 
- all links or buttons to be tapped two or more times

[Back to Testing](https://code.briarproject.org/briar/briar/-/wikis/Testing)